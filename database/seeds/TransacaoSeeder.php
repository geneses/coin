<?php

use Illuminate\Database\Seeder;

class TransacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transacao_tipos')->insert([
            'nome' => 'Pontos de fidelidade',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Bônus de inicio rapido',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Bônus de indicação',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Compras de pacote',
            'acao' => '0',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Débito para',
            'acao' => '0',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Bloqueado',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Saldo desbloqueado',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Bônus de venda',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Compras de produto',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Transferência para',
            'acao' => '0',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Recebimento de transferência',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Saque',
            'acao' => '0',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Débito',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Bônus binário',
            'acao' => '1',
        ]);

        DB::table('transacao_tipos')->insert([
            'nome' => 'Bônus residual',
            'acao' => '1',
        ]);
    }
}
