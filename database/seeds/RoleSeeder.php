<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'empresa',        
        ]);

        DB::table('roles')->insert([
            'name' => 'usuario_ativo',
        ]);

        DB::table('roles')->insert([
            'name' => 'usuario_pendente',
        ]);

        DB::table('roles')->insert([
            'name' => 'administrativo_cd',
        ]);

        DB::table('roles')->insert([
            'name' => 'usuario_pa',
        ]);

        DB::table('roles')->insert([
            'name' => 'usuario_site',
        ]);
    }
}
