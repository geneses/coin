<?php

use Illuminate\Database\Seeder;

class MaximoMinimo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('maximo_minimos')->insert([
            'maximo' => '10000',
            'minimo' => '20',
            'tipo' => 'saque',
        ]);

        DB::table('maximo_minimos')->insert([
            'maximo' => '10000',
            'minimo' => '20',
            'tipo' => 'transferencia',
        ]);
    }
}
