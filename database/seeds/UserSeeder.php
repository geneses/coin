<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'nome' => 'admin',
            'user' => 'admin',
            'email' => 'suporte.coinclube@gmail.com',
            'senha_transacao' => 'QuKv1jooja',
            'password' => Hash::make('123123'),
        ]);
    }
}
