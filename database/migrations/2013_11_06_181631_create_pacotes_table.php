<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->integer('bonus_binario');
            $table->integer('disponivel')->nullable();
            $table->integer('comissao_unilevel');
            $table->integer('comissao_binario');
            $table->integer('ponto_unilevel');
            $table->integer('inicio_rapido')->default(0);
            $table->integer('inicio_rapido_valor')->default(0);
            $table->integer('ativo_mes')->default(0);
            $table->float('v_compra_produto',5,2); 
            $table->float('valor_pacote',8,2);
            $table->float('residual',8,2);
            $table->timestamps();
        });

         Schema::table('pacotes', function(Blueprint $table) {
            $table->bigInteger('fk_transacao_tipo')->unsigned();
            $table->foreign('fk_transacao_tipo')->references('id')->on('transacao_tipos')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacotes');
    }
}
