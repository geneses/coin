<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracaos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_empresa')->nullable();
            $table->text('logo_empresa')->nullable();
            $table->string('email_empresa')->nullable();
            
            $table->string('nome_site')->nullable();
            $table->text('logo_site')->nullable();
            $table->string('email_site')->nullable();

            $table->string('nome_escritorio')->nullable();
            $table->text('logo_escritorio')->nullable();
            $table->string('email_escritorio')->nullable();

            $table->string('nome_loja')->nullable();
            $table->text('logo_loja')->nullable();
            $table->string('email_loja')->nullable();

            $table->text('termos_uso')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuracaos');
    }
}
