<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaldoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saldo_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('saldo',10,2);
            $table->timestamps();
        });

        Schema::table('saldo_users', function(Blueprint $table) {
            $table->bigInteger('fk_user')->unsigned();
            $table->foreign('fk_user')->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saldo_users');
    }
}
