<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuporteMensagemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suporte_mensagems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->string('mensagem');
            $table->string('status'); 
            $table->string('data_abertura')->nullable();
            $table->string('data_alteracao')->nullable();
            $table->string('data_fechamento')->nullable();
            $table->timestamps();
        });

        Schema::table('suporte_mensagems', function(Blueprint $table) {
            $table->bigInteger('fk_user')->unsigned();
            $table->foreign('fk_user')->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suporte_mensagems');
    }
}
