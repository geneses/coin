<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualificacaoTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualificacao_tipos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('premio');
            $table->integer('pontuacao_binario');
            $table->integer('pontuacao_unilevel');
            $table->integer('maximo_binario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualificacao_tipos');
    }
}
