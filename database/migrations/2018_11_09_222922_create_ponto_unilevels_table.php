<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePontoUnilevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ponto_unilevels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('valor',8,2)->nullable();
            $table->string('nivel');
            $table->timestamps();
        });

        Schema::table('ponto_unilevels', function(Blueprint $table) {
            $table->bigInteger('fk_pacote')->unsigned();
            $table->foreign('fk_pacote')->references('id')->on('pacotes')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ponto_unilevels');
    }
}
