<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaldoBloqueadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saldo_bloqueados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('saldo',10,2);
        });

        Schema::table('saldo_bloqueados', function(Blueprint $table) {
            $table->bigInteger('fk_saldo_user')->unsigned();
            $table->foreign('fk_saldo_user')->references('id')->on('saque_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saldo_bloqueados');
    }
}
