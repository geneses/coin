<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ativo')->default('true');
            $table->string('nome');
            $table->string('imagem');
            $table->float('peso',10,2);
            $table->text('descricao');
            $table->float('comissao_produto',10,2);
            $table->integer('estoque');
            $table->float('valor_compra',10,2);
            $table->float('valor_v_site',10,2);
            $table->float('valor_v_usuario',10,2);
            $table->float('valor_v_pa',10,2);
            $table->float('valor_v_cd',10,2);
            $table->float('pontos_binario',10,2);
            $table->float('ponto_unilevel',10,2)->default(0);
            $table->float('pontos_carreira',10,2);
            $table->date('data_cadastro');
            
        });


        Schema::table('produtos', function(Blueprint $table) {
            $table->bigInteger('fk_categoria')->unsigned()->nullable();
            $table->foreign('fk_categoria')->references('id')->on('categorias');
            $table->bigInteger('fk_user')->unsigned();
            $table->foreign('fk_user')->references('id')->on('users');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
