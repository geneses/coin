<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoFidelidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto_fidelidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ativo')->default('true');
            $table->string('nome');
            $table->string('imagem');
            $table->float('peso',10,2);
            $table->text('descricao');
            $table->integer('estoque');
            $table->float('valor_compra',10,2);
            $table->float('valor_ponto_fidelidade',10,2);
        });

        Schema::table('produto_fidelidades', function(Blueprint $table) {
            $table->bigInteger('fk_categoria')->unsigned()->nullable();
            $table->foreign('fk_categoria')->references('id')->on('fidelidade_categorias');
            $table->bigInteger('fk_user')->unsigned();
            $table->foreign('fk_user')->references('id')->on('users');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto_fidelidades');
    }
}
