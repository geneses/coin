<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transacaos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('valor',8,2);
            $table->timestamps();
        });

        Schema::table('transacaos', function(Blueprint $table) {
            $table->bigInteger('fk_transacao_tipo')->unsigned();
            $table->foreign('fk_transacao_tipo')->references('id')->on('transacao_tipos')
                    ->onDelete('cascade');
        });

        Schema::table('transacaos', function(Blueprint $table) {
            $table->bigInteger('fk_membro')->unsigned();
            $table->foreign('fk_membro')->references('id')->on('users')
                    ->onDelete('cascade');
        });

        Schema::table('transacaos', function(Blueprint $table) {
            $table->bigInteger('fk_adesao_usuario')->unsigned()->nullable();
            $table->foreign('fk_adesao_usuario')->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transacaos');
    }
}
