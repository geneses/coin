<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome')->nullable();
            $table->string('user')->nullable()->unique();
            $table->string('status')->nullable()->default('Ativo');
            $table->integer('inicio_rapido')->default(0);
            $table->integer('ativo_mes')->default(0);
            $table->integer('ponto_unilevel')->default(0);
            $table->integer('ponto_fidelidade')->default(0);
            $table->integer('nivel_unilevel')->default(0);
            $table->integer('derramamento')->nullable();
            $table->integer('ponto_esquerda')->default(0);
            $table->integer('ponto_direita')->default(0);
            $table->integer('infinite')->default(0);
            $table->integer('total_ponto_es')->default(0);
            $table->integer('linha_patrocinada')->nullable();
            $table->integer('total_ponto_di')->default(0);
            $table->string('patrocinador')->nullable();
            $table->string('acima')->nullable();
            $table->string('lado_rede')->default(0);
            $table->string('posicao_geral')->default(0);
            $table->string('avatar')->nullable();
            $table->date('data_nascimento')->nullable();
            $table->string('cpf')->nullable();
            $table->string('rg')->nullable();
            $table->string('orgao_expedidor')->nullable();
            $table->string('pis')->nullable();
            $table->string('telefone')->nullable();
            $table->string('telefone2')->nullable();
            $table->string('rua')->nullable();
            $table->string('complemento')->nullable();
            $table->string('numero')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cidade')->nullable();
            $table->string('estado')->nullable();
            $table->string('cep')->nullable();
            $table->string('sexo')->nullable();
            $table->date('data_cadastro')->nullable();
            $table->date('data_ativacao')->nullable();
            $table->string('banco')->nullable();
            $table->string('agencia')->nullable();
            $table->string('agencia_digito')->nullable();
            $table->string('conta')->nullable();
            $table->string('conta_digito')->nullable();
            $table->string('conta_tipo')->nullable();
            $table->string('recru_texto1')->nullable();
            $table->string('recru_texto2')->nullable();
            $table->string('recru_video1')->nullable();
            $table->string('recru_video2')->nullable();
            $table->string('recru_fundo1')->nullable();
            $table->string('recru_fundo2')->nullable();
            $table->string('recru_link')->nullable();
            $table->string('email')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('senha_transacao')->nullable();;
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('users', function(Blueprint $table) {
            $table->bigInteger('fk_pacote')->unsigned()->nullable();
            $table->foreign('fk_pacote')->references('id')->on('pacotes')
                    ->onDelete('cascade');
        });

        Schema::table('users', function(Blueprint $table) {
            $table->bigInteger('fk_qualificacao')->unsigned()->nullable();
            $table->foreign('fk_qualificacao')->references('id')->on('qualificacaos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
