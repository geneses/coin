<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFidelidadeUserComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fidelidade_user_compras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('quantidade');
            $table->float('valor',10,2);
        });

        Schema::table('fidelidade_user_compras', function(Blueprint $table) {
            $table->bigInteger('fk_user')->unsigned();
            $table->foreign('fk_user')->references('id')->on('users');
        });

        Schema::table('fidelidade_user_compras', function(Blueprint $table) {
            $table->bigInteger('fk_produto')->unsigned();
            $table->foreign('fk_produto')->references('id')->on('produto_fidelidades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fidelidade_user_compras');
    }
}
