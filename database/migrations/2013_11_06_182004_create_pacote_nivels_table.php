<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacoteNivelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacote_nivels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('valor',8,2)->nullable();
            $table->string('nivel');
            $table->timestamps();
        });

        Schema::table('pacote_nivels', function(Blueprint $table) {
            $table->bigInteger('fk_pacote')->unsigned();
            $table->foreign('fk_pacote')->references('id')->on('pacotes')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacote_nivels');
    }
}
