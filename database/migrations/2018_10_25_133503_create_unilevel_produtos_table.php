<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnilevelProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unilevel_produtos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('valor')->nullable();
            $table->string('nivel');
            $table->timestamps();
        });

        Schema::table('unilevel_produtos', function(Blueprint $table) {
            $table->bigInteger('fk_produto')->unsigned();
            $table->foreign('fk_produto')->references('id')->on('produtos')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unilevel_produtos');
    }
}
