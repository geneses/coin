$(document).ready(function($) {
  var uploadField = document.getElementById("recru_fundo1");
  var uploadField2 = document.getElementById("recru_fundo2");

  uploadField.onchange = function() {
    if (this.files[0].size > 1097152) {
      alert("Imagem muito grande!");
      this.value = "";
    }
  };

  uploadField2.onchange = function() {
    if (this.files[0].size > 1097152) {
      alert("Imagem muito grande!");
      this.value = "";
    }
  };

  var base_url = "http://" + window.location.host.toString();
  var base_url = location.protocol + "//" + window.location.host.toString();

  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
  });

  $(document).on("click", ".add", function() {
    console.log("asdasd");
    var dados = new FormData($("#form")[0]); //pega os dados do form

    console.log(dados);

    $.ajax({
      type: "post",
      url: "/gerenciar-capturas/store",
      data: dados,
      processData: false,
      contentType: false,
      beforeSend: function() {
        jQuery(".add").button("loading");
      },
      complete: function() {
        jQuery(".add").button("reset");
      },
      success: function(data) {
        //Verificar os erros de preenchimento
        if (data.errors) {
          $(".callout").removeClass("hidden"); //exibe a div de erro
          $(".callout")
            .find("p")
            .text(""); //limpa a div para erros successivos

          $.each(data.errors, function(nome, mensagem) {
            $(".callout")
              .find("p")
              .append(mensagem + "</br>");
          });
        } else {
          $("#table")
            .DataTable()
            .draw(false);

          jQuery("#criar_editar-modal").modal("hide");

          $(function() {
            iziToast.destroy();
            iziToast.success({
              title: "OK",
              message: "Recrutamento adicionado com Sucesso!"
            });
          });
        }
      },

      error: function() {
        iziToast.error({
          title: "Erro Interno",
          message: "Operação Cancelada!"
        });
      }
    });
  });

  $(document).on("click", ".alterar_lado", function() {
    console.log("asdasd");
    var dados = new FormData($("#form")[0]); //pega os dados do form

    console.log(dados);

    $.ajax({
      type: "post",
      url: "/gerenciar-usuarios/lado-rede",
      data: dados,
      processData: false,
      contentType: false,
      beforeSend: function() {
        jQuery(".alterar_lado").button("loading");
      },
      complete: function() {
        jQuery(".alterar_lado").button("reset");
      },
      success: function(data) {
        //Verificar os erros de preenchimento
        if (data.errors) {
          $(".callout").removeClass("hidden"); //exibe a div de erro
          $(".callout")
            .find("p")
            .text(""); //limpa a div para erros successivos

          $.each(data.errors, function(nome, mensagem) {
            $(".callout")
              .find("p")
              .append(mensagem + "</br>");
          });
        } else {
          $("#table")
            .DataTable()
            .draw(false);

          jQuery("#criar_editar-modal").modal("hide");

          $(function() {
            iziToast.destroy();
            iziToast.success({
              title: "OK",
              message: "Recrutamento adicionado com Sucesso!"
            });
          });
        }
      },

      error: function() {
        iziToast.error({
          title: "Erro Interno",
          message: "Operação Cancelada!"
        });
      }
    });
  });
});

function myFunction() {
  var copyText = document.getElementById("user");
  copyText.select();
  document.execCommand("copy");
}
