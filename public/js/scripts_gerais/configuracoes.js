$(document).ready(function($) {
    
    var base_url = 'http://' + window.location.host.toString();
    var base_url = location.protocol + '//' + window.location.host.toString();


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '.salvar', function() {

        var dados = new FormData($("#form")[0]); 
        $.ajax({
            type: 'post',
            url: "/configuracoes-geral/store",
            enctype:"multipart/form-data",
            cache: false,
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.salvar').button('loading');
            },
            complete: function() {
                jQuery('.salvar').button('reset');
            },
            success: function(data) {
                console.log(data);
                    //Verificar os erros de preenchimento
                if ((data.errors)) {

                    $('.callout').removeClass('hidden'); //exibe a div de erro
                    $('.callout').find('p').text(""); //limpa a div para erros successivos

                    $.each(data.errors, function(nome, mensagem) {
                            $('.callout').find("p").append(mensagem + "</br>");
                    });

                } else {
                    
                    $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Configurações Salvas com Sucesso!',
                        });
                    });

                
                }
            },

            error: function() {

                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
        });
});
