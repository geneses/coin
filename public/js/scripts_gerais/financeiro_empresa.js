$(document).ready(function($) {
    var base_url = 'http://' + window.location.host.toString();
    var base_url = location.protocol + '//' + window.location.host.toString();

    var user = $("#user");
        user.focusout( function(){
                console.log('dados');
                $.ajax({
                    type: 'post',
                    url: "/gerenciar-usuarios/nome-user",
                    data: {
                        'user': user.val(),
                    },
                    success: function(data) {
                        if ((!data.errors)) {

                        }
                        $('#nome-user').removeClass('hidden');
                        $('#nome_user').val(data.nome);
                        
                        if(typeof data.nome == 'undefined'){
                            $('#salvar').prop('disabled',true);
                        }else{
                            $('#salvar').prop('disabled',false);
                        }
                    },
                    error: function() {
                        
                    },

                });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var tabela = $('#table').DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            ajax: './list',
            columns: [
            { data: null, name: 'order' },
            { data: 'nome', name: 'nome' },
            { data: 'valor', name: 'valor' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' },
            { data: 'acoes', name: 'acoes' }
            ],
            createdRow : function( row, data, index ) {
                row.id = "item-" + data.id;   
            },

            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: false,
            scrollX: true,
            sorting: [[ 1, "asc" ]],
            responsive: true,
            lengthMenu: [
                [10, 15, -1],
                [10, 15, "Todos"]
            ],
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "<div><i class='fa fa-circle-o-notch fa-spin' style='font-size:38px;'></i> <span style='font-size:20px; margin-left: 5px'> Carregando...</span></div>",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            columnDefs : [
              { targets : [2], sortable : false },
              { "width": "5%", "targets": 0 }, //nº
              { "width": "20%", "targets": 1 },//nome
              { "width": "20%", "targets": 2 },//nome
              { "width": "10%", "targets": 3 },//ação
              { "width": "10%", "targets": 4 },//ação
              { "width": "10%", "targets": 5 },//ação
            ]
    });

    tabela.on('draw.dt', function() {
        tabela.column(0, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
            cell.innerHTML = tabela.page.info().page * tabela.page.info().length + i + 1;
        });
    }).draw();

    
$(document).on('click', '.credito', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-financeiro/credito",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.credito').prop('disabled',true);
                $('.credito').html('Aguarde...');
                },
            complete: function() {
                $('.credito').prop('disabled',false);
                $('.credito').html('Cadastrar');
            },
            success: function(data) {
                $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Crédito cadastrado com Sucesso!',
                        });
                    });
            },

            error: function(data) {
                iziToast.error({
                    title: 'Erro',
                    message: data.responseText,
                });
            },

        });
    });


$(document).on('click', '.debito', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-financeiro/debito",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.debito').prop('disabled',true);
                $('.debito').html('Aguarde...');
                },
            complete: function() {
                $('.debito').prop('disabled',false);
                $('.debito').html('Debitar');
            },
            success: function(data) {
                $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Débito efetuado com Sucesso!',
                        });
                    });
            },

            error: function(data) {
                iziToast.error({
                    title: 'Erro',
                    message: data.responseText,
                });
            },

        });
    });

$(document).on('click', '.edit', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-financeiro/modificar-saque",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.debito').prop('disabled',true);
                $('.debito').html('Aguarde...');
                },
            complete: function() {
                $('.debito').prop('disabled',false);
                $('.debito').html('Cadastrar');
            },
            success: function(data) {
                $('#table').DataTable().draw(false);
                jQuery('#criar_editar-modal').modal('hide');

                $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Alteraçã efetuada com Sucesso!',
                        });
                    });
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });


    
$(document).on('click', '.btnVer', function() {

        $('.modal-footer .btn-action').removeClass('edit');
        $('.modal-footer .btn-action').addClass('hidden');
        $('.modal-title').text('Ver Transação');
        
        //desabilita os campos
        $('#nome').prop('readonly',true);
        $('#sigla').prop('readonly',true);
        $('#email').prop('readonly',true);

        $('.callout').addClass("hidden"); //ocultar a div de aviso
        $('.callout').find("p").text(""); //limpar a div de aviso

        var btnEditar = $(this);

        $('#form :input').each(function(index,input){
            $('#'+input.id).val($(btnEditar).data(input.id));
        });

        
        jQuery('#criar_editar-modal').modal('show');
});
$(document).on('click', '.btnEditar', function() {
        $('.modal-footer .btn-action').removeClass('add');
        $('.modal-footer .btn-action').addClass('edit');
        $('.modal-footer .btn-action').removeClass('hidden');

        $('.modal-title').text('Editar Transação');
        $('.callout').addClass("hidden"); //ocultar a div de aviso
        $('.callout').find("p").text(""); //limpar a div de aviso

        //habilita os campos desabilitados
        $('#nome').prop('readonly',false);
        $('#sigla').prop('readonly',false);
        $('#email').prop('readonly',false);

        var btnEditar = $(this);

        $('#form :input').each(function(index,input){
            $('#'+input.id).val($(btnEditar).data(input.id));
        });

        
        jQuery('#criar_editar-modal').modal('show'); //Abrir o modal
});

   
  
});

