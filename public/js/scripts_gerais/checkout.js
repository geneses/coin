$(document).ready(function($) {
    
    var base_url = 'http://' + window.location.host.toString();
    var base_url = location.protocol + '//' + window.location.host.toString();

    function parcelas(total){
    
        var option = ''
        $.getJSON('/checkout/parcelas/'+total,function(data){
            $.each(data, function(i,data){
                option += '<option value='+data.id+' data-parcelas='+data.parcelas+' data-porcentagem='+data.porcentagem+'>'+data.valor+'</option>';
            })
            $("#parcelas").html(option)
        })
        
        
    }

    $("input[type=radio]").click(function(){
        //Condição para sem frete
        
        var valor = parseFloat($("#valor2").val()) 
        if($(this).val() == 0){
            $("#valor").val("R$ "+valor);
            parcelas(0)
        }else{
            var valor_peso = $(this).data('valor');
            var soma =  valor_peso + valor;
            $("#valor").val("R$ "+soma)
            parcelas(valor_peso)
        }
    })

    $("#parcelas").change(function(){
        var v_string = $("#parcelas :selected").text().split("R$"); 

        $("#valor").val("R$ "+($("#parcelas :selected").data("parcelas") * v_string[1]))
    })


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '.pagar', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form

        //console.log(dados);

        $.ajax({
            type: 'post',
            url: "/checkout/pagar",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                //jQuery('.pagar').button('loading');
            },
            complete: function() {
                //jQuery('.pagar').button('reset');
            },
            success: function(data) {
                 //Verificar os erros de preenchimento
                if ((data.errors)) {

                    $('.callout').removeClass('hidden'); //exibe a div de erro
                    $('.callout').find('p').text(""); //limpa a div para erros successivos

                    $.each(data.errors, function(nome, mensagem) {
                            $('.callout').find("p").append(mensagem + "</br>");
                    });

                } else {
                    
                    window.location.reload('clube');

                    $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Compra realizada com Sucesso!',
                        });
                    });
                }
            },

            error: function(data) {
                //console.log(data.responseJSON.message);


                var json = JSON.parse(data.responseJSON.message);
                json  = json[0].parameter+' '+json[0].message;

                var msg = data.responseJSON.message;
                if(typeof data.responseJSON.message == 'undefined'){
                    msg = data.responseJSON;
                }else{
                    msg = json;
                }

                iziToast.error({
                    title: 'Erro Interno',
                    message: msg,
                });
            },
        });
    });


});
