$(document).ready(function($) {
    
    var base_url = 'http://' + window.location.host.toString();
    var base_url = location.protocol + '//' + window.location.host.toString();


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(document).on('click', '#delete', function() {
        var id = $('#delete').val();

        $.ajax({
            type: 'get',
            url: "/fidelidade/delete/item/carrinho/"+id,
            enctype:"multipart/form-data",
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.delete').button('loading');
            },
            complete: function() {
                jQuery('.delete').button('reset');
            },
            success: function(data) {
                setTimeout(function () { location.reload(1); }, 0);

            },

            error: function() {

                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },
        });
    });

    $(document).on('click', '#delete_clube', function() {
        var id = $('#delete_clube').val();

        $.ajax({
            type: 'get',
            url: "/clube/delete/item/carrinho/"+id,
            enctype:"multipart/form-data",
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.delete').button('loading');
            },
            complete: function() {
                jQuery('.delete').button('reset');
            },
            success: function(data) {
                setTimeout(function () { location.reload(1); }, 0);

            },

            error: function() {

                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },
        });
    });

    // $(document).on('click', '.comprar', function() {
    //         var dados = new FormData($("#form")[0]); //pega os dados do form

    //         console.log('dados');
    //         $.ajax({
    //             type: 'post',
    //             url: "/clube/comprar",
    //             data: dados,
    //             processData: false,
    //             contentType: false,
    //             beforeSend: function(){
    //             $('.comprar').prop('disabled',true);
    //             $('.comprar').html('Aguarde...');
    //             },
    //             complete: function() {
    //                 $('.comprar').prop('disabled',false);
    //                 $('.comprar').html('Finalizar compra');
    //             },
    //             success: function(data) {
    //                 window.location.reload();
    //             },
    //             error: function(data) {
    //             iziToast.error({
    //                 title: 'Erro',
    //                 message: data.responseText,
    //                 });
    //             },

    //         });
    // });
    $(document).on('click', '.gerarPedido', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form

        console.log('dados');
        $.ajax({
            type: 'post',
            url: "/clube/gerarpedido",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
            $('.gerarPedido').prop('disabled',true);
            $('.gerarPedido').html('Aguarde...');
            },
            complete: function() {
                $('.gerarPedido').prop('disabled',false);
                $('.gerarPedido').html('Gerar Pedido');
            },
            success: function(data) {
                window.location.reload();
            },
            error: function(data) {
            iziToast.error({
                title: 'Erro',
                message: data.responseText,
                });
            },

        });

});



});


$(document).on('click', '.confirmar_compra', function() { 
    $('.modal-title').text('Finalizar Compra'); 
    jQuery('#confirmar-modal').modal('show');
    $('.btn-action').addClass('comprar');
    $('.btn-action').removeClass('gerarPedido');
});

$(document).on('click', '.gerar_pedido', function() {  
    $('.modal-title').text('Gerar Pedido');
    jQuery('#confirmar-modal').modal('show');
    $('.btn-action').removeClass('comprar');
    $('.btn-action').addClass('gerarPedido');
});

