$(document).ready(function($) {
    
    var base_url = 'http://' + window.location.host.toString();
    var base_url = location.protocol + '//' + window.location.host.toString();

    var ponto = new Array();
    var ponto_unilevel = new Array();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    
    var tabela = $('#table').DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            ajax: './gerenciar-pacotes/list',
            columns: [
            { data: null, name: 'order' },
            { data: 'nome', name: 'nome' },
            { data: 'ponto_unilevel', name: 'ponto_unilevel' },
            { data: 'comissao_unilevel', name: 'comissao_unilevel' },
            { data: 'comissao_binario', name: 'comissao_binario' },
            { data: 'acoes', name: 'acoes' }
            ],
            createdRow : function( row, data, index ) {
                row.id = "item-" + data.id;   
            },

            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: false,
            scrollX: true,
            sorting: [[ 1, "asc" ]],
            responsive: true,
            lengthMenu: [
                [10, 15, -1],
                [10, 15, "Todos"]
            ],
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "<div><i class='fa fa-circle-o-notch fa-spin' style='font-size:38px;'></i> <span style='font-size:20px; margin-left: 5px'> Carregando...</span></div>",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            columnDefs : [
              { targets : [2], sortable : false },
              { "width": "5%", "targets": 0 }, //nº
              { "width": "20%", "targets": 1 },//nome
              { "width": "20%", "targets": 2 },//nome
              { "width": "10%", "targets": 3 }//ação
            ]
    });

    tabela.on('draw.dt', function() {
        tabela.column(0, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
            cell.innerHTML = tabela.page.info().page * tabela.page.info().length + i + 1;
        });
    }).draw();

    $('.modal-footer').on('click', '.add', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-pacotes/store",
            data: {
                'nome': $("#nome").val(),
                'bonus_binario': $("#bonus_binario").val(),
                'ponto_unilevel_pacote': $("#ponto_unilevel").val(),
                'v_compra_produto': $("#v_compra_produto").val(),
                'v_compra_produto': $("#v_compra_produto").val(),
                'valor_pacote': $("#valor_pacote").val(),
                'comissao_unilevel': $("#comissao_unilevel").val(),
                'comissao_binario': $("#comissao_binario").val(),
                'fk_transacao_tipo': $("#fk_transacao_tipo").val(),
                'inicio_rapido': $("#inicio_rapido").val(),
                'disponivel': $("#disponivel").val(),
                'ativo_mes': $("#ativo_mes").val(),
                'residual': $("#residual").val(),
                'inicio_rapido_valor': $("#inicio_rapido_valor").val(),
                'ponto': ponto,
                'ponto_unilevel': ponto_unilevel
            },
            beforeSend: function(){
                jQuery('.add').button('loading');
            },
            complete: function() {
                jQuery('.add').button('reset');
            },
            success: function(data) {
                 //Verificar os erros de preenchimento
                if ((data.errors)) {

                    $('.callout').removeClass('hidden'); //exibe a div de erro
                    $('.callout').find('p').text(""); //limpa a div para erros successivos

                    $.each(data.errors, function(nome, mensagem) {
                            $('.callout').find("p").append(mensagem + "</br>");
                    });

                } else {
                    
                    $('#table').DataTable().draw(false);

                    jQuery('#criar_editar-modal').modal('hide');

                    $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Transação adicionado com Sucesso!',
                        });
                    });
                
                }
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });


    $('.modal-footer').on('click', '.edit', function() {


        var dados = new FormData($("#form")[0]); //pega os dados do form

        console.log(dados);

        $.ajax({
            type: 'post',
            url: "/gerenciar-pacotes/update",
            data: {
                'id': $("#id").val(),
                'nome': $("#nome").val(),
                'bonus_binario': $("#bonus_binario").val(),
                'ponto_unilevel_pacote': $("#ponto_unilevel").val(),
                'v_compra_produto': $("#v_compra_produto").val(),
                'v_compra_produto': $("#v_compra_produto").val(),
                'valor_pacote': $("#valor_pacote").val(),
                'comissao_unilevel': $("#comissao_unilevel").val(),
                'comissao_binario': $("#comissao_binario").val(),
                'fk_transacao_tipo': $("#fk_transacao_tipo").val(),
                'residual': $("#residual").val(),
                'inicio_rapido': $("#inicio_rapido").val(),
                'disponivel': $("#disponivel").val(),
                'inicio_rapido_valor': $("#inicio_rapido_valor").val(),
                'ativo_mes': $("#ativo_mes").val(),
                'ponto': ponto,
                'ponto_unilevel': ponto_unilevel
            },
            beforeSend: function(){
                jQuery('.edit').button('loading');
            },
            complete: function() {
                jQuery('.edit').button('reset');
            },
            success: function(data) {
                 //Verificar os erros de preenchimento
                if ((data.errors)) {

                    $('.callout').removeClass('hidden'); //exibe a div de erro
                    $('.callout').find('p').text(""); //limpa a div para erros successivos

                    $.each(data.errors, function(nome, mensagem) {
                            $('.callout').find("p").append(mensagem + "</br>");
                    });

                } else {

                    for (var i = 0; i < ponto.length; i++) {
                        $('.linha'+i).remove();
                    }
                    
                    ponto.splice(0,ponto.length);
                    
                    for (var i = 0; i < ponto_unilevel.length; i++) {
                        $('.linhaa'+i).remove();
                    }

                    ponto_unilevel.splice(0,ponto_unilevel.length);
                    var id = $('#id').val();
        
                    
                    $('#table').DataTable().draw(false);

                    jQuery('#criar_editar-modal').modal('hide');

                    $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Transação alterado com Sucesso!',
                        });
                    });
                }
            },

            error: function() {
                jQuery('#criar_editar-modal').modal('hide'); //fechar o modal

                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });

    $('.modal-footer').on('click', '.excluir', function() {
        var dados = new FormData($("#deletar")[0]); //pega os dados do form

        console.log(dados);

        $.ajax({
            type: 'post',
            url: "/gerenciar-pacotes/delete",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.excluir').button('loading');
            },
            complete: function() {
                jQuery('.excluir').button('reset');
            },
            success: function(data) {
                 //Verificar os erros de preenchimento
                if ((data.errors)) {

                    $('.callout').removeClass('hidden'); //exibe a div de erro
                    $('.callout').find('p').text(""); //limpa a div para erros successivos

                    $.each(data.errors, function(nome, mensagem) {
                            $('.callout').find("p").append(mensagem + "</br>");
                    });

                } else {
                    
                    $('#table').DataTable().draw(false);

                    jQuery('#criar_deletar-modal').modal('hide');

                    $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Transação deletado com Sucesso!',
                        });
                    });
                }
            },

            error: function() {
                jQuery('#criar_editar-modal').modal('hide'); //fechar o modal
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });


    var i = 0;

  //Adicionar material
  $(document).on('click', '.btnAdcNivel', function() {
    //verificar se a opção selecionada possiu valor
    //verificar se a opção selecionada já se encontra no array ponto e emitir alerta quando já estiver
    i = ponto.length;
    var cols = '';
    cols = '';
    novaLinha = null; 
    var nivel = $('#nivel :selected').val();
    var valor = $('#valor').val();

    var novaLinha = '<tr class="'+'linha'+i+'">';
    //Adc material ao array
    ponto.push({'nivel': nivel, 'valor': valor});

    /* Crian a linha p/ tabela*/
    cols += '<td>'+nivel+'</td>';
    cols += '<td>'+valor+'</td>';
    cols += '<td class="text-left"><a class="btnRemoverPonto btn btn-xs btn-danger" data-indexof="'+ponto.indexOf(ponto[i])+'" data-linha="'+i+'"><i class="fa fa-trash"></i> Remover</a></td>';
    novaLinha += cols + '</tr>';
    


    $('#pacote_id').append(novaLinha); /*Adc a linha  tabela*/
    i+=1;

    $('#valor').val('');

    });

    //Remover Material
    $(document).on('click', '.btnRemoverPonto', function(){
    ponto.splice($(this).data('indexof'),1); //remove do array de acordo com o indice
    $('.linha'+ $(this).data('linha')).remove(); //Remove a linha da tela 
    });


    var j = 0;
    
  //Adicionar material


  $(document).on('click', '.btnAdcPontoUnilevel', function() {
    //verificar se a opção selecionada possiu valor
    //verificar se a opção selecionada já se encontra no array ponto_unilevel e emitir alerta quando já estiver
    j = ponto_unilevel.length;
    var cols = '';
    cols = '';
    novaLinha = null; 
    var nivel = $('#nivel2 :selected').val();
    var valor = $('#valor2').val();

    var novaLinha = '<tr class="'+'linhaa'+j+'">';
    //Adc material ao array
    ponto_unilevel.push({'nivel': nivel, 'valor': valor});

    /* Crian a linha p/ tabela*/
    cols += '<td>'+nivel+'</td>';
    cols += '<td>'+valor+'</td>';
    cols += '<td class="text-left"><a class="btnRemoverPontoUnilevel btn btn-xs btn-danger" data-indexof="'+ponto_unilevel.indexOf(ponto_unilevel[j])+'" data-linhaa="'+j+'"><i class="fa fa-trash"></i> Remover</a></td>';
    novaLinha += cols + '</tr>';
    
    $('#ponto_unilevel_id').append(novaLinha); /*Adc a linha  tabela*/
    j+=1;

    $('#valor').val('');

});

    //Remover Material
    $(document).on('click', '.btnRemoverPontoUnilevel', function(){

    ponto_unilevel.splice($(this).data('indexof'),1); //remove do array de acordo com o indice
    console.log(ponto_unilevel);
    $('.linhaa'+ $(this).data('linhaa')).remove(); //Remove a linha da tela 
    });


$(document).on('click', '.btnAdicionar', function() {

        for (var i = 0; i < ponto.length; i++) {
            $('.linha'+i).remove();
        }
        
        ponto.splice(0,ponto.length);
        
        for (var i = 0; i < ponto_unilevel.length; i++) {
            $('.linhaa'+i).remove();
        }

        ponto_unilevel.splice(0,ponto_unilevel.length);

        $('.modal-footer .btn-action').removeClass('edit');
        $('.modal-footer .btn-action').addClass('add');
        $('.modal-footer .btn-action').removeClass('hidden');

        //habilita os campos desabilitados
        $('#nome').prop('readonly',false);
        $('#sigla').prop('readonly',false);
        $('#email').prop('readonly',false);
        

        $('.modal-title').text('Novo Cadastro de Transação');
        $('.callout').addClass("hidden"); 
        $('.callout').find("p").text(""); 

        $('#form')[0].reset();

        jQuery('#criar_editar-modal').modal('show');
});

$(document).on('click', '.btnVer', function() {

        $('.modal-footer .btn-action').removeClass('edit');
        $('.modal-footer .btn-action').addClass('hidden');
        $('.modal-title').text('Ver Transação');
        
        //desabilita os campos
        $('#nome').prop('readonly',true);
        $('#sigla').prop('readonly',true);
        $('#email').prop('readonly',true);

        $('.callout').addClass("hidden"); //ocultar a div de aviso
        $('.callout').find("p").text(""); //limpar a div de aviso

        var btnEditar = $(this);

        $('#form :input').each(function(index,input){
            $('#'+input.id).val($(btnEditar).data(input.id));
        });

        
        jQuery('#criar_editar-modal').modal('show');
});
$(document).on('click', '.btnEditar', function() {
        $('.modal-footer .btn-action').removeClass('add');
        $('.modal-footer .btn-action').addClass('edit');
        $('.modal-footer .btn-action').removeClass('hidden');

        $('.modal-title').text('Editar Transação');
        $('.callout').addClass("hidden"); //ocultar a div de aviso
        $('.callout').find("p").text(""); //limpar a div de aviso

        //habilita os campos desabilitados
        $('#nome').prop('readonly',false);
        $('#sigla').prop('readonly',false);
        $('#email').prop('readonly',false);

        var btnEditar = $(this);

        $('#form :input').each(function(index,input){
            $('#'+input.id).val($(btnEditar).data(input.id));
        });

       

        for (var i = 0; i < ponto.length; i++) {
            $('.linha'+i).remove();
        }
        
        ponto.splice(0,ponto.length);
        
        for (var i = 0; i < ponto_unilevel.length; i++) {
            $('.linhaa'+i).remove();
        }

        ponto_unilevel.splice(0,ponto_unilevel.length);
        var id = $('#id').val();
       
        $.ajax({
            type: 'get',
            url: "/gerenciar-pacotes/nivel/"+id,
            processData: false,
            contentType: false,
            success: function(response) {
               console.log(response);
                for(var i = 0; i < response.data.length; i++){                  

                    var cols = '';
                    cols = '';
                    novaLinha = null; 
                    var nivel = response.data[i].nivel;
                    var valor = response.data[i].valor;

                    var novaLinha = '<tr class="'+'linha'+i+'">';
                    //Adc material ao array
                    ponto.push({'nivel': nivel, 'valor': valor});

                    /* Crian a linha p/ tabela*/
                    cols += '<td>'+nivel+'</td>';
                    cols += '<td>'+valor+'</td>';
                    cols += '<td class="text-left"><a class="btnRemoverPonto btn btn-xs btn-danger" data-indexof="'+ponto.indexOf(ponto[i])+'" data-linha="'+i+'"><i class="fa fa-trash"></i> Remover</a></td>';
                    novaLinha += cols + '</tr>';
                    
                    $('#pacote_id').append(novaLinha); /*Adc a linha  tabela*/
                }      
            },
        });

        $.ajax({
            type: 'get',
            url: "/gerenciar-pacotes/ponto/"+id,
            processData: false,
            contentType: false,
            success: function(response) {
               
                for(var i = 0; i < response.data.length; i++){                  
                    console.log('response'+response.data[i].id);

                    var cols = '';
                    cols = '';
                    novaLinha = null; 
                    var nivel = response.data[i].nivel;
                    var valor = response.data[i].valor;
    
                    var novaLinha = '<tr class="'+'linhaa'+i+'">';
                    //Adc material ao array
                    ponto_unilevel.push({'nivel': nivel, 'valor': valor});
                    /* Crian a linha p/ tabela*/
                    cols += '<td>'+nivel+'</td>';
                    cols += '<td>'+valor+'</td>';
                    cols += '<td class="text-left"><a class="btnRemoverPontoUnilevel btn btn-xs btn-danger" data-indexof="'+ponto_unilevel.indexOf(ponto_unilevel[i])+'" data-linhaa="'+i+'"><i class="fa fa-trash"></i> Remover</a></td>';
                    novaLinha += cols + '</tr>';
                    
                    $('#ponto_unilevel_id').append(novaLinha); /*Adc a linha  tabela*/
                }
                console.log(ponto_unilevel);      
            },
        });

        jQuery('#criar_editar-modal').modal('show'); //Abrir o modal
});
$(document).on('click', '.btnDeletar', function() {
   $('.modal-title').text('Deletar Transação');   
   $('.modal-footer .btn-action').removeClass('add');
   $('.modal-footer .btn-action').removeClass('edit');
   $('.modal-footer .btn-action').addClass('excluir');
   
   var btnExcluir = $(this);

    $('#deletar :input').each(function(index,input){
        $('#'+input.id).val($(btnExcluir).data(input.id));
    });

    jQuery('#criar_deletar-modal').modal('show'); //Abrir o modal 

});

});
