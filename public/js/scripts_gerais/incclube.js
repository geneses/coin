$(document).ready(function($) {
    
    var base_url = 'http://' + window.location.host.toString();
    var base_url = location.protocol + '//' + window.location.host.toString();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

   
    $(document).on('click', '.carrinho', function() {

        console.log('dados');
        $.ajax({
            type: 'post',
            url: "/clube/carrinho",
            data: {
                'qtd': $("#qtd").val(),
                'fk_produto': $("#fk_produto").val(),
                'valor': $("#valor").val(),
            },
            success: function(data) {
                 if ((!data.errors)) {

                 }
                 var qtd = $('#qtd').val();
                 var total = $('.contCarrinho').text();
                    total = parseInt(total)+ parseInt(qtd);
                    $('.contCarrinho').text(total);
            },
            error: function() {
                
            },

        });
    });

    $(document).on('click', '.comprar', function() {

            console.log('dados');
            $.ajax({
                type: 'get',
                url: "/clube/comprar",
                data: {
                    
                },
                beforeSend: function(){
                $('.comprar').prop('disabled',true);
                $('.comprar').html('Aguarde...');
                },
                complete: function() {
                    $('.comprar').prop('disabled',false);
                    $('.comprar').html('Finalizar compra');
                },
                success: function(data) {
                    window.location.reload();
                },
                error: function() {
                    
                },

            });
    });


    $(document).on('click', '.cadastrarUser', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/clube/cadastrar",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.cadastrarUser').prop('disabled',true);
                $('.cadastrarUser').html('Aguarde...');
            },
            complete: function() {
                $('.cadastrarUser').prop('disabled',false);
                $('.cadastrarUser').html('Cadastrar');
            },
            success: function(data) {

                $(function() {
                    iziToast.destroy();
                    iziToast.success({
                    title: 'OK',
                    message: 'Cadastro realizado com Sucesso!',
                }) });

                window.location = "/entrar";
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });


  
});

