$(document).ready(function($) {
    var base_url = 'http://' + window.location.host.toString();
    var base_url = location.protocol + '//' + window.location.host.toString();
    var patrocinadores = new Array();
    var nivel = 1;
    var usuario;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#tipo').change(function(){
        if($("#tipo :selected").text() == 'Jurídica'){
            $('#fisica').addClass('hidden');
            $('#juridica').removeClass('hidden');
        }else{ 
            $('#juridica').addClass('hidden');
            $('#fisica').removeClass('hidden'); 
        }    
    });

    var user_name = $("#user");
        user_name.focusout( function(){
                console.log('dados');
                $.ajax({
                    type: 'post',
                    url: "/gerenciar-usuarios/verifica-user",
                    data: {
                        'user': user_name.val(),
                    },
                    success: function(data) {
                        if(typeof data.nome == 'undefined'){
                            $('#salvar').prop('disabled',false);
                        }else{
                            alert('Username já existe')
                            $('#salvar').prop('disabled',true);
                        }
                    },
                    error: function() {
                        
                    },

                });
    });

    var email = $("#email");
        email.focusout( function(){
                console.log('dados');
                $.ajax({
                    type: 'post',
                    url: "/gerenciar-usuarios/verifica-email",
                    data: {
                        'email': email.val(),
                    },
                    success: function(data) {
                        if(typeof data.nome == 'undefined'){
                            $('#salvar').prop('disabled',false);
                        }else{
                            alert('Email já existe')
                            $('#salvar').prop('disabled',true);
                        }
                    },
                    error: function() {
                        
                    },

                });
    });

     var user = $("#patrocinador");
        user.focusout( function(){
                console.log('dados');
                $.ajax({
                    type: 'post',
                    url: "/gerenciar-usuarios/nome-user",
                    data: {
                        'user': user.val(),
                    },
                    success: function(data) {
                        if ((!data.errors)) {

                        }
                        $('#nome-user').removeClass('hidden');
                        $('#nome_user').val(data.nome);
                        
                        if(typeof data.nome == 'undefined'){
                            $('#salvar').prop('disabled',true);
                        }else{
                            $('#salvar').prop('disabled',false);
                        }
                    },
                    error: function() {
                        
                    },

                });
    });


$(document).on('click', '.termo', function() {
    $('#termo').modal('show');
});

$(document).on('click', '.usuario', function() {
      usuario = this.id;
      $.ajax({
        type: 'get',
        url: "/gerenciar-usuarios/rede-patrocinador/"+this.id,
        success: function(response) {
        
        for (var j = 0 ; j < patrocinadores.length; j++) {
            $('.linha'+ j).remove(); //Remove a linha da tela 
        }
        patrocinadores.splice(0,patrocinadores.length) ;
        nivel = 2;
        for(var i = 0; i < response.data.length; i++){
            console.log('---'+response.data[i].id);
            patrocinadores.push({'id':response.data[i].id });
            var cols = '';
            cols = '';
            novaLinha = null;
            
            /* Crian a linha p/ tabela*/  
            novaLinha = '<tr class="'+'linha'+i+'">'; 
            cols += '<td>'+nivel+'</td>';
            cols += '<td> <a id="'+response.data[i].user+'" class="usuario2" data-toggle="modal" href="#sdas">'+response.data[i].user+'</td>';
            cols += '<td>'+response.data[i].status+'</td>';
            cols += '<td>'+response.data[i].data_cadastro+'</td>';
            cols += '<td> <a id="'+usuario+'" class="voltar" data-toggle="modal" href="#s">Voltar</td>';
            novaLinha += cols + '</tr>';

        $('#table_patrocinador').append(novaLinha); /*Adc a linha  tabela*/
        }
        },
        error: function() {
            
        },

    });

    $('#indicados').removeClass('hidden');
});

$(document).on('click', '.usuario2', function() {
      usuario = this.id;
      $.ajax({
        type: 'get',
        url: "/gerenciar-usuarios/rede-patrocinador/"+this.id,
        success: function(response) {

        for (var i = 0 ; i < patrocinadores.length; i++) {
            $('.linha'+ i).remove(); //Remove a linha da tela 
        }
        patrocinadores = [];
        nivel = nivel +1;
        for(var i = 0; i < response.data.length; i++){
            patrocinadores.push({'id':response.data[i].id });
            var cols = '';
            cols = '';
            novaLinha = null; 
            var novaLinha = '<tr class="'+'linha'+i+'">';
            
            /* Crian a linha p/ tabela*/   
            cols += '<td>'+nivel+'</td>';
            cols += '<td> <a id="'+response.data[i].user+'" class="usuario2" data-toggle="modal" href="#sdas">'+response.data[i].user+'</td>';
            cols += '<td>'+response.data[i].status+'</td>';
            cols += '<td>'+response.data[i].data_cadastro+'</td>';
            cols += '<td> <a id="'+usuario+'" class="voltar" data-toggle="modal" href="#s">Voltar</td>';
            novaLinha += cols + '</tr>';

        $('#table_patrocinador').append(novaLinha); /*Adc a linha  tabela*/
        }
        },
        error: function() {
            
        },

    });

    $('#indicados').removeClass('hidden');
});

$(document).on('click', '.voltar', function() {
      if(nivel == 2){
        return;
      }
      usuario = this.id;
      $.ajax({
        type: 'get',
        url: "/gerenciar-usuarios/rede-patrocinador-voltar/"+this.id,
        success: function(response) {

        for (var i = 0 ; i < patrocinadores.length; i++) {
            $('.linha'+ i).remove(); //Remove a linha da tela 
        }
        patrocinadores = [];
        nivel = nivel -1;
        for(var i = 0; i < response.data.length; i++){
            patrocinadores.push({'id':response.data[i].id });
            var cols = '';
            cols = '';
            novaLinha = null; 
            var novaLinha = '<tr class="'+'linha'+i+'">';
            
            /* Crian a linha p/ tabela*/   
            cols += '<td>'+nivel+'</td>';
            cols += '<td> <a id="'+response.data[i].user+'" class="usuario2" data-toggle="modal" href="#sdas">'+response.data[i].user+'</td>';
            cols += '<td>'+response.data[i].status+'</td>';
            cols += '<td>'+response.data[i].data_cadastro+'</td>';
            cols += '<td> <a id="'+usuario+'" class="voltar" data-toggle="modal" href="#s">Voltar</td>';
            novaLinha += cols + '</tr>';

        $('#table_patrocinador').append(novaLinha); /*Adc a linha  tabela*/
        }
        },
        error: function() {
            
        },

    });

    $('#indicados').removeClass('hidden');
});

$(document).on('click', '.criarAvatar', function() {
    var dados = new FormData($("#form")[0]); //pega os dados do form
    $.ajax({
        type: 'post',
        url: "/gerenciar-usuarios/avatar",
        data: dados,
        processData: false,
        contentType: false,
        beforeSend: function(){
            $('.criarAvatar').prop('disabled',true);
            $('.criarAvatar').html('Aguarde...');
            },
        complete: function() {
            $('.criarAvatar').prop('disabled',false);
            $('.criarAvatar').html('Cadastrar');
        },
        success: function(data) {
            $(function() {
                    iziToast.destroy();
                    iziToast.success({
                        title: 'OK',
                        message: 'Avatar cadastrado com Sucesso!',
                    });
                });
        },

        error: function() {
            iziToast.error({
                title: 'Erro Interno',
                message: 'Operação Cancelada!',
            });
        },

    });
});

$(document).on('click', '.imagemPadrao', function() {
    var dados = new FormData($("#form")[0]); //pega os dados do form
    $.ajax({
        type: 'post',
        url: "/gerenciar-usuarios/imagem-padrao",
        data: dados,
        processData: false,
        contentType: false,
        beforeSend: function(){
            $('.imagemPadrao').prop('disabled',true);
            $('.imagemPadrao').html('Aguarde...');
            },
        complete: function() {
            $('.imagemPadrao').prop('disabled',false);
            $('.imagemPadrao').html('Cadastrar');
        },
        success: function(data) {
            $(function() {
                    iziToast.destroy();
                    iziToast.success({
                        title: 'OK',
                        message: 'Imagem cadastrado com Sucesso!',
                    });
                });
        },

        error: function() {
            iziToast.error({
                title: 'Erro Interno',
                message: 'Operação Cancelada!',
            });
        },

    });
});

$(document).on('click', '.cadastrar', function() {
       
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-usuarios/store",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.cadastrar').prop('disabled',true);
                $('.cadastrar').html('Aguarde...');
                },
            complete: function() {
                $('.cadastrar').prop('disabled',false);
                $('.cadastrar').html('Cadastrar');
            },
            success: function(data) {
                    window.location = "/entrar";
            },

            error: function(data) {
                 iziToast.error({
                    title: 'Error',
                    message: data.responseText,
                });

                $('#termo').modal('hide');
                $('.cadastrar').prop('disabled',false);
                $('.cadastrar').html('Cadastrar');
                $('.callout').removeClass('hidden'); //exibe a div de erro
                    $('.callout').find('p').text(""); //limpa a div para erros successivos
                    $.each(data.responseJSON.errors, function(nome, mensagem) {
                        console.log('adasdsa');
                            $('.callout').find("p").append(mensagem + "</br>");
                    });
               // console.log(data.responseJSON.errors);
               
                
            },

        });
    });

$(document).on('click', '.cadastrarPainel', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-usuarios/store",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.cadastrarPainel').prop('disabled',true);
                $('.cadastrarPainel').html('Aguarde...');
                },
            complete: function() {
                $('.cadastrarPainel').prop('disabled',false);
                $('.cadastrarPainel').html('Cadastrar');
            },
            success: function(data) {
                 $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Usuario cadastrado com Sucesso!',
                        });
                    });
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });

$(document).on('click', '.updateBanco', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-usuarios/banco",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.updateBanco').prop('disabled',true);
                $('.updateBanco').html('Aguarde...');
                },
            complete: function() {
                $('.updateBanco').prop('disabled',false);
                $('.updateBanco').html('Cadastrar');
            },
            success: function(data) {
                 $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Usuario alterado com Sucesso!',
                        });
                    });
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });

    });

$(document).on('click', '.alterarSenha', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-usuarios/senha",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.alterarSenha').prop('disabled',true);
                $('.alterarSenha').html('Aguarde...');
                },
            complete: function() {
                $('.alterarSenha').prop('disabled',false);
                $('.alterarSenha').html('Cadastrar');
            },
            success: function(data) {
                 $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Senha alterada com Sucesso!',
                        });
                    });
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });

$(document).on('click', '.saque', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-usuarios/criar-saque",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.saque').prop('disabled',true);
                $('.saque').html('Aguarde...');
                },
            complete: function() {
                $('.saque').prop('disabled',false);
                $('.saque').html('Cadastrar');
            },
            success: function(data) {
                 $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Saque solicitado com Sucesso!',
                        });
                    });
                 setTimeout(function () { location.reload(1); }, 2500);
            },

            error: function(data) {
                iziToast.error({
                    title: 'Erro',
                    message: data.responseText,
                });
            },

        });
    });

$(document).on('click', '.transferencia', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-usuarios/transferencia",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.transferencia').prop('disabled',true);
                $('.transferencia').html('Aguarde...');
                },
            complete: function() {
                $('.transferencia').prop('disabled',false);
                $('.transferencia').html('Cadastrar');
            },
            success: function(data) {
                 $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Transferência realizada com Sucesso!',
                        });
                    });
                 setTimeout(function () { location.reload(1); }, 2500);

            },

            error: function(data) {
                iziToast.error({
                    title: 'Erro Interno',
                    message: data.responseText,
                });
            },

        });
    });

$(document).on('click', '.updatePostUsuario', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        $.ajax({
            type: 'post',
            url: "/gerenciar-usuarios/updatePostUsuario",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('.updatePostUsuario').prop('disabled',true);
                $('.updatePostUsuario').html('Aguarde...');
                },
            complete: function() {
                $('.updatePostUsuario').prop('disabled',false);
                $('.updatePostUsuario').html('Cadastrar');
            },
            success: function(data) {
                 $(function() {
                        iziToast.destroy();
                        iziToast.success({
                            title: 'OK',
                            message: 'Usuário alterado com Sucesso!',
                        });
                    });
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });


   
  
});



