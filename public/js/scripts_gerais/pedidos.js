$(document).ready(function($) {
    
    var base_url = 'http://' + window.location.host.toString();
    var base_url = location.protocol + '//' + window.location.host.toString();
    var valor_total_frete = 0;
    var frete;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var nome = $('#usuario').val();
    var tabela = $('#table').DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            ajax: base_url+ '/gerenciar-financeiro/list_pedidos/'+nome,
            columns: [
            { data: null, name: 'order' },
            { data: 'codigo_pedido', name: 'codigo_pedido' },
            { data: 'usuario', name: 'usuario' },
            { data: 'status', name: 'status' },
            { data: 'valor', name: 'valor' },
            { data: 'acoes', name: 'acoes' }
            ],
            createdRow : function( row, data, index ) {
                row.id = "item-" + data.id;   
            },

            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: false,
            scrollX: true,
            sorting: [[ 1, "asc" ]],
            responsive: true,
            lengthMenu: [
                [10, 15, -1],
                [10, 15, "Todos"]
            ],
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "<div><i class='fa fa-circle-o-notch fa-spin' style='font-size:38px;'></i> <span style='font-size:20px; margin-left: 5px'> Carregando...</span></div>",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            columnDefs : [
              { targets : [2], sortable : false },
              { "width": "5%", "targets": 0 }, //nº
              { "width": "20%", "targets": 1 },//nome
              { "width": "20%", "targets": 2 },//nome
              { "width": "10%", "targets": 3 }//ação
            ]
    });

    tabela.on('draw.dt', function() {
        tabela.column(0, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
            cell.innerHTML = tabela.page.info().page * tabela.page.info().length + i + 1;
        });
    }).draw();

    $(document).on('click', '.pagar-pedido', function(){  

        $.ajax({
            type: 'get',
            url: "/gerenciar-financeiro/getPedido/"+$(this).data('codigo'),
            success: function(data) {
                console.log(data)
                var row = "";
                var valor_total = 0;
                var quantidades = 0;
                
                $.each(data.pedido, function(i, item) {
                    quantidades += item.quantidade;
                    valor_total = (item.quantidade * item.valor).toFixed(2);
                    row = row +"<tr><td>"+item.nome+"</td><td>"+item.quantidade+"</td><td> R$ "+item.valor+",00</td><td>R$ "+valor_total+"</td></tr>";
                });
                    valor_total_frete = parseFloat(data.total);
                    frete = parseFloat(data.frete);
                    //<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked=""></input>
                    var radio_frete = "<div class='radio col-md-12'><input type='radio' class='opcao' name='frete_valor' id='frete' value='frete'/></div>"
                    var radio_retirada = "<div class='radio col-md-12'><input type='radio' class='opcao' name='frete_valor' id='retirada' value='retirada'/></div>"
                    row = row+"<tr><td><strong>Forma de Envio</strong></td></tr><tr><td>Retirada</td><td>R$ 0.00</td><td></td><td td align='center'>"+radio_retirada+"</td></tr><tr><td>Frete</td><td>R$ "+data.frete.toFixed(2)+"</td><td></td><td align='center'>"+radio_frete+"</td></tr>";
                    row = row+"<tr class='success'><td colspan='3'><b>Total:</b></td><td id='total_pagar'>R$ "+data.total+",00</td></tr>";
                    //row = row+"<tr class='success'><td><strong>Total</strong<td></td><td>"+quantidades+"</td><td></td><td>R$ "+data.total+"</td></tr>";
                //$("#fk_user").val(data.usuario);
                $('#t-body').html(row);
                $('.modal-title').text('Pagar Pedido');
                //console.log($(this).data('codigo'))
                $('#codigo_pedido').val(data.codigo_pedido);
                jQuery('#confirmar-modal').modal('show');
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });

    $(document).on('click','.opcao',function(){
        var id = $(this).attr('id');
        //console.log();
        if($("#"+id).is(":checked") && id === 'retirada'){
            $("#total_pagar").text("R$ "+(valor_total_frete).toFixed(2))
        }
        else if($("#"+id).is(":checked") && id === 'frete'){
            $("#total_pagar").text("R$ "+(valor_total_frete + frete).toFixed(2))            
        }
        $("#tipo_retirada").val($(this).attr('id'))

    })

    $(document).on('click', '.pagar', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form
        console.log('dados');
        $.ajax({
            type: 'post',
            url: base_url+"/gerenciar-financeiro/finalizarCompra",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
            $('.pagar').prop('disabled',true);
            $('.pagar').html('Aguarde...');
            },
            complete: function() {
                $('.pagar').prop('disabled',false);
                $('.pagar').html('Gerar Pedido');
            },
            success: function(data) {
                window.location.reload();
            },
            error: function(data) {
            iziToast.error({
                title: 'Erro',
                message: data.responseText,
                });
            },

        });
});
});