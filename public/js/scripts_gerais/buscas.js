$(document).ready(function($) {
    
    var base_url = 'http://' + window.location.host.toString();
    var base_url = location.protocol + '//' + window.location.host.toString();


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var valores =  new Array();

    $(document).on('click', '.buscarExtrato', function() {
        console.log('asdasd');
        var dados = new FormData($("#form")[0]); //pega os dados do form

        console.log(dados);

        $.ajax({
            type: 'post',
            url: "/gerenciar-financeiro/extrato",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.add').button('loading');
            },
            complete: function() {
                jQuery('.add').button('reset');
            },
            success: function(response) {
                for (var i = 0; i < valores.length; i++) {
                    $('.linhaa'+i).remove();
                }
                var total = 0;
                var j = 0
                for(j; j < response.data.length; j++){  
                    valores.push('nivel');
                    var cols = '';
                    cols = '';
                    novaLinha = null; 
                    var novaLinha = '<tr class="'+'linhaa'+j+'">';

                    /* Crian a linha p/ tabela*/
                    cols += '<td>'+moment(response.data[j].created_at).format("DD/MM/YYYY")+'</td>';
                    cols += '<td>'+response.data[j].nome+'</td>';
                    cols += '<td>'+response.data[j].origem+'</td>';
                    if(response.data[j].acao == 1){
                        total += parseFloat(response.data[j].valor);
                        cols += '<td style="color: rgb(0, 128, 0);">R$ '+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else if(response.data[j].acao == 0){
                        cols += '<td style="color: red;">R$ - '+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else{
                        cols += '<td style="color: orange;">R$ '+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }

                    novaLinha += cols + '</tr>';
                    
                    $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                }
              
                valores.push('nivel');
                var cols = '';
                cols = '';
                novaLinha = null; 
                $('.linha'+1).remove();

                var novaLinha = '<tr class="'+'linha'+1+'">';

                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'Saldo:'+'</td>';
                cols+= '<td style="color: rgb(0, 128, 0);">'+total.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';

                novaLinha += cols + '</tr>';
                    
                $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });

    $(document).on('click', '.buscarExtratoGeral', function() {
        console.log('asdasd');
        var dados = new FormData($("#form")[0]); //pega os dados do form

        console.log(dados);

        $.ajax({
            type: 'post',
            url: "/gerenciar-financeiro/extrato-geral",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.add').button('loading');
            },
            complete: function() {
                jQuery('.add').button('reset');
            },
            success: function(response) {
                for (var i = 0; i < valores.length; i++) {
                    $('.linhaa'+i).remove();
                }
                var total = 0;
                var j = 0
                for(j; j < response.data.length; j++){  
                    valores.push('nivel');
                    var cols = '';
                    cols = '';
                    novaLinha = null; 
                    var novaLinha = '<tr class="'+'linhaa'+j+'">';

                    /* Crian a linha p/ tabela*/
                    cols += '<td>'+moment(response.data[j].created_at).format("DD/MM/YYYY")+'</td>';
                    cols += '<td>'+response.data[j].nome+'</td>';
                    cols += '<td>'+"<a  href='../login-conta-user/"+response.data[j].id_origem +"' title='Entrar'>"+response.data[j].origem+"</a>"+'</td>';
                    cols += '<td>'+"<a  href='../login-conta-user/"+response.data[j].id_destino +"' title='Entrar'>"+response.data[j].user+"</a>"+'</td>';
                    if(response.data[j].acao == 1){
                        total += response.data[j].valor;
                        cols += '<td style="color: rgb(0, 128, 0);">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else if(response.data[j].acao == 0){
                        cols += '<td style="color: red;"> - '+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else{
                        cols += '<td style="color: orange;">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }

                    novaLinha += cols + '</tr>';
                    
                    $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                }
              
                valores.push('nivel');
                var cols = '';
                cols = '';
                novaLinha = null; 
                $('.linha'+1).remove();

                var novaLinha = '<tr class="'+'linha'+1+'">';

                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'Saldo:'+'</td>';
                cols+= '<td style="color: rgb(0, 128, 0);">'+total.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';

                novaLinha += cols + '</tr>';
                    
                $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });

    $(document).on('click', '.buscarUserGeral', function() {
        console.log('asdasd');
        var dados = new FormData($("#form")[0]); //pega os dados do form

        console.log(dados);

        $.ajax({
            type: 'post',
            url: "/gerenciar-financeiro/user-extrato",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.add').button('loading');
            },
            complete: function() {
                jQuery('.add').button('reset');
            },
            success: function(response) {
                for (var i = 0; i < valores.length; i++) {
                    $('.linhaa'+i).remove();
                }
                var total = 0;
                var j = 0
                for(j; j < response.data.length; j++){  
                    valores.push('nivel');
                    var cols = '';
                    cols = '';
                    novaLinha = null; 
                    var novaLinha = '<tr class="'+'linhaa'+j+'">';

                    /* Crian a linha p/ tabela*/
                    cols += '<td>'+moment(response.data[j].created_at).format("DD/MM/YYYY")+'</td>';
                    cols += '<td>'+response.data[j].nome+'</td>';
                    cols += '<td>'+response.data[j].user+'</td>';
                    cols += '<td>'+response.data[j].origem+'</td>';
                    if(response.data[j].acao == 1){
                        total += response.data[j].valor;
                        cols += '<td style="color: rgb(0, 128, 0);">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else if(response.data[j].acao == 0){
                        cols += '<td style="color: red;"> - '+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else{
                        cols += '<td style="color: orange;">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }

                    novaLinha += cols + '</tr>';
                    
                    $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                }
              
                valores.push('nivel');
                var cols = '';
                cols = '';
                novaLinha = null; 
                $('.linha'+1).remove();

                var novaLinha = '<tr class="'+'linha'+1+'">';

                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'Saldo:'+'</td>';
                cols+= '<td style="color: rgb(0, 128, 0);">'+total.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';

                novaLinha += cols + '</tr>';
                    
                $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });

    $(document).on('click', '.buscaPacoteUser', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form

        console.log(dados);

        $.ajax({
            type: 'post',
            url: "/gerenciar-financeiro/user-extrato-pacote",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.add').button('loading');
            },
            complete: function() {
                jQuery('.add').button('reset');
            },
            success: function(response) {
                for (var i = 0; i < valores.length; i++) {
                    $('.linhaa'+i).remove();
                }
                var total = 0;
                var j = 0
                for(j; j < response.data.length; j++){  
                    valores.push('nivel');
                    var cols = '';
                    cols = '';
                    novaLinha = null; 
                    var novaLinha = '<tr class="'+'linhaa'+j+'">';

                    /* Crian a linha p/ tabela*/
                    cols += '<td>'+moment(response.data[j].created_at).format("DD/MM/YYYY")+'</td>';
                    cols += '<td>'+response.data[j].nome+'</td>';
                    cols += '<td>'+response.data[j].user+'</td>';
                    cols += '<td>'+response.data[j].origem+'</td>';
                    if(response.data[j].acao == 1){
                        total += response.data[j].valor;
                        cols += '<td style="color: rgb(0, 128, 0);">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else if(response.data[j].acao == 0){
                        cols += '<td style="color: red;"> - '+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else{
                        cols += '<td style="color: orange;">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }

                    novaLinha += cols + '</tr>';
                    
                    $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                }
              
                valores.push('nivel');
                var cols = '';
                cols = '';
                novaLinha = null; 
                $('.linha'+1).remove();

                var novaLinha = '<tr class="'+'linha'+1+'">';

                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'Saldo:'+'</td>';
                cols+= '<td style="color: rgb(0, 128, 0);">'+total.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';

                novaLinha += cols + '</tr>';
                    
                $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });

   $(document).on('click', '.buscaPacoteData', function() {
        console.log('asdasd');
        var dados = new FormData($("#form")[0]); //pega os dados do form

        console.log(dados);

        $.ajax({
            type: 'post',
            url: "/gerenciar-financeiro/extrato-pacote",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.add').button('loading');
            },
            complete: function() {
                jQuery('.add').button('reset');
            },
            success: function(response) {
                for (var i = 0; i < valores.length; i++) {
                    $('.linhaa'+i).remove();
                }
                var total = 0;
                var j = 0
                for(j; j < response.data.length; j++){  
                    valores.push('nivel');
                    var cols = '';
                    cols = '';
                    novaLinha = null; 
                    var novaLinha = '<tr class="'+'linhaa'+j+'">';

                    /* Crian a linha p/ tabela*/
                    cols += '<td>'+moment(response.data[j].created_at).format("DD/MM/YYYY")+'</td>';
                    cols += '<td>'+response.data[j].nome+'</td>';
                    cols += '<td>'+response.data[j].user+'</td>';
                    cols += '<td>'+response.data[j].origem+'</td>';
                    if(response.data[j].acao == 1){
                        total += response.data[j].valor;
                        cols += '<td style="color: rgb(0, 128, 0);">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else if(response.data[j].acao == 0){
                        cols += '<td style="color: red;"> - '+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else{
                        cols += '<td style="color: orange;">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }

                    novaLinha += cols + '</tr>';
                    
                    $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                }
              
                valores.push('nivel');
                var cols = '';
                cols = '';
                novaLinha = null; 
                $('.linha'+1).remove();

                var novaLinha = '<tr class="'+'linha'+1+'">';

                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'Saldo:'+'</td>';
                cols+= '<td style="color: rgb(0, 128, 0);">'+total.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';

                novaLinha += cols + '</tr>';
                    
                $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });

    $(document).on('click', '.buscaProdutoData', function() {
        var dados = new FormData($("#form")[0]); //pega os dados do form

        console.log(dados);

        $.ajax({
            type: 'post',
            url: "/gerenciar-produtos/extrato-pacote",
            data: dados,
            processData: false,
            contentType: false,
            beforeSend: function(){
                jQuery('.add').button('loading');
            },
            complete: function() {
                jQuery('.add').button('reset');
            },
            success: function(response) {
                for (var i = 0; i < valores.length; i++) {
                    $('.linhaa'+i).remove();
                }
                var total = 0;
                var j = 0
                for(j; j < response.data.length; j++){  
                    valores.push('nivel');
                    var cols = '';
                    cols = '';
                    novaLinha = null; 
                    var novaLinha = '<tr class="'+'linhaa'+j+'">';

                    /* Crian a linha p/ tabela*/
                    cols += '<td>'+moment(response.data[j].created_at).format("DD/MM/YYYY")+'</td>';
                    cols += '<td>'+response.data[j].nome+'</td>';
                    cols += '<td>'+response.data[j].user+'</td>';
                    cols += '<td>'+response.data[j].origem+'</td>';
                    if(response.data[j].acao == 1){
                        total += response.data[j].valor;
                        cols += '<td style="color: rgb(0, 128, 0);">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else if(response.data[j].acao == 0){
                        cols += '<td style="color: red;"> - '+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }else{
                        cols += '<td style="color: orange;">'+response.data[j].valor.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';
                    }

                    novaLinha += cols + '</tr>';
                    
                    $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                }
              
                valores.push('nivel');
                var cols = '';
                cols = '';
                novaLinha = null; 
                $('.linha'+1).remove();

                var novaLinha = '<tr class="'+'linha'+1+'">';

                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'</td>';
                cols+= '<td>'+'Saldo:'+'</td>';
                cols+= '<td style="color: rgb(0, 128, 0);">'+total.toLocaleString("pt-BR", {style: "currency", currency: "BRL", minimumFractionDigits: 2})+'</td>';

                novaLinha += cols + '</tr>';
                    
                $('#valores').append(novaLinha); /*Adc a linha  tabela*/
                
            },

            error: function() {
                iziToast.error({
                    title: 'Erro Interno',
                    message: 'Operação Cancelada!',
                });
            },

        });
    });
    
});

