@extends('adminlte::page')


<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>


<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>

<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type = "text/javascript"></script>

<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>

<script src="{{ asset('js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/moeda.js') }}" type="text/javascript"></script>
<link href="{{ asset('css/jquery-te.css') }}" rel="stylesheet" type="text/css"/>
<script src ="{{ asset('/js/scripts_gerais/frete.js') . '?update=' . str_random(3) }}" type = "text/javascript" ></script>
@section('htmlheader_title')
	Gerenciar Fretes
@endsection

@section('main-content')

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Fretes</h3>
                       
                       <div class="pull-right">      
                            <a class="btnAdicionar btn btn-primary btn-sm" title="Adicionar Frete" data-toggle="tooltip"><span class="glyphicon glyphicon-plus"></span> Cadastrar Frete</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table" id="table">
                            <thead>
                            <tr>
                                <th>Nº</th>
                                <th>Titulo</th>   
                                <th>Faixa de Peso</th>
                                <th>Valor</th>
                                <th width="20%">Ações</th>
                            </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>


@endsection


@include('frete.modals.criar_editar-modal')
@include('frete.modals.deletar_frete-modal')