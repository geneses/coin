<div id="criar_editar-modal" class="modal fade bs-example" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div> <!-- Fim de ModaL Header-->

      <div class="modal-body">

        <div class="erros callout callout-danger hidden">
                <p></p>
        </div>
       <form class="form-horizontal" role="form" id="form" enctype="multipart/form-data" >
                    <div class="form-group">
                        <div class="col-sm-12">
                            <strong>Titulo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input onpaste="return false;" type="text" maxlength="254" class="form-control" name="titulo"  id="titulo">
                            </div>       
                        </div>
   
                        <div class="col-sm-4">
                            <strong>Peso Inicial:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Peso Inicial" class="input-group-addon"><i class="fa fa-balance-scale"></i></span>
                                <input  type="text"  maxlength="254" class="form-control" name="peso_inicial"  id="peso_inicial">
                            </div>       
                        </div>

                         <div class="col-sm-4">
                            <strong>Peso Final:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Peso Final" class="input-group-addon"><i class="fa fa-balance-scale"></i></span>
                                <input type="text"  maxlength="13"  class="form-control" name="peso_final"  id="peso_final">
                            </div>       
                        </div>

                    
                        <div class="col-sm-4">
                            <strong>Valor fo Frete:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor" class="input-group-addon"><i class="fa fa-money"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="13" onkeydown="FormataMoeda(this, 10, event)" class="form-control" name="valor"  id="valor">
                            </div>       
                        </div>

                        <div class="col-sm-12">
                            <strong>Observaçẽs:</strong>
                                <textarea name="observacoes" id="observacoes" rows="5" placeholder="Opcional" class="form-control"></textarea>
                        </div>

                    </div>
                  
                        <input type="hidden" id="id" name="id">
                </form>

      </div> <!-- Fim de ModaL Body-->


      <div class="modal-footer">
        <button type="button" class="btn btn-action btn-success add" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Cadastrar
          <i class="fa fa-floppy-o"> </i>
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <i class='fa fa-times'></i>
        </button>
      </div> <!-- Fim de ModaL Footer-->

    </div> <!-- Fim de ModaL Content-->

  </div> <!-- Fim de ModaL Dialog-->

</div> <!-- Fim de ModaL Usuario-->