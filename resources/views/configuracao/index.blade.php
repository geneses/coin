@extends('adminlte::page')


<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>


<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/configuracoes.js') . '?update=' . str_random(3) }}" type = "text/javascript" ></script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>

<script src="{{ asset('js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<link href="{{ asset('css/jquery-te.css') }}" rel="stylesheet" type="text/css"/>
@section('htmlheader_title')
	Configurações Gerais
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Configurações Gerais</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                           <form role="form" id="form" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <h4>Geral</h4>
                                    </div>
                                
                                    <div class="form-group col-md-4">
                                        <label for="">Nome da Empresa</label>
                                        <input type="text" name="nome_empresa" id="nome_empresa" class="form-control" value="{{$configuracao->nome_empresa ?? null}}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Email da Empresa</label>
                                        <input type="text" class="form-control" name="email_empresa" id="email_empresa" value="{{$configuracao->email_empresa ?? null}}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="logo">Logo</label>
                                        <input type="file" name="logo_empresa" id="logo_empresa" class="form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <h4>Site</h4>
                                    </div>
                                
                                    <div class="form-group col-md-4">
                                        <label for="">Nome - Site</label>
                                        <input type="text" class="form-control" name="nome_site" id="nome_site" value="{{$configuracao->nome_site ?? null}}" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Email - Site</label>
                                        <input type="text" class="form-control" name="email_site" id="email_site" value="{{$configuracao->email_site ?? null}}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="logo">Logo - Site</label>
                                        <input type="file" name="logo_site" id="logo_site" class="form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <h4>Escritório</h4>
                                    </div>
                                
                                    <div class="form-group col-md-4">
                                        <label for="">Nome - Escritório</label>
                                        <input type="text" class="form-control" name="nome_escrito" id="nome_escrito" value="{{$configuracao->nome_escrito ?? null}}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Email - Escritório</label>
                                        <input type="text" class="form-control" name="email_escritorio" id="email_escritorio" value="{{$configuracao->email_escritorio ?? null}}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="logo">Logo - Escritório</label>
                                        <input type="file" name="logo_escritorio" id="logo_escritorio" class="form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <h4>Loja</h4>
                                    </div>
                                
                                    <div class="form-group col-md-4">
                                        <label for="">Nome - loja</label>
                                        <input type="text" class="form-control" name="nome_loja" id="nome_loja" value="{{$configuracao->nome_loja ?? null}}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Email - loja</label>
                                        <input type="email" class="form-control" name="email_loja" id="email_loja" value="{{$configuracao->email_loja ?? null}}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="logo">Logo - loja</label>
                                        <input type="file" name="logo_loja" id="logo_loja" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <h4 for="">Termos de Uso:</h4>
                                        <textarea name="termos_uso" id="termos_uso" class="form-control" rows="6">{{$configuracao->termos_uso ?? null}}</textarea>
                                    </div>
                                    <div class="col-md-12 ">
                                        <a class="salvar pull-right btn btn-success">Salvar</a>
                                    </div>
                                </div>
                           </form>
                            
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>


@endsection

