@extends('adminlte::page')

<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/user.js') }}" type = "text/javascript" ></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type = "text/javascript"></script>

<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>

@section('htmlheader_title')
	Buscar Usuário
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box box-success">

                    <div class="box-header with-border">
                        <h3 class="box-title">Produtos</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       <table>
                           <tr>
                               <td>Nome </td>
                               <td> {{$produto->nome}}</td>
                           </tr> 
                       </table>                    
                    </div>
                    
<div class="pull-right">      
                            <a class="btnPagar btn btn-primary btn-sm" title="Pagar usuario" data-toggle="tooltip"><span class="glyphicon glyphicon-plus"></span> Cadastrar Tipo Transação</a>
                        </div>
                    <!-- /.box-body -->   
                </div>

			</div>
		</div>
	</div>
@include('produtos.modals.pagar')
@endsection
