<div id="criar_editar-modal" class="modal fade bs-example" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div> <!-- Fim de ModaL Header-->

      <div class="modal-body">

        <div class="erros callout callout-danger hidden">
                <p></p>
        </div>
       <form class="form-horizontal" role="form" id="form" enctype="multipart/form-data" >
                    <div class="form-group">
                        <div class="col-sm-6">
                            <strong>Nome:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input onpaste="return false;" type="text" maxlength="254" class="form-control" name="nome"  id="nome">
                            </div>       
                        </div>
   
                        <div class="col-sm-2">
                            <strong>Estoque:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Estoque" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input  type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="estoque"  id="estoque">
                            </div>       
                        </div>

                        <div class="col-sm-2">
                            <strong>Valor do PA:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor do PA" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" onkeydown="FormataMoeda(this, 10, event)" maxlength="13" class="form-control" name="valor_v_pa"  id="valor_v_pa">
                            </div>       
                        </div>
        
                        <div class="col-sm-2">
                            <strong>Valor do cd:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor do cd" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="13" class="form-control" name="valor_v_cd"  id="valor_v_cd">
                            </div>       
                        </div>
                    </div>

                     <div class="form-group">
                        <div class="col-sm-4">
                            <strong>Valor da compra:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor da compra" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="13" onkeydown="FormataMoeda(this, 10, event)" class="form-control" name="valor_compra"  id="valor_compra">
                            </div>       
                        </div>

                        <div class="col-sm-2">
                            <strong>Valor do site:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor do site" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="13" onkeydown="FormataMoeda(this, 10, event)" class="form-control" name="valor_v_site"  id="valor_v_site">
                            </div>       
                        </div>
        
                        <div class="col-sm-2">
                            <strong>Valor do usuário:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor do usuario" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="13" onkeydown="FormataMoeda(this, 10, event)" class="form-control" name="valor_v_usuario"  id="valor_v_usuario">
                            </div>       
                        </div>

                        <div class="col-sm-2">
                            <strong>Pontos binário:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Pontos binário" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="pontos_binario"  id="pontos_binario">
                            </div>       
                        </div>

                         <div class="col-sm-2">
                            <strong>Pontos unilevel:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Pontos unilevel" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="ponto_unilevel"  id="ponto_unilevel">
                            </div>       
                        </div>
        
                        <div class="col-sm-2">
                            <strong>Pontos carreira:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Pontos carreira" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="pontos_carreira"  id="pontos_carreira">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>Imagem:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Imgem" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="file" maxlength="254" class="form-control" name="imagem"  id="imagem">
                            </div>       
                        </div>
                        <div class="col-sm-3">
                            <strong>Comissão do produto:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Comissão do produto" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="comissao_produto"  id="comissao_produto">
                            </div>       
                        </div>  

                        <div class="col-sm-4">
                            <strong>Categoria:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Comissão do produto" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select id="fk_categoria" name="fk_categoria" class="form-control">
                                    @foreach($categorias as $categoria)
                                    <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                                    @endforeach
                                </select>
                            </div>       
                        </div> 

                        <div class="col-sm-4">
                            <strong>Ativo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Comissão do produto" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select id="ativo" name="ativo" class="form-control">
                                    <option value="true">Ativo</option>
                                    <option value="false">Inativo</option>
                                </select>
                            </div>       
                        </div>
                    </div>
                    
                    <div class="col-sm-12">
                        <strong>Descrição:</strong>
                        <div class="input-group">
                             <textarea id="descricao" name="descricao"></textarea>
                        </div>       
                    </div>

                    <script>
                        CKEDITOR.replace('descricao');
                    </script>

                    <div class="col-sm-9">
                        <strong>Peso:</strong>
                        <div class="input-group">
                            <span data-toggle="tooltip" title="Peso" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                            <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="peso"  id="peso">
                        </div>       
                    </div>
                    
                    

                    <div class="form-group">
                        <div class="col-sm-8">
                            <strong>Nível</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nível" class="input-group-addon"><i class="fa fa-cube"></i></span>
                                <select class="form-control" name="nivel" id="nivel">
                                    <option>Nivel 1</option>
                                    <option>Nivel 2</option>
                                    <option>Nivel 3</option>
                                    <option>Nivel 4</option>
                                    <option>Nivel 5</option>
                                    <option>Nivel 6</option>
                                    <option>Nivel 7</option>
                                    <option>Nivel 8</option>
                                    <option>Nivel 9</option>
                                    <option>Nivel 10</option>
                                    <option>Nivel 11</option>
                                    <option>Nivel 12</option>
                                    
                                </select>
                            </div>       
                        </div>
                    
                        <div class="col-sm-3">
                            <strong>Pontos</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Horário pausa" class="input-group-addon"><i class="fa fa-bars"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="ponto" id="ponto">
                            </div>       
                        </div>

                        <div class="col-sm-1">
                            
                                <a class="btnAdcUnilevel btn btn-sm btn-primary" style="margin-top: 22px; padding: 11px"><i class="glyphicon glyphicon-plus"></i></a>
                        </div>
                    </div>

                    <div class="col-md-12">
                    <br>
                        <table class="table table-bordered table-responsive" id="materiais">
                            <tr>
                                <th>Nível</th>
                                <th>Pontos</th>
                                <th>Ações</th>
                            </tr>
                            <tbody id="material_id">
                            </tbody>
                        </table>
                    </div>
                  


                    <div class="form-group">
                        <div class="col-sm-8">
                            <strong>Fidelidade Nível</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="FidelidadeNível" class="input-group-addon"><i class="fa fa-cube"></i></span>
                                <select class="form-control" name="nivel1" id="nivel1">
                                    <option>Nivel 1</option>
                                    <option>Nivel 2</option>
                                    <option>Nivel 3</option>
                                    <option>Nivel 4</option>
                                    <option>Nivel 5</option>
                                    <option>Nivel 6</option>
                                    <option>Nivel 7</option>
                                    <option>Nivel 8</option>
                                    <option>Nivel 9</option>
                                    <option>Nivel 10</option>
                                    <option>Nivel 11</option>
                                    <option>Nivel 12</option>
                                    
                                </select>
                            </div>       
                        </div>
                    
                        <div class="col-sm-3">
                            <strong>Pontos</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Horário pausa" class="input-group-addon"><i class="fa fa-bars"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="ponto1" id="ponto1">
                            </div>       
                        </div>

                        <div class="col-sm-1">
                                <a class="btnAdcFidelidade btn btn-sm btn-primary" style="margin-top: 22px; padding: 11px"><i class="glyphicon glyphicon-plus"></i></a>
                        </div>
                    </div>

                    <div class="col-md-12">
                    <br>
                        <table class="table table-bordered table-responsive" id="materiais">
                            <tr>
                                <th>Nível</th>
                                <th>Pontos</th>
                                <th>Ações</th>
                            </tr>
                            <tbody id="fidelidade_id">
                            </tbody>
                        </table>
                    </div>
                            <input type="hidden" id="id" name="id">
                </form>

      </div> <!-- Fim de ModaL Body-->


      <div class="modal-footer">
        <button type="button" class="btn btn-action btn-success add" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Cadastrar
          <i class="fa fa-floppy-o"> </i>
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <i class='fa fa-times'></i>
        </button>
      </div> <!-- Fim de ModaL Footer-->

    </div> <!-- Fim de ModaL Content-->

  </div> <!-- Fim de ModaL Dialog-->

</div> <!-- Fim de ModaL Usuario-->