@extends('fidelidade/layouts/template')
@section('title', 'Detalhe do produto')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- ========================= SECTION TOPBAR ========================= -->
<section class="section-topbar border-top padding-y-sm">
<div class="container">
	<span>	<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('fidelidade.index') }}">Início</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detalhe do produto</li>
</ol> </span>
	<div class="float-right dropdown">
	
	</div> 
</div> <!-- container.// -->
</section>
<!-- ========================= SECTION TOPBAR .// ========================= -->

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content bg padding-y-sm">
<div class="container">

<div class="row">
<div class="col-xl-12 col-md-12 col-sm-12">
<main class="card">
	<div class="row no-gutters">
		<aside class="col-sm-6 border-right">
<article class="gallery-wrap"> 
<div class="img-big-wrap">

<div> <a href=" {{ asset($produto->imagem) }}" data-fancybox="">
	<img src=" {{ asset($produto->imagem) }}"></a>
</div>

</div>

<div class="img-small-wrap">
	@foreach ($fotos as $valor){  
		<div class="item-gallery"><a href=" {{ asset($value->imagem) }}" data-fancybox=""> <img src="{{ asset($value->imagem) }}"/></a></div>
	@endforeach
</div> 

</article> 
		</aside>
		<aside class="col-sm-6">
<article class="card-body">

	<h3 class="title mb-3">{{ $produto->nome_produto }}</h3>

<div class="mb-3"> 
	<var class="price h3 text-warning" > 
		<span style="font-size: 20px" class="currency" >  </span><span style="font-size: 20px" class="num">
		</span><br>
	</var> 
</div> <!-- price-detail-wrap .// -->

<dl class="row">
 <strong style="font-size: 30px; margin-left: 15px; margin-top: -30px">R$@if(Auth::user())
                @if(!Auth::user()->hasRole('usuario_site'))
				{{  $produto->valor_ponto_fidelidade }}
				
				@else
				{{  $produto->valor_ponto_fidelidade }}
				@endif
				
				@else
				{{  $produto->valor_ponto_fidelidade }}
			@endif</strong>

</dl>

<hr>
	<div class="row">
		<div class="col-sm-5">
			<dl class="dlist-inline">
			  <dt>Quantidade: </dt>
			  <dd> 
			  	{{ $produto->estoque }}
			  </dd>
			</dl>  <!-- item-property .// -->
		</div> <!-- col.// -->
		
	</div> <!-- row.// -->
	<hr>
		<input type="hidden" id="fk_produto" name="fk_produto" value="{{$produto->id}}">
		<input type="hidden" id="valor" name="valor" value="{{$produto->valor_ponto_fidelidade}}">
		<input type="numer" id="qtd" name="qtd" >
		<a href="" class="btn  btn-warning carrinho" data-toggle="modal" data-target="#">  <i class="fa fa-cart-plus"></i> Adicionar ao carrinho</a>
	
	<!--	<a href="#" class="btn" style="background-color: red; color: white"><i class="fa fa-envelope"></i> Ação não permitida</a>	
	
		<a href="" class="btn  btn-warning" data-toggle="modal" data-target="#pergunta">  <i class="fa fa-envelope"></i> Enviar mensagem</a>
-->

<!-- short-info-wrap .// -->
</article> <!-- card-body.// -->
		</aside> <!-- col.// -->
	</div> <!-- row.// -->
</main> <!-- card.// -->

<!-- PRODUCT DETAIL -->
<article class="card mt-3">
	<div class="card-body">
		<h4>Descrição</h4>
	<p>	{!! $produto->descricao !!}</p>

	</div> <!-- card-body.// -->
</article> <!-- card.// -->

<!-- PRODUCT DETAIL .// -->

</div> <!-- col // -->

</div> <!-- row.// -->



</div><!-- container // -->
</section>
<!-- ========================= SECTION CONTENT .END// ========================= -->


@endsection