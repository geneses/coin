@extends('fidelidade/layouts/template')
@section('title', 'Index')
@section('content')

<style type="text/css">
.tales {
    width: 100%;
}

.carousel-inner {
    width: 100%;
    max-height: 400px !important;
}

.imagens {
    width: 100%;
    max-height: 400px !important;
}
</style>

<!-- ========================= SECTION CONTENT ========================= -->

<div id="carousel2_indicator" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">
        <?php $i=0  ?>
        @foreach ($banner as $value)
        <?php if ($i==0){ ?>
        <div class="carousel-item active">
            <?php }else{ ?>
            <div class="carousel-item">
                <?php } ?>
                @if($value->link == '')
                <a href="#">
                    <img class="d-block w-100 imagens" src=" {{ asset($value->imagem) }}" alt="First slide">
                </a>
                @else
                <a href="{{ $value->link }}" target="_blank">
                    <img class="d-block w-100 imagens" src=" {{ asset($value->imagem) }}" alt="First slide">
                </a>
                @endif

            </div>
            <?php $i++ ?>
            @endforeach

        </div>
        <a class="carousel-control-prev" href="#carousel2_indicator" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel2_indicator" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


    <section class="section-content bg padding-y-sm">
        <div class="container">

            <div class="row-sm">
                @foreach($produtos as $value)
                <div class="col-md-3 col-sm-6">
                    <figure class="card card-product">
                        <div class="img-wrap"><a href="{{ route('fidelidade.detalhe',$value->id) }}"><img width="100%"
                                    src="{{ asset($value->imagem) }}"></div></a>
                        <figcaption class="info-wrap text-center">
                            <a href="{{ route('fidelidade.detalhe',$value->id) }}" class="title"
                                style="font-size: 15px; color: black">{{$value->nome }}</a>
                            <div class="price-wrap text-center">
                                <span class="price-new"><strong style="font-size: 20px">R$
                                        @if(Auth::user())
                                        @if(Auth::user()->hasRole('usuario_v_usuario'))
                                        {{  $value->valor_v_usuario }}

                                        @else
                                        {{  $value->valor_v_site }}
                                        @endif

                                        @else
                                        {{  $value->valor_v_site }}
                                        @endif
                                    </strong></span><br>
                            </div> <!-- price-wrap.// -->
                        </figcaption>
                    </figure> <!-- card // -->
                </div> <!-- col // -->

                @endforeach


            </div> <!-- row.// -->


        </div><!-- container // -->
    </section>

    @endsection




    <!-- custom javascript 
<script type="text/javascript" url="{{ asset('/incclube/js/bootstrap-notify.min.js') }}"></script>
<script type="text/javascript" url="{{ asset('/incclube/js/sweetalert.min.js') }}"></script>
<script type="text/javascript" url="{{ asset('/incclube/js/jquery-te.js') }}"></script>

<script url="{{ asset('/incclube/js/script.js') }}" type="text/javascript"></script>


<link href="{{ asset('/incclube/css/responsive.css') }}" rel="stylesheet" media="only screen and (max-width: 1200px)" />
-->