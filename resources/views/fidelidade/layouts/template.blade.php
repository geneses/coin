<!DOCTYPE HTML>
<html lang="pt-br">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="fidelilidade">

<title>Fidelidade</title>
	
<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/loja_fidelidade.js') . '?update=' . str_random(3)   }}" type = "text/javascript" ></script>
<script src="{{ asset('js/iziToast.min.js') }}"></script>

<link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">

<!-- jQuery -->
<script src="{{ asset('/incclube/js/jquery-2.0.0.min.js') }}" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="{{ asset('/incclube/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<link href="{{ asset('/incclube/css/bootstrap-custom.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/incclube/css/jquery-te.css') }}" rel="stylesheet" type="text/css"/>

<!-- Font awesome 5 -->
<link href="{{ asset('/incclube/fonts/fontawesome/css/fontawesome-all.min.css') }}" type="text/css" rel="stylesheet">

<!-- plugin: fancybox  -->
<script src="{{ asset('/incclube/plugins/fancybox/fancybox.min.js') }}" type="text/javascript"></script>
<link href="{{ asset('/incclube/plugins/fancybox/fancybox.min.css') }}" type="text/css" rel="stylesheet">

<!-- plugin: owl carousel  -->
<link href="{{ asset('/incclube/plugins/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{ asset('/incclube/plugins/owlcarousel/assets/owl.theme.default.css') }}" rel="stylesheet">
<script src="{{ asset('/incclube/plugins/owlcarousel/owl.carousel.min.js') }}"></script>

<!-- custom style -->
<link href="{{ asset('/incclube/css/uikit.css') }}" rel="stylesheet" type="text/css"/>

<meta name="csrf-token" content="{{ csrf_token() }}"> 

<style type="text/css">
	a.nav-link{
		color:white;
	}
</style>

</head>
<body>
<header class="section-header">
<section class="header-main">
	<div class="container">
<div class="row align-items-center">
	<div class="col-lg-3">
	<div class="brand-wrap">
		<img class="logo" src="{{ asset('incclube/images/logo12.png') }}">
		
	</div> <!-- brand-wrap.// -->
	</div>
	<div class="col-lg-6 col-sm-6">
		<form method="post" class="search-wrap" action="{{ route('buscar.produto') }}" >
			{{csrf_field()}}
			<div class="input-group">
			    <input type="text" class="form-control" placeholder="Digite o nome do produto" name="produto" required="" id="produto">
			    <div class="input-group-append">
			      <button class="btn btn-primary" type="submit" style="background-color: #003dbc; color: white; border-color: #538dbf">
			        <i class="fa fa-search"></i>
			      </button>
			    </div>
		    </div>
		</form> <!-- search-wrap .end// -->
	</div> <!-- col.// -->
	<div class="col-lg-3 col-sm-6">
		<div class="widgets-wrap d-flex justify-content-end">
			<div class="widget-header">
			Seu Saldo R$: {{ Auth::user()->ponto_fidelidade }}		
			</div>
		

			<div class="widget-header dropdown">
				<?php if(Auth::user()){  ?>
				@inject('carrinhos', 'App\FidelidadeCarrinho')
                    <?php  
                    $carrinho = $carrinhos::JOIN('users','users.id','=','fidelidade_carrinhos.fk_user')
                    ->SELECT('fidelidade_carrinhos.quantidade')
                    ->WHERE('users.id',Auth::user()->id) 
                    ->WHERE('fidelidade_carrinhos.codigo_pedido',null)
                    ->SUM('fidelidade_carrinhos.quantidade')
                    ->get();
                    $qtd = 0;
                    foreach ($carrinho as $value) {
                    	$qtd += $value->quantidade;
                    }
                    ?>
               <?php }else{ $qtd = 0;} ?>  
				<a href="{{ route('fidelidade.carrinho') }}" class="btn btn-warning">  <i class="fa fa-cart-plus contCarrinho"> <?php echo $qtd;  ?></i></a>
				
				<!--
				<a href="cadastro_produto.php" class="ml-3 icontext"  data-offset="20,10">
					<div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-user"></i></div>
					<div class="text-wrap">
						<small>Olá </small>
						<span>Minha conta </span>
					</div>
				</a>
				
				<a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10">
					<div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-user"></i></div>
					<div class="text-wrap">
						
						<span>Login <i class="fa fa-caret-down"></i></span>
					</div>
				</a>
		
				-->
				<div class="dropdown-menu dropdown-menu-right">
					<form class="px-4 py-3" method="post">
						<div class="form-group">
						  <label>Username</label>
						  <input type="text" class="form-control" name="username">
						</div>
						<div class="form-group">
						  <label>Senha</label>
						  <input type="password" class="form-control" name="senha">
						</div>
						<button type="submit" class="btn btn-primary" name="entrar">Entrar</button>
						</form>
						<hr class="dropdown-divider">

						<!--
						<a class="dropdown-item" href="#">Have account? Sign up</a>
						<a class="dropdown-item" href="#">Forgot password?</a>	-->

				</div> <!--  dropdown-menu .// -->
			</div> <!-- widget  dropdown.// -->

			<div class="widget-header">

				@guest
                            <li><a href="{{ route('entrar') }}">Login</a></li>
                            <li><a href="{{ route('register.clube') }}">Registrar</a></li>
                        @else
                        
                        {{ Auth::user()->name }} <span class="caret"></span>                             
                            <a href="{{ route('logout') }}"  class="btn" style="background-color: blue; color: white" 
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                                 
                            </li>
                        @endguest
			</div> <!-- widget .// -->

		</div>	<!-- widgets-wrap.// -->	
	</div> <!-- col.// -->
</div> <!-- row.// -->
	</div> <!-- container.// -->
</section> <!-- header-main .// -->
</header>
    <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #00548f;">
  <div class="container">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="main_nav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link pl-0" href="{{ route('fidelidade.index') }}" style="color: white"> <strong>Início</strong></a>
        </li>
      	<li class="nav-item dropdown" style="color: white">
          <a class="nav-link dropdown-toggle" href="#" id="dropdown07" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white">Categorias</a>
          <div class="dropdown-menu" aria-labelledby="dropdown07">
            @inject('categorias', 'App\FidelidadeCategoria')
                    <?php  $categorias = $categorias::orderBy('created_at', 'desc')->get() ?>
            @foreach ($categorias as $value)
            	<a class="dropdown-item" href="{{ route('fidelidade-produtos-categoria',$value->id) }}">
            		{{ $value->nome }} ({{$value->quantidade_produtos}})
            	</a>
            @endforeach
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link pl-0" href="{{ route('fidelidade-todos-produtos') }}" style="color: white">Todos os produtos</a>
        </li> 

        <li class="nav-item">
          <a class="nav-link pl-0" href="{{ route('dashboard') }}" style="color: white">Painel administrativo</a>
        </li>
       
        <!--
        <li class="nav-item">
          <a class="nav-link" href="perguntas.php"style="color: white">Mensagens</a>
        </li>
      -->

		<a id="demoNotify" href="#"></a>   
      </ul>
    </div> <!-- collapse .// -->
  </div> <!-- container .// -->
</nav>
  </div> <!-- container .// -->
</nav>

        @yield('content')
<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-secondary">
	<div class="container">
		
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50"> Desenvolvido por: fidelilidade</p>
			</div>
			<div class="col-sm-6">
				<p class="text-md-right text-white-50">
	Copyright &copy  <br>
<a href="http://bootstrap-ecommerce.com" class="text-white-50">fidelilidade-ecommerce</a>
				</p>
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
</html>