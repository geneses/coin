@extends('fidelidade/layouts/template')
<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<script src="{{ asset('js/scripts_gerais/carrinho.js') }}"></script>
@section('title', 'Carrinho')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">

<section class="section-content bg padding-y-sm">
<div class="container">

<div class="row-sm">
  <table class="table">
    <tr>
      <td></td>
      <td>Nome</td>
      <td>Valor</td>
      <td>Quantidade</td>
      <td>Ação</td>
    </tr>
  @foreach($carrinho as $item)
    <tr>  
      <td><img src=" {{ asset($item->imagem) }}" width="100px" height="100px"></td>
      <td>{{ $item->nome }}</td>
      <td>R$ {{ $item->valor }}</td>
      <td>{{ $item->quantidade }}</td>
      <td><button class="btn btn-danger" id="delete"  value="{{$item->id}}">Excluir</button></td>
    </tr>
  @endforeach
  <tr>
    <th></th>
    <th>Total</th>
    <th>R$ {{ $carrinho->sum('valor') * $carrinho->sum('quantidade') }}</th>
    <th>{{ $carrinho->sum('quantidade') }}</th>
    <th></th>
  </tr>
  </table>
  <div class="col-12">
      
    <button class="btn btn-success float-right confirmar_compra"  type="button">Finalizar compra</button>
    <!-- <button class="btn btn-primary float-right gerar_pedido" style="margin-right:10px"  type="button">Gerar pedido</button> -->
  </div>
</div> <!-- row.// -->
</div><!-- container // -->
</section>
@endsection
@include('fidelidade.carrinho.modals.confirmar_compra')
