@extends('adminlte::page')

<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/pedidos.js') }}" type = "text/javascript" ></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type = "text/javascript"></script>

<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 
@section('htmlheader_title')
	Gerenciar Pedidos
@endsection

@section('main-content')

<form method="post" action="{{ route('pedidos.post') }}">
     {{csrf_field()}}  
    <input type="text" name="nome" id="nome">
    <button>Buscar Pedido</button>
</form>

<input type="hidden" id="usuario" name="usuario" value="{{$nome}}">
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pedidos</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                    
                        <table class="table" id="table">
                            <thead>

                            <tr>
                                <th>Nº</th> 
                                <th>Código</th>   
                                <th>Usuário</th>
                                <th>Status</th>   
                                <th>Valor</th>
                                <th width="20%">Ações</th>
                            </tr>
                            </thead>
                            
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@include('pedidos.modals.pagar')
@endsection
