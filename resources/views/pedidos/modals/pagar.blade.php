<div id="confirmar-modal" class="modal fade bs-example" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div> <!-- Fim de ModaL Header-->

      <div class="modal-body">

        <div class="erros callout callout-danger hidden">
                <p></p>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <table class="table table-condensed">
                    <thead>
                        <th>Produto</th>
                        <th>Quantiadde</th>
                        <th>Valor Unitário</th>
                        <th>Valor</th>
                    </thead>
                    <tbody id="t-body">
                    </tbody>
                </table>
            </div>
        </div>
      <h4>Informe sua senha financeira para confirmar a compra</h4>     
       <form class="form-horizontal" role="form" id="form">             
              <input type="hidden" name="codigo_pedido" id="codigo_pedido">
              <input type="hidden" name="tipo_retirada" id="tipo_retirada" value="">
             <input type="password" id="senha" name="senha" class="form-control">
        </form>

               

      </div> <!-- Fim de ModaL Body-->

      <div class="modal-footer">
        <button type="button" class="btn btn-action btn-success pagar" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">
          Confirmar
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          Cancelar
        </button>
      </div> <!-- Fim de ModaL Footer-->

    </div> <!-- Fim de ModaL Content-->

  </div> <!-- Fim de ModaL Dialog-->

</div> <!-- Fim de ModaL Usuario-->