@extends('adminlte::page')
<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/financeiro_empresa.js') }}" type = "text/javascript" ></script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type = "text/javascript"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<meta name="csrf-token" content="{{ csrf_token() }}"> 

@section('htmlheader_title')
    Adicionar crédito
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    
                        <div class="box-body">   

<div class="col-sm-8 col-md-offset-2" style="    background-color: white;"> 

 <form class="form-horizontal" role="form" id="form" >
       {{csrf_field()}}
                <div class="form-group">
                    <h2 class="text-center">Adicionar crédito</h2>
                        <div class="col-sm-12 hidden" id="nome-user">
                            <strong>Nome do usuário:</strong>
                            <div class="input-group ">
                                <span data-toggle="tooltip" title="User" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" id="nome_user">
                            </div>       
                        </div>

                        <div class="col-sm-12">
                            <strong>User:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="User" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="user" id="user">
                            </div>       
                        </div>

                         <div class="col-sm-12">
                            <strong>Valor a ser creditado:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Crédito" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" class="form-control" name="valor"  id="valor" maxlength="13" onkeypress="moeda(event)"  onkeydown="FormataMoeda(this, 10, event)">
                            </div>       
                        </div>

                        <div class="col-sm-12">
                            <strong>Senha financeira:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="senha" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="password" maxlength="254" class="form-control" name="password"  id="password">
                            </div>       
                        </div>
                         <div class="text-center" style="margin-top: 40px">
                
                                <button id="salvar" style="font-size: 20px; margin-bottom: 20px; margin-top: 20px" type="button" class="btn btn-action btn-success credito" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Creditar
                         
                        </button>
                    </div>
                         
                </div>
               </form>       
            </div>

              </div>
                    </div>
                </div>
        </div>
</div>
@endsection
