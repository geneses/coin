@extends('adminlte::page')

<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/buscas.js')  . '?update=' . str_random(3)   }}" type = "text/javascript" ></script>

@section('htmlheader_title')
	Gerenciar Categoria
@endsection

@section('main-content')


<div class="container-fluid spark-screen">
		<div class="row">
			<?php $mobile = FALSE;
    $teste = FALSE;

    $user_agents = array("iPhone","iPad","Android","webOS","BlackBerry","iPod","Symbian","IsGeneric");

    foreach($user_agents as $user_agent){

        if (strpos($_SERVER['HTTP_USER_AGENT'], $user_agent) !== FALSE) {
            $mobile = TRUE;

            break;
        }
    }
?>
	<div class="col-md-12">
		<div class="box box-success">            				                    
                <div class="box-body text-center table-responsive">
                	<div class="form-group">
                		<form id="form">
                		<div class="col-md-2"><input style="border: groove;" name="inicio" id="inicio" type="date" class="btn btn-action" value="Buscar"><span>De </span></div>
                		<div class="col-md-2"><input style="border: groove;" name="fim" id="fim" type="date" class="btn btn-action" value="Buscar"><span>A </span> <span>(OPCIONAL)</span></div>

                		<div class="col-md-2"><span></span><input readonly="" class="btn btn-action btn-success buscarExtratoGeral" value="Buscar"></div>

                		<div class="col-md-3"><span>User </span> <input style="border: groove;" name="user" id="user" type="text"></div>

                		<div class="col-md-2"><span></span><input readonly="" class="btn btn-action btn-success buscarUserGeral" value="Buscar"></div>
                		</form>
                	</div>
                </div>
        </div>
    </div>

<div class="hiddens">

<?php if ($mobile){ ?>

			<div class="col-md-12" style="margin-top: 80px;">
<?php }else{  ?>
	<div class="col-md-12">
	<?php } ?>
				<div class="box box-success">            				                    
                    <div class="box-body text-center table-responsive">
	                    <table class="table table-striped">
							<thead>
		                    	<tr>
		                    		<th>Data</th>
		                    		<th>Descrição</th>
		                    		<th>Usuário</th>
		                    		<th>Origem</th>
		                    		<th>Valor</th>
		                    	</tr>
		                    </thead>

		                    <tbody id="valores">
                            </tbody>
		                    	
	                    </table>
					</div>
                    </div>
                    <div class="box box-success">            				                    
                    	<div class="box-body text-center">
                    		<div class="panel-body">
					<div class="col-md-2"></div>
					<div class="col-md-3">
						<span style="background:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<label>Creditado</label>
					</div>
					<div class="col-md-3">
						<span style="background:orange;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<label>Bloqueado</label>
					</div>
					<div class="col-md-3">
						<span style="background:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<label>Debitado</label>
					</div>
				</div>
                    	</div>
                	</div>
               </div>

                </div>
            </div>
</div>

@endsection