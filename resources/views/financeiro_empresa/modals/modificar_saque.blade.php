<div id="criar_editar-modal" class="modal fade bs-example" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div> <!-- Fim de ModaL Header-->

      <div class="modal-body">

        <div class="erros callout callout-danger hidden">
                <p></p>
        </div>


       <form class="form-horizontal" role="form" id="form" >
                    <div class="form-group">
                        <div class="col-sm-12">
                            <strong>Nome:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="nome"  id="nome">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>Rua:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Rua" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="rua"  id="rua">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>complemento:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="complemento" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="complemento"  id="complemento">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>numero:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="numero" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="numero"  id="numero">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>bairro:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="bairro" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="bairro"  id="bairro">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>estado:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="estado" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="estado"  id="estado">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>cep:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="cep" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="cep"  id="cep">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>telefone:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="telefone" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="telefone"  id="telefone">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>telefone2:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="telefone2" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="telefone2"  id="telefone2">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>banco:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="banco" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="banco"  id="banco">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>agencia:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="agencia" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="agencia"  id="agencia">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>agencia_digito:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="agencia_digito" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="agencia_digito"  id="agencia_digito">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>conta:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="conta" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="conta"  id="conta">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>conta_digito:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="conta_digito" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="conta_digito"  id="conta_digito">
                            </div>       
                        </div>


                        <div class="col-sm-3">
                            <strong>conta_tipo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="conta_tipo" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="conta_tipo"  id="conta_tipo">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>conta_digito:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="conta_digito" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="conta_digito"  id="conta_digito">
                            </div>       
                        </div>

                    </div>  

                    <div class="form-group">
                        <div class="col-sm-4">
                            <strong>Status:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Ação" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select name="status" id="status" class="form-control">
                                  <option>SOLICITADO</option>
                                  <option>PAGO</option>
                                  <option>CANCELADO</option>
                                  <option>EM ANÁLISE</option>
                                </select>
                            </div>       
                        </div>
                    </div>
                  
                            <input type="hidden" id="id" name="id">
                </form>

      </div> <!-- Fim de ModaL Body-->


      <div class="modal-footer">
        <button type="button" class="btn btn-action btn-success add" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Cadastrar
          <i class="fa fa-floppy-o"> </i>
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <i class='fa fa-times'></i>
        </button>
      </div> <!-- Fim de ModaL Footer-->

    </div> <!-- Fim de ModaL Content-->

  </div> <!-- Fim de ModaL Dialog-->

</div> <!-- Fim de ModaL Usuario-->