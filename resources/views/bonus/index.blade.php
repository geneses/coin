@extends('adminlte::page')

<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/transacao.js') }}" type = "text/javascript" ></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type = "text/javascript"></script>

<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>

@section('htmlheader_title')
	Gerenciar Tipo de Transação
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
            @isset($bonus[$titulo])
				<div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$titulo}}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h4>{{$bonus[$titulo]}}</h4>
                    </div>
                    <!-- /.box-body -->
                </div>
            @else
            <h3>Bônus não encontrado</h3>
            @endif
			</div>
		</div>
	</div>


@endsection
