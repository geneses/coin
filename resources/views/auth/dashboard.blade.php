@extends('adminlte::page')

<script src="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/scripts_gerais/captura.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>

@section('htmlheader_title')
Dashboard
@endsection

@section('teste')
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner">

        <?php $i=0  ?>
        @foreach ($slides as $value)
        <?php if ($i==0){ ?>
        <div class="item active">
            <?php }else{ ?>

            <div class="item">
                <?php } ?>
                <img src="{{ $value->imagem }}" alt="First slide" style="width: 100%;height: 36%;">
            </div>
            <?php $i++;  ?>
            @endforeach

        </div>
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
            <span class="fa fa-angle-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
            <span class="fa fa-angle-right"></span>
        </a>
    </div>
    @endsection

    @section('main-content')

    <div class="container-fluid spark-screen">

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-body" style="    height: 115;">
                            <strong>Link de indicação:</strong>
                            <div class="input-group">
                                <span style="cursor: pointer;" data-toggle="tooltip" title="Copiar link"
                                    class="input-group-addon" onclick="myFunction()"><i class="fa fa-copy "></i></span>
                                <input type="text" maxlength="254" class="form-control" name="user" id="user"
                                    value="http://localhost:8000/registrar/{{ Auth::user()->user }}" readonly="">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-body">
                            <table class="table">
                                <thead>
                                    <th>Qualificação binária</th>
                                    <th>Posição da rede</th>
                                </thead>
                                <tr>
                                    <td width="20px">Ativa</td>
                                    <td>
                                        <form id="form">
                                            {{csrf_field()}}
                                            <select class="form-control" name="lado_rede">
                                                @if($saldo->lado_rede == 0)
                                                <option value="0" selected="">ESQUERDA</option>
                                                @else
                                                <option value="0">ESQUERDA</option>
                                                @endif

                                                @if($saldo->lado_rede == 1)
                                                <option value="1" selected="">DIREITA</option>
                                                @else
                                                <option value="1">DIREITA</option>
                                                @endif
                                            </select>
                                        </form>
                                    </td>

                                    <td><button class="btn btn-primary alterar_lado">Alterar</button> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-body">
                            @if(Auth::user())
                            @if(!Auth::user()->hasRole('usuario_site'))
                            <div class="form-group">
                                <div class="col-sm-12">

                                    <div class="col-md-12" style="top: 20px">

                                        <div class="col-md-3 text-center">
                                            <div class="panel panel-primary" style="border-color: #00a65a;">
                                                <div class="panel-heading"
                                                    style="background-color: #00a65a;border-color: #00a65a;">
                                                    <h3 class="panel-title" style="">Total de ganhos</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <h2
                                                        style="margin-bottom: 100px;margin-bottom: 53px;margin-top: 70px;">
                                                        R$ {{  number_format($total_ganho, 2, ',', '.') }}
                                                    </h2>

                                                    <span class="span-current-balance">&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <div class="panel panel-primary" style="border-color: #00a65a;">
                                                <div class="panel-heading"
                                                    style="background-color: #00a65a;border-color: #00a65a;">
                                                    <h3 class="panel-title">Saldo Para Saque</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <h2
                                                        style="margin-bottom: 100px;margin-bottom: 53px;margin-top: 70px;">

                                                        @if($saldo->saldo == null)
                                                        R$ 0,00
                                                        @else
                                                        R$ {{ number_format($saldo->saldo, 2, ',', '.')  }}
                                                        @endif
                                                    </h2>

                                                    <span class="span-current-balance">&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <div class="panel panel-primary" style="border-color: #00a65a;">
                                                <div class="panel-heading"
                                                    style="background-color: #00a65a;border-color: #00a65a;">
                                                    <h3 class="panel-title">Kit</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <h2>
                                                        @if(Auth::user()->fk_pacote == null)
                                                        GRATUITO
                                                        @endif

                                                        {{ Auth::user()->fk_pacote }}
                                                    </h2>

                                                    <div class="form-group" style="margin-top: 50px">

                                                        <form class="form-horizontal" role="form" id="form"
                                                            method="POST"
                                                            action="{{ url('/gerenciar-usuarios/get-usuario') }}">
                                                            {{csrf_field()}}
                                                            <input type="hidden" class="form-control" name="user"
                                                                value="{{Auth::user()->user}}">

                                                            <button style="background-color: rgb(193, 193, 193);
    border-color: #cdcbac; color: black; font-weight: 600" style="margin-top: 20px" type="submit"
                                                                class="btn btn-primary btn-block"
                                                                data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">UPGRADE

                                                            </button>
                                                        </form>
                                                    </div>

                                                    <span class="span-current-balance">&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <div class="panel panel-primary" style="border-color: #00a65a;">
                                                <div class="panel-heading"
                                                    style="background-color: #00a65a;border-color: #00a65a;">
                                                    <h3 class="panel-title" style="">Qalificação atual</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <h2
                                                        style="margin-bottom: 100px;margin-bottom: 53px;margin-top: 70px;">
                                                        {{ $saldo->nome_qualificacao }}
                                                    </h2>

                                                    <span class="span-current-balance">&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endif

                    </div>


                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="col-md-4">
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user text-center">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-aqua-active">
                                        <h5 class="widget-user-username" style="font-size: 18px;">Pontos de Equipe
                                            Diário</h5>
                                    </div>
                                    <div class="widget-user-image">
                                        <img style="width: 95px;position: relative; height: 85px;" class="img-circle"
                                            src="{{ asset('img/users1.png') }}" alt="User Avatar">
                                    </div>
                                    <div class="box-footer">
                                        <div class="row text-center">
                                            <div class="col-sm-6 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">ESQUERDA</h5>
                                                    <span class="description-text">{{ $saldo->ponto_esquerda }}</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="description-block">
                                                    <h5 class="description-header">DIREITA</h5>
                                                    <span class="description-text">{{ $saldo->ponto_direita }}</span>
                                                </div>
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            </div>

                            <div class="col-md-4">
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user text-center">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-aqua-active">
                                        <h5 class="widget-user-username" style="font-size: 18px;">Pontos de Equipe Total
                                        </h5>
                                    </div>
                                    <div class="widget-user-image">
                                        <img style="width: 95px;position: relative; height: 85px;" class="img-circle"
                                            src="{{ asset('img/users1.png') }}" alt="User Avatar">
                                    </div>
                                    <div class="box-footer">
                                        <div class="row text-center">
                                            <div class="col-sm-6 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">ESQUERDA</h5>
                                                    <span class="description-text">{{ $saldo->total_ponto_es }}</span>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="description-block">
                                                    <h5 class="description-header">DIREITA</h5>
                                                    <span class="description-text">{{ $saldo->total_ponto_di }}</span>
                                                </div>
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            </div>

                            <div class="col-md-4">
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user text-center">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-aqua-active">
                                        <h5 class="widget-user-username" style="font-size: 18px;">Tota de Membros Ativos
                                        </h5>
                                    </div>
                                    <div class="widget-user-image">
                                        <img style="width: 95px;position: relative; height: 85px;" class="img-circle"
                                            src="{{ asset('img/users.png') }}" alt="User Avatar">
                                    </div>
                                    <div class="box-footer">
                                        <div class="row text-center">
                                            <div class="col-sm-6 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">ESQUERDA</h5>
                                                    <span class="description-text">0</span>
                                                </div>

                                            </div>

                                            <div class="col-sm-6">
                                                <div class="description-block">
                                                    <h5 class="description-header">DIREITA</h5>
                                                    <span class="description-text">0</span>
                                                </div>
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
        @endsection