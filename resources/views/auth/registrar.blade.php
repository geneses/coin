<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script src ="{{ asset('/js/scripts_gerais/cadastro_user.js') }}" type = "text/javascript" ></script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<script type="text/javascript">
function usuario_validate(e,args){
    var evt = (document.all)? event.keyCode : e.charCode;
  var valid_chars = 'qwertyuiopasdfghjklzxcvbnm123456789';  
  var chr= String.fromCharCode(evt);    // pegando a tecla digitada
  if (valid_chars.indexOf(chr)<0){
    e.preventDefault();
  }
}

function email_validate(e,args){
    var evt = (document.all)? event.keyCode : e.charCode;
  var valid_chars = 'qwertyuiopasdfghjklzxcvbnm123456789@_.';  
  var chr= String.fromCharCode(evt);    // pegando a tecla digitada
  if (valid_chars.indexOf(chr)<0){
    e.preventDefault();
  }
}
</script>

<div class="col-sm-8 col-md-offset-2" style="    background-color: white;"> 

 <form class="form-horizontal" role="form" id="form" >
       {{csrf_field()}}

       <div class="modal fade" id="termo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Termos de uso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           temiasmidasoda aisdnaisdn asoidnasiod
           <br>
           <label><input required="" type="checkbox" id="termo" name="termo"> 
           Aceito os termos de uso</label>
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary cadastrar">Cadastrar</button>
      </div>
    </div>
  </div>
</div>


 				<div class="form-group">
 					<h2 class="text-center">Para seu acesso</h2>
                    <div class="col-sm-12 hidden" id="nome-user">
                            <strong>Nome do patrocinador:</strong>
                            <div class="input-group ">
                                <span data-toggle="tooltip" title="User" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" id="nome_user">
                            </div>       
                        </div>

                        <div class="col-sm-12">
                            <strong>Indicador:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Indicador" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="patrocinador" onkeypress="usuario_validate(event)" id="patrocinador" value="{{ $user }}">
                            </div>       
                        </div>

                        <div class="col-sm-12" style="margin-top: 20px">
                            <strong>User name:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="User name" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="usuario_validate(event)" maxlength="254" class="form-control" name="user"  id="user">
                            </div>       
                        </div>

                        <div class="col-sm-12">
                            <strong>E-mail:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="E-mail" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="email" onkeypress="email_validate(event)" maxlength="254" class="form-control" name="email"  id="email">
                            </div>       
                        </div>
                    
                        <div class="col-sm-6">
                            <strong>Senha:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Senha" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="password" maxlength="254" class="form-control" name="password"  id="password">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>Confirmar senha:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Confirmar" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="password" maxlength="254" class="form-control" name="password_confirmation"  id="password_confirmation">
                            </div>       
                        </div>
                        </div>
                        <h2 class="text-center">Seus dados pessoais</h2>
                        <div class="col-md-6">
                            <select class="form-control" id="tipo" name="tipo">
                                <option value="fisica">Física</option>
                                <option value="juridica">Jurídica</option>
                            </select>
                        </div>
                        <div class="form-group">
                        <div class="col-sm-8">
                            <strong>Nome:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="nome"  id="nome">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Sexo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Sexo" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select class="form-control" name="sexo"  id="sexo">
                                  <option>MASCULINO</option>
                                  <option>FEMININO</option>
                                </select>
                            </div>       
                        </div>
          
                        <div class="col-sm-4">
                            <strong>RG:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="RG" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="rg"  id="rg">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>Data de nascimento:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Data de nascimento" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="date" maxlength="254" class="form-control" name="data_nascimento"  id="data_nascimento">
                            </div>       
                        </div>

                         <div id="fisica">
                             <div class="col-sm-5">
                                <strong>CPF:</strong>
                                <div class="input-group">
                                    <span data-toggle="tooltip" title="cpf" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" maxlength="254" class="form-control" name="cpf"  id="cpf">
                                </div>       
                            </div>
                        </div>
                        <div class="hidden" id="juridica">
                            <div class="col-sm-5">
                                <strong>CNPJ:</strong>
                                <div class="input-group">
                                    <span data-toggle="tooltip" title="cnpj" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" maxlength="254" class="form-control" name="cnpj"  id="cnpj">
                                </div>       
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <strong>Orgão expedidor:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Orgão expedidor" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="orgao_expedidor"  id="orgao_expedidor">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>PIS/PASEP:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="PIS/PASEP" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="pis"  id="pis">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>Telefone:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Telefone" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="telefone"  id="telefone">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>Telefone:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Telefone" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="telefone2"  id="telefone2">
                            </div>       
                        </div>
                    </div>

                     <h2 class="text-center">Dados de endereço</h2>

                     <div class="form-group">
                        <div class="col-sm-4">
                            <strong>Rua:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Rua" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="rua"  id="rua">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Complemento:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Complemento" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="complemento"  id="complemento">
                            </div>       
                        </div>
                   

                    
                        <div class="col-sm-4">
                            <strong>Cidade:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Cidade" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cidade"  id="cidade">
                            </div>       
                        </div>
                    </div>

                    <div class="form-group">
                         <div class="col-sm-3">
                            <strong>Estado:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Estado" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="estado"  id="estado">
                            </div>       
                        </div>

                    	 <div class="col-sm-3">
                            <strong>Cep:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Cep" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cep"  id="cep">
                            </div>       
                        </div>
                      
                        <div class="col-sm-2">
                            <strong>Numero:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Numero" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="number" maxlength="254" class="form-control" name="numero"  id="numero">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Bairro:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Bairro" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="bairro"  id="bairro">
                            </div>       
                        </div>
                    </div>

                    <div class="erros callout callout-danger hidden" style="background: red; color:white; padding: 5px">
                         <p></p>
                    </div>

                    
                    <div class="text-center" style="margin-top: 40px">
                     <strong> Eu declaro que li e aceitei os <a href="#">Termos de Uso e Condições </a> contidos no acordo da Revolution</strong>
<br>
                          <button id="salvar" style="font-size: 20px; margin-bottom: 20px; margin-top: 20px" type="button" class="btn btn-action btn-success termo" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Cadastrar
				        </button>
                    </div>
                </form>
            </div>

