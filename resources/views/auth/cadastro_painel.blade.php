@extends('adminlte::page')
<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/cadastro_user.js') }}" type = "text/javascript" ></script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 

@section('htmlheader_title')
    Cadastro de afiliado
@endsection

@section('menu','dados')
@section('main-content')
<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                        <div class="box-body">
<div class="col-sm-8 col-md-offset-2" style="    background-color: white;"> 

 <form class="form-horizontal" role="form" id="form" >
       {{csrf_field()}}
 				<div class="form-group">
 					<h2 class="text-center">Dados de acesso</h2>
                        
                    <input type="hidden" maxlength="254" class="form-control" name="patrocinador"  id="patrocinador" value="{{ Auth::user()->user }}">
                         
                        <div class="col-sm-12">
                            <strong>User name:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="User name" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="user"  id="user">
                            </div>       
                        </div>

                        <div class="col-sm-12">
                            <strong>E-mail:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="E-mail" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="email" maxlength="254" class="form-control" name="email"  id="email">
                            </div>       
                        </div>
                    
                        <div class="col-sm-6">
                            <strong>Senha:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Senha" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="password"  id="password">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>Confirmar senha:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Confirmar" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="passwordConfirme"  id="passwordConfirme">
                            </div>       
                        </div>
</div>
                        <h2 class="text-center">Dados pessoais</h2>
                        <div class="form-group">
                        <div class="col-sm-8">
                            <strong>Nome:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="nome"  id="nome">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Sexo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Sexo" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select class="form-control" name="sexo"  id="sexo">
                                  <option>MASCULINO</option>
                                  <option>FEMININO</option>
                                </select>
                            </div>       
                        </div>
          
                        <div class="col-sm-4">
                            <strong>RG:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="RG" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="rg"  id="rg">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>Data de nascimento:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Data de nascimento" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="date" maxlength="254" class="form-control" name="data_nascimento"  id="data_nascimento">
                            </div>       
                        </div>

                         <div class="col-sm-5">
                            <strong>CPF:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="cpf" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cpf"  id="cpf">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>Orgão expedidor:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Orgão expedidor" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="orgao_expedidor"  id="orgao_expedidor">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>PIS/PASEP:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="PIS/PASEP" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="pis"  id="pis">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>Telefone:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Telefone" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="telefone"  id="telefone">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>Telefone:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Telefone" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="telefone2"  id="telefone2">
                            </div>       
                        </div>
                    </div>

                     <h2 class="text-center">Dados de endereço</h2>

                     <div class="form-group">
                        <div class="col-sm-4">
                            <strong>Rua:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Rua" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="rua"  id="rua">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Complemento:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Complemento" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="complemento"  id="complemento">
                            </div>       
                        </div>
                   

                    
                        <div class="col-sm-4">
                            <strong>Cidade:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Cidade" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cidade"  id="cidade">
                            </div>       
                        </div>
                    </div>

                    <div class="form-group">
                         <div class="col-sm-3">
                            <strong>Estado:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Estado" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="estado"  id="estado">
                            </div>       
                        </div>

                    	 <div class="col-sm-3">
                            <strong>Cep:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Cep" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cep"  id="cep">
                            </div>       
                        </div>
                      
                        <div class="col-sm-2">
                            <strong>Numero:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Numero" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="numero"  id="numero">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Bairro:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Bairro" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="bairro"  id="bairro">
                            </div>       
                        </div>
                    </div>

                    
                    <div class="text-center" style="margin-top: 40px">
                        <input type="checkbox" name=""><strong> Eu declaro que li e aceitei os <a href="#">Termos de Uso e Condições </a> contidos no acordo da Revolution</strong>
<br>
                          <button style="font-size: 20px; margin-bottom: 20px; margin-top: 20px" type="button" class="btn btn-action btn-success cadastrarPainel" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Cadastrar
				        </button>
                    </div>
                </form>
            </div>

              </div>
                    </div>
                </div>
        </div>
</div>
@endsection
