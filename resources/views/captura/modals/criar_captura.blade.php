<div id="criar_editar-modal" class="modal fade bs-example" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div> <!-- Fim de ModaL Header-->

            <div class="modal-body">

                <div class="erros callout callout-danger hidden">
                    <p></p>
                </div>


                <form class="form-horizontal" role="form" id="form">
                    <div class="col-sm-3">
                        <strong>Recrutamento Texto1:</strong>
                        <div class="input-group">
                            <span data-toggle="tooltip" title="Recrutamento Texto1" class="input-group-addon"><i
                                    class="fa fa-pencil"></i></span>
                            <input type="text" maxlength="254" class="form-control" name="recru_texto1"
                                id="recru_texto1" value="{{ Auth::User()->nome }}">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <strong>Recrutamento Texto2:</strong>
                        <div class="input-group">
                            <span data-toggle="tooltip" title="Recrutamento Texto2" class="input-group-addon"><i
                                    class="fa fa-pencil"></i></span>
                            <input type="text" maxlength="254" class="form-control" name="recru_texto2"
                                id="recru_texto2">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <strong>Recrutamento video1:</strong>
                        <div class="input-group">
                            <span data-toggle="tooltip" title="Avatar" class="input-group-addon"><i
                                    class="fa fa-pencil"></i></span>
                            <input type="text" maxlength="254" class="form-control" name="recru_video1"
                                id="recru_video1">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3">
                            <strong>Recrutamento video2:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Avatar" class="input-group-addon"><i
                                        class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="recru_video2"
                                    id="recru_video2">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <strong>Recrutamento fundo1:</strong>
                        <div class="input-group">
                            <span data-toggle="tooltip" title="Recrutamento fundo1" class="input-group-addon"><i
                                    class="fa fa-pencil"></i></span>
                            <input type="text" maxlength="254" class="form-control" name="recru_fundo1"
                                id="recru_fundo1">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <strong>Recrutamento fundo2:</strong>
                        <div class="input-group">
                            <span data-toggle="tooltip" title="Recrutamento fundo2" class="input-group-addon"><i
                                    class="fa fa-pencil"></i></span>
                            <input type="text" maxlength="254" class="form-control" name="recru_fundo2"
                                id="recru_fundo2">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <strong>Recrutamento link:</strong>
                        <div class="input-group">
                            <span data-toggle="tooltip" title="Recrutamento link" class="input-group-addon"><i
                                    class="fa fa-pencil"></i></span>
                            <input type="text" maxlength="254" class="form-control" name="recru_link" id="recru_link">
                        </div>
                    </div>

                    <input type="hidden" id="id" name="id">
                </form>

            </div> <!-- Fim de ModaL Body-->


            <div class="modal-footer">
                <button type="button" class="btn btn-action btn-success add"
                    data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Alterar
                    <i class="fa fa-floppy-o"> </i>
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    <i class='fa fa-times'></i>
                </button>
            </div> <!-- Fim de ModaL Footer-->

        </div> <!-- Fim de ModaL Content-->

    </div> <!-- Fim de ModaL Dialog-->

</div> <!-- Fim de ModaL Usuario-->