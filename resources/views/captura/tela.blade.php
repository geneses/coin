<!DOCTYPE html>
<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<title></title>
</head>

<body background="http://coin.local:8080/{{ $user->recru_fundo1 }}">
<meta name="csrf-token" content="{{ csrf_token() }}"> 

<div class="col-md-12">

<div class="col-md-6" style="margin-top: 50px">
	<iframe width="800" height="500" src="https://www.youtube.com/embed/{{$user->recru_video1}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<div class="col-md-6">
	<h4>{{ $user->recru_texto1 }}</h4>
	
	<form action="{{ route('cadastrar-capturado') }}" method="post">
		     {{csrf_field()}}

		<input type="text" name="nome" id="nome" class="form-control" placeholder="Nome">
		<input type="text" name="email" id="email" class="form-control" placeholder="E-mail">
		<input type="text" name="whatsapp" id="whatsapp" class="form-control" placeholder="whatsapp">
		<input type="hidden" name="fk_user" id="fk_user" class="form-control" value="{{ $user->id }}">
		<button class="btn btn-primary">Enviar</button>


	
	</form>
</div>

</div>

</body>
</html>

