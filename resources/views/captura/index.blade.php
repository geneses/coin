<?php //dd(phpinfo()) ?>
@extends('adminlte::page')

<script src="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/scripts_gerais/captura.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>

<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>

@section('htmlheader_title')
Gerenciar Capturas
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Configurar Capturas</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <form class="form-horizontal" role="form" id="form">

                        <div class="col-sm-12">
                            <strong>Recrutamento link:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Recrutamento link" class="input-group-addon"><i
                                        class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="recru_link"
                                    id="recru_link" value="{{ Auth::User()->recru_link }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <strong>Recrutamento Texto1:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Recrutamento Texto1" class="input-group-addon"><i
                                        class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="recru_texto1"
                                    id="recru_texto1" value="{{ Auth::User()->recru_texto1 }}">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <strong>Recrutamento Texto2:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Recrutamento Texto2" class="input-group-addon"><i
                                        class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="recru_texto2"
                                    id="recru_texto2" value="{{ Auth::User()->recru_texto2 }}">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <strong>Recrutamento video1:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Avatar" class="input-group-addon"><i
                                        class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="recru_video1"
                                    id="recru_video1" value="{{ Auth::User()->recru_video1 }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <strong>Recrutamento video2:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Avatar" class="input-group-addon"><i
                                        class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="recru_video2"
                                    id="recru_video2" value="{{ Auth::User()->recru_video2 }}">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <strong>Recrutamento fundo1(limite 1mb):</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Recrutamento fundo1" class="input-group-addon"><i
                                        class="fa fa-pencil"></i></span>
                                <input type="file" maxlength="254" class="form-control" name="recru_fundo1"
                                    id="recru_fundo1" value="{{ Auth::User()->recru_fundo1 }}">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <strong>Recrutamento fundo2(limite 1mb):</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Recrutamento fundo2" class="input-group-addon"><i
                                        class="fa fa-pencil"></i></span>
                                <input type="file" maxlength="254" class="form-control" name="recru_fundo2"
                                    id="recru_fundo2" value="{{ Auth::User()->recru_fundo2 }}">
                            </div>
                        </div>

                        <input type="hidden" id="id" name="id">
                    </form>

                </div>
                <div class="text-right" style="margin-right: 24px;padding-bottom: 10px;">
                    <button type="button" class="btn btn-action btn-success add"
                        data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Salvar
                        <i class="fa fa-floppy-o"> </i>
                    </button>
                </div>
                <!-- /.box-body -->
            </div>

        </div>

        <div class="col-md-12">

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Capturados</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table">
                        <tr>
                            <td>Nome</td>
                            <td>E-email</td>
                            <td>whatsapp</td>
                        </tr>
                        @foreach($capturados as $capturado)
                        <tr>
                            <td>{{$capturado->nome}}</td>
                            <td>{{$capturado->email}}</td>
                            <td>{{$capturado->whatsapp}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection