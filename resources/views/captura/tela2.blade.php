<!DOCTYPE html>
<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<title></title>
</head>

<body background="http://coin.local:8080/{{ $user->recru_fundo2 }}">
<meta name="csrf-token" content="{{ csrf_token() }}"> 

<div class="col-md-12">

<div class="text-center" style="margin-top: 50px">
	<iframe width="800" height="500" src="https://www.youtube.com/embed/{{$user->recru_video2}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<div class="text-center">
	<h4>{{ $user->recru_texto2 }}</h4>

	<a class="btn btn-primary" href="{{ route('registrar', $user->user) }}"> Cadastra-se</a>
</div>

</div>

</body>
</html>

