@hasSection('menu')
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" style="margin-top: 40px;">
     
            
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            @if(Auth::user())
            <li><a href="{{ route('update.usuario') }}"><i class="fa fa-dashboard"></i> <span>Pessoal</span></a></li>
            <li><a href="{{ route('bancario') }}"><i class="fa fa-dashboard"></i> <span>Financeiro</span></a></li>
            <li><a href="{{ route('senha') }}"><i class="fa fa-dashboard"></i> <span>Alterar senha</span></a></li>
            <li><a href="{{ route('avatar') }}"><i class="fa fa-dashboard"></i> <span>Foto</span></a></li>
            @endif
        </ul><!-- /.sidebar-menu -->

    </section>
    <!-- /.sidebar -->
</aside>
@else    


<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" style="margin-top: 40px;">
     
            
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            @if(Auth::user())
            @if(Auth::user()->hasRole('empresa'))
              <!-- SIDEBAR EMPRESA -->    


            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Usuários<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('gerenciar-usuarios.index','todos') }}">Todos</a></li>
                    <li><a href="{{ route('gerenciar-usuarios.index','pagos') }}">Pagos</a></li>
                    <li><a href="{{ route('gerenciar-usuarios.index','pendentes') }}">Pendentes</a></li>
                    <li><a href="{{ route('gerenciar-usuarios.index','ativos') }}">Ativos</a></li>
                    <li><a href="{{ route('gerenciar-usuarios.index','bloqueados') }}">Bloqueados</a></li>
                    <li><a href="{{ route('gerenciar-usuarios.index','inativos') }}">Inativos</a></li>
                    <li><a href="{{ route('registrar') }}">Cadastrar</a></li>
                    
                </ul>
            </li>         
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Loja<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('gerenciar-categorias.index') }}">Cadastrar Categorias</a></li>
                    <li><a href="{{ route('gerenciar-produtos.index') }}">Cadastrar Produtos</a></li>
                    <li><a href="{{ route('gerenciar-banners.index') }}">Cadastrar Banner</a></li>    
                    <li><a href="{{ route('gerenciar-produtos.vendidos') }}">Vendidos</a></li>
                    <li><a href="{{ route('gerenciar-produtos.disponivel') }}">Disponíveis</a></li>
                    <li><a href="{{ route('gerenciar-fretes.index') }}">Configuar Frete</a></li>
                    <li><a href="{{ route('gerenciar-produtos.plataforma') }}">Plataforma de pagamento</a></li>
                    <li><a href="{{ route('gerenciar-parcelamentos.index') }}">Parcelamento</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Loja Fidelidade<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('gerenciar-fidelidade-categorias.index') }}">Cadastrar Categorias</a></li>
                    <li><a href="{{ route('gerenciar-fidelidade-produtos.index') }}">Cadastrar Produtos</a></li>
                    <li><a href="{{ route('gerenciar-fidelidade-banners.index') }}">Cadastrar Banner</a></li>    
                    <li><a href="{{ route('gerenciar-produtos.vendidos') }}">Vendidos</a></li>
                    <li><a href="{{ route('gerenciar-produtos.disponivel') }}">Disponíveis</a></li>
                    <li><a href="{{ route('gerenciar-fretes.index') }}">Configuar Frete</a></li>
                </ul>
            </li>
             
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Financeiro<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('gerenciar-buscar-usuario.index') }}">Pagar pacote</a></li>
                    <li><a href="{{ route('gerenciar-financeiro.pedidos') }}">Pagar produto</a></li>
                    <li><a href="{{ route('gerenciar-financeiro.credito') }}">Adicionar Crédito</a></li>
                    <li><a href="{{ route('gerenciar-financeiro.debito') }}">Adicionar Débito</a>
                    </li><li><a href="{{ route('gerenciar-financeiro.saques.solicitados.exibir') }}">Solicitação de Saques</a></li>
                    
                    <li><a href="{{ route('extrato') }}">Extrato</a></li>
                    

                    <li class="treeview">
                        <a href="#"><i class='fa fa-link'></i>Imposto<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                        <li><a href="{{ route('gerenciar-impostos.index') }}">Cadastrar</a></li>

                        </ul>
                    </li>  

                    <li class="treeview">
                        <a href="#"><i class='fa fa-link'></i>Extrato<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('gerenciar-financeiro.get.extrato.geral') }}">Extrato Geral</a></li>
                             
                             <li><a href="{{ route('user-venda-pacote') }}">Vendas de pacotes</a></li>
                             <li><a href="{{ route('gerenciar-produtos.index') }}">Vendas de produtos</a></li>
                             <li><a href="{{ route('gerenciar-produtos.index') }}">Vendas de PA e CD</a></li>
                             <li><a href="{{ route('gerenciar-financeiro.saques.solicitados.exibir') }}">Solicitacoes de saques</a></li>
                        </ul>
                    </li>  

                     <li class="treeview">
                        <a href="#"><i class='fa fa-link'></i>Balanço<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                         <li><a href="{{ route('gerenciar-produtos.index') }}">Adicionar lucros</a></li>
                         <li><a href="{{ route('gerenciar-produtos.index') }}">Adicionar despesas</a></li>
                         <li><a href="{{ route('gerenciar-produtos.index') }}">Balanço</a></li>
                        </ul>
                    </li>  

                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Configurações do sistema<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('imagem-padrao') }}">Imagem padrão do usuário</a></li>
                    <li><a href="{{ route('gerenciar-transacoes.index') }}">Tipo de transação</a></li>
                    <li><a href="{{ route('gerenciar-pacotes.index') }}">Cadastrar pacotes</a></li>
                    <li><a href="{{ route('maximo.index') }}">Configurar limites</a></li>
         
                    <li class="treeview">
                        <a href="#"><i class='fa fa-link'></i>Bônificações<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('gerenciar-qualificacoes.index') }}">Plano de carreira</a></li>
                            <li><a href="{{ route('gerenciar-infinite.index') }}">Bônus Infinite</a></li>
                            <li><a href="{{ url('bonus=Bônus início rápido') }}">Bônus Início rápido</a></li>
                            <li><a href="{{ url('bonus=Bônus binário') }}">Bônus Binário</a></li>
                            <li><a href="{{ url('bonus=Bônus binário') }}">Bônus Upgrade</a></li>
                            <li><a href="{{ url('bonus=Bônus binário') }}">Bônus Lider de Cadastro</a></li>
                            <li><a href="{{ url('bonus=Bônus binário') }}">Bônus Lider de Compras</a></li>
                            <li><a href="{{ url('bonus=Bônus binário') }}">Bônus Participação</a></li>
                            <li><a href="{{ url('bonus=Bônus matriz forçada') }}">Bônus Matriz forçada</a></li>
                            <li><a href="{{ url('bonus=Bônus venda direta') }}">Bônus Venda Direta</a></li>
                            <li><a href="{{ url('bonus=Bônus venda indireta') }}">Bônus Venda Indireta</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('gerenciar-slides.index') }}">Slides</a></li>
                <li><a href="{{ route('gerenciar-material.index') }}">Material de apoio</a></li>
                </ul>   

            </li>

            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Configuração<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('configuracoes.index') }}">Empresa - Nome e Logo marca</a></li>
                    <li><a href="{{ route('gerenciar-capturas.index') }}">Pagina de captura</a></li>
                    <li class="treeview">
                        <a href="#"><i class='fa fa-link'></i>Layout<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Login</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Sistema</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Loja</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#"><i class='fa fa-link'></i>E-mails<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Configuração gerais</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Serviço de e-mail</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Convite</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Novo cadastro</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Aviso patrocinador</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Pedido finaliazo</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Pedido pago</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Adesão pago</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Aviso pedido finalizado</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Pedido enviado</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Solicitação paga</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Transferência de saldo</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Soicitação de saque</a></li>
                        </ul>
                    </li>
                    
                    <li class="treeview">
                        <a href="#"><i class='fa fa-link'></i>Mensagens<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Fale conosco</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Perguntas frequentes</a></li>
                        </ul>
                    </li>


                    <li class="treeview">
                        <a href="#"><i class='fa fa-link'></i>Jurídico<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Contrato</a></li>
                            <li><a href="{{ route('gerenciar-transacoes.index') }}">Termos e codições gerais</a></li>
                        </ul>
                    </li>

                 </ul>
            </li>

            @endif
            @endif

            @if(Auth::user())
            @if(Auth::user()->hasRole('usuario_ativo') || Auth::user()->hasRole('usuario_pendente'))
            <!-- SIDEBAR USUARIO ATIVO -->

            <li><a href="{{ route('incclube.index') }}"><i class="fa fa-shopping-cart"></i> <span>Loja</span></a></li>

            <li><a href="{{ route('gerenciar-cadastro.cadastro') }}"><i class="fa fa-users"></i> <span>Cadastrar</span></a></li>

              <li class="treeview">
                <a href="#"><i class='fa fa-sitemap'></i>Redes<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('redes') }}">Binária</a></li>
                    <li><a href="{{ route('rede.indicacao') }}">Indicações</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class='fa fa-money'></i>Financeiro<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('gerenciar-buscar-usuario.index') }}">Pagar usuario</a></li>
                    <li><a href="{{ route('extrato') }}">Extrato</a></li>
                    <li><a href="{{ route('gerenciar-usuarios.saque') }}">Solicitar saque</a></li>
                    <li><a href="{{ route('gerenciar-financeiro.saques.solicitados') }}">Saques solicitados</a></li>
                    <li><a href="{{ route('gerenciar-financeiro.senha') }}">Senha financeiro</a></li>
                    <li><a href="{{ route('transferencia') }}">Transferência de saldo</a></li>
                </ul>
            </li>

            <li><a href="{{ route('gerenciar-material.index') }}"><i class="fa fa-upload"></i> <span>Materiais</span></a></li>

            <li><a href="{{ route('gerenciar-financeiro.pedidos') }}"><i class="fa fa-list-alt"></i> <span>Pedidos</span></a></li>

            @endif
            @endif

            @if(Auth::user())
            @if(Auth::user()->hasRole('administrativo_cd'))
            <!--SIDEABR Administrativo CD -->
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Produtos<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Disponiveis</a></li>
                    <li><a href="#">Vendidos</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Vender Produtos<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Id de Pedido</a></li>
                    <li><a href="#">Loja Virtual</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Comprar produtos<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Loja virtual</a></li>
                </ul>
            </li> 

            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i>Financeiros<span></span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Total de vendas</a></li>
                    <li><a href="#">Total de compras</a></li>
                </ul>
            </li>

            <li><a href="#"><i class='fa fa-link'></i><span>Suporte</span></a></li>
            <li><a href="#"><i class='fa fa-link'></i><span>Sair</span></a></li>  
            
            <li><a href="#"><i class='fa fa-link'></i><span></span></a></li>
            @endif
            @endif
        </ul><!-- /.sidebar-menu -->

    </section>
    <!-- /.sidebar -->
</aside>
@endif