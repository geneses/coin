<!-- Main Header -->
<header class="main-header" style="min-height: 80px; ">

    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo" style="min-height: 80px;background: white; border-right: none;">
        <img src="{{ asset('incclube/images/brand.png') }}" width="80px">
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <button style="
    margin: 23px 0 0 20px;
    padding: 3px 9px;
    background: #F6F6F6;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    border: none;
" data-original-title="Alternar navegação" title="" data-placement="right" data-toggle="offcanvas" id="toggle-left" class="btn btn-default" type="button">
      <i class="fa fa-bars"></i>
    </button>

        <!--<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="border-right: none;"> -->
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
              
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu" id="user_menu" style="display: inline-flex;">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" style="border-left: none;">
                            <!-- The user image in the navbar-->
                            <div id="hide-name" class="" style="margin-top: 25px;margin-right: 20px;">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
            <strong style="color: #5D5F63">{{ Auth::user()->user }}</strong> <i class="fa fa-angle-down"></i>
          </a><ul class="margin"></ul>
          <ul class="dropdown-menu animated fadeInDown">
            <li>
              <a href="javascript:void(0);" style="cursor:default;">
                <i class="fa fa-user"></i> ID 38
              </a>
            </li>
            <li>
              <a href="javascript:void(0);" style="cursor:default;">
                <i class="fa fa-star"></i> <b><font color="green">31/12/2018</font></b>
              </a>
            </li>

            
            
            <li>
              <a href="{{ route('update.usuario') }}">
                <i class="fa fa-pencil"></i> Editando os dados
              </a>
            </li>
            <li>
              <a href="{{ url('/logout') }}" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off"></i> Sair
              </a>
            </li>
          </ul>
        </div>
                            <img src="{{ asset(Auth::user()->avatar) }}" class="user-image" alt="User Image" style="    width: 70px;margin-top: 4;height: 70px;}" />
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{ asset(Auth::user()->avatar) }}" class="img-circle" alt="User Image" />
                                <p>
                                    {{ Auth::user()->name }}
                                    <small>{{ trans('adminlte_lang::message.login') }} Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body 
                            <li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.followers') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.sales') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.friends') }}</a>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ url('/settings') }}" class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ trans('adminlte_lang::message.signout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
</header>
