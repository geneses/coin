@extends('incclube/layouts/template')
@section('title', 'Carrinho')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script src="{{ asset('js/scripts_gerais/carrinho.js' . '?update=' . str_random(3) ) }}"></script>

<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>


<meta name="csrf-token" content="{{ csrf_token() }}">

<section class="section-content bg padding-y-sm">
<div class="container">

<div class="row-sm">
  <table class="table">
    <tr>
      <td></td>
      <td>Nome</td>
      <td>Valor</td>
      <td>Quantidade</td>
      <td>Ação</td>
    </tr>
  @foreach($carrinho as $item)
    <tr>  
      <td><img src=" {{ asset($item->imagem) }}" width="100px" height="100px"></td>
      <td>{{ $item->nome }}</td>
      <td>R$ {{ $item->valor_v_usuario }}</td>
      <td>{{ $item->quantidade }}</td>
      <td><button class="btn btn-danger" id="delete_clube"  value="{{$item->id}}">Excluir</button></td>

    </tr>
  @endforeach
  <tr>
    <th></th>
    <th>Total</th>
    <th>R$ {{ $valor }}</th>
    <th>{{ $carrinho->sum('quantidade') }}</th>
  </tr>
  </table>

  <a href="{{ route('checkout.index')}}" class="btn btn-primary" >Finalizar compra</a>
    <button class="btn btn-primary float-right gerar_pedido" style="margin-left:10px"  type="button">Gerar pedido</button>


</div> <!-- row.// -->


</div><!-- container // -->
</section>

@endsection

@include('incclube.carrinho.modals.confirmar_compra')

