@extends('incclube/layouts/template')
@section('title', 'Carrinho')
@section('content')
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<section class="section-topbar border-top padding-y-sm">
<div class="container">
	<span>	<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index.php">Início</a></li>
    <li class="breadcrumb-item active" aria-current="page">Cadastro</li>
</ol> </span>
	<div class="float-right dropdown">
	
	</div> 
</div> <!-- container.// -->
</section>

<section class="section-content bg padding-y-sm">
	<div class="container">

<div class="col-md-12">

<div class="card">
<header class="card-header">
	<h4 class="card-title mt-2">Cadastre-se</h4>
</header>
<article class="card-body">
	<form method="post" role="form" id="form" >
		 {{csrf_field()}}
		<div class="form-row">
			<div class="col form-group">
				<label>Nome</label>
			  	<input type="text" class="form-control" placeholder="" name="nome" id="nome">
			</div> <!-- form-group end.// -->
			<div class="col form-group">
				<label>Username</label>
				<input type="text" class="form-control" placeholder="" name="user" id="user">
			</div> <!-- form-group end.// -->
		</div> <!-- form-row end.// -->

		<div class="form-row">
				<div class="col form-group">
				<label>E-mail</label>
			  	<input type="email" class="form-control" placeholder="" name="email" id="email">
			</div> <!-- form-group end.// -->
			<div class="col form-group">
				<label>Telefone</label>
				<input type="text" class="form-control" placeholder="" name="telefone" id="telefone">
			</div> <!-- form-group end.// -->

			<div class="col form-group">
				<label>Senha</label>
				<input type="password" class="form-control" placeholder="" name="password" id="password">
			</div> <!-- form-group end.// -->
		</div> 

		<div class="form-row">
			<div class="col form-group">
				<label>Rua</label>
			  	<input type="text" class="form-control" placeholder="" name="rua" id="rua">
			</div> <!-- form-group end.// -->
			<div class="col form-group">
				<label>Cep</label>
				<input type="text" class="form-control" placeholder="" name="cep" id="cep">
			</div>
			<div class="col form-group">
				<label>Estado</label>
				<input type="text" class="form-control" placeholder="" name="estado" id="estado">
			</div> 

			<div class="col form-group">
				<label>Cidade</label>
				<input type="text" class="form-control" placeholder="" name="cidade" id="cidade">
			</div> <!-- form-group end.// -->
		
		</div>
		
	    
	    <div class="form-group" style="margin-left: -15px">
	    	<div class="col-md-12 text-left">
	        <button type="button" class="btn btn-primary cadastrarUser" name="salvar">Cadastrar</button>
	    </div>

	    </div> <!-- form-group// -->      
    
			</form>
			</article> <!-- card-body end .// -->

		</div> <!-- card.// -->
	</div>
</div>

</div>
</section>


@endsection