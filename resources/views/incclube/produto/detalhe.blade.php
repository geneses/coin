@extends('incclube/layouts/template')
@section('title', 'Detalhe do produto')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- ========================= SECTION TOPBAR ========================= -->
<section class="section-topbar border-top padding-y-sm">
<div class="container">
	<span>	<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index.php">Início</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detalhe do produto</li>
</ol> </span>
	<div class="float-right dropdown">
	
	</div> 
</div> <!-- container.// -->
</section>
<!-- ========================= SECTION TOPBAR .// ========================= -->

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content bg padding-y-sm">
<div class="container">

<div class="row">
<div class="col-xl-12 col-md-12 col-sm-12">
<main class="card">
	<div class="row no-gutters">
		<aside class="col-sm-6 border-right">
<article class="gallery-wrap"> 
<div class="img-big-wrap">

<div> <a href=" {{ asset($produto->imagem) }}" data-fancybox="">
	<img src=" {{ asset($produto->imagem) }}"></a>
</div>

</div>

<div class="img-small-wrap">
	@foreach ($fotos as $valor){  
		<div class="item-gallery"><a href=" {{ asset($value->imagem) }}" data-fancybox=""> <img src="{{ asset($value->imagem) }}"/></a></div>
	@endforeach
</div> 

</article> 
		</aside>
		<aside class="col-sm-6">
<article class="card-body">

	<h3 class="title mb-3">{{ $produto->nome_produto }}</h3>

<div class="mb-3"> 
	<var class="price h3 text-warning" > 
		<span style="font-size: 20px" class="currency" >  </span><span style="font-size: 20px" class="num">
		</span><br>
	</var> 
</div> <!-- price-detail-wrap .// -->

<dl class="row">
 <strong style="font-size: 30px; margin-left: 15px; margin-top: -30px">R$@if(Auth::user())
                @if(!Auth::user()->hasRole('usuario_site'))
				{{  $produto->valor_v_usuario }}
				
				@else
				{{  $produto->valor_v_site }}
				@endif
				
				@else
				{{  $produto->valor_v_site }}
			@endif</strong>

</dl>

<hr>
	<div class="row">
		<div class="col-sm-5">
			<dl class="dlist-inline">
			  <dt>Quantidade: </dt>
			  <dd> 
			  	{{ $produto->estoque }}
			  </dd>
			</dl>  <!-- item-property .// -->
		</div> <!-- col.// -->
		
	</div> <!-- row.// -->
	<hr>
		<input type="hidden" id="valor" name="valor" value="{{$produto->valor_v_usuario}}">
		<input type="hidden" id="fk_produto" name="fk_produto" value="{{$produto->id}}">
		<input type="numer" id="qtd" name="qtd" >
		<a href="" class="btn  btn-warning carrinho" data-toggle="modal" data-target="#">  <i class="fa fa-cart-plus"></i> Adicionar ao carrinho</a>
	
	<!--	<a href="#" class="btn" style="background-color: red; color: white"><i class="fa fa-envelope"></i> Ação não permitida</a>	
	
		<a href="" class="btn  btn-warning" data-toggle="modal" data-target="#pergunta">  <i class="fa fa-envelope"></i> Enviar mensagem</a>
-->

<!-- short-info-wrap .// -->
</article> <!-- card-body.// -->
		</aside> <!-- col.// -->
	</div> <!-- row.// -->
</main> <!-- card.// -->

<!-- PRODUCT DETAIL -->
<article class="card mt-3">
	<div class="card-body">
		<h4>Descrição</h4>
	<p>	{!! $produto->descricao !!}</p>

	</div> <!-- card-body.// -->
</article> <!-- card.// -->

<!-- PRODUCT DETAIL .// -->

</div> <!-- col // -->

</div> <!-- row.// -->



</div><!-- container // -->
</section>
<!-- ========================= SECTION CONTENT .END// ========================= -->


<div class="modal fade" id="pergunta" tabindex="-1" role="dialog" aria-labelledby="Fotos" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
	    <form method="post">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Mensagem</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
					<textarea name="pergunta" class="form-control" rows="5"> </textarea>
					<input type="hidden" name="fk_produto" value="">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
	      
	        	<button type="submit" name="salvar_pergunta" class="btn btn-primary">Perguntar</button>
	    	
	    		<label style="background-color: red; padding: 5.5px; margin-top: 4px; color: white">Faça o seu login</label>
	      </div>
	    </div>
	    </form>
  </div>
</div>

@endsection