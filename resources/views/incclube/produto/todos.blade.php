@extends('incclube/layouts/template')
@section('title', 'Index')
@section('content')

<style type="text/css">
	.tales {
  width: 100%;
}
.carousel-inner{
  width:100%;
  max-height: 400px !important;
}

.imagens{
	width:100%;
  max-height: 400px !important;	
}
</style>

<section class="section-content bg padding-y-sm">
<div class="container">

<div class="row-sm">
@foreach($produtos as $value)
<div class="col-md-3 col-sm-6">
	<figure class="card card-product">
		<div class="img-wrap"><a href="{{ route('incclube.detalhe',$value->id) }}"><img width="100%" src="{{ asset($value->imagem) }}"></div></a>
		<figcaption class="info-wrap text-center">
			<a href="{{ route('incclube.detalhe',$value->id) }}" class="title" style="font-size: 15px; color: black">{{$value->nome }}</a>
			<div class="price-wrap text-center" >
				<span class="price-new"><strong style="font-size: 20px">R$ 
          @if(Auth::user())
            @if(!Auth::user()->hasRole('usuario_v_usuario'))
              {{  $value->valor_v_usuario }}
              
              @else
              {{  $value->valor_v_site }}
              @endif
              
              @else
              {{  $value->valor_v_site }}
          @endif
        </strong></span><br>	
			</div> <!-- price-wrap.// -->
		</figcaption>
	</figure> <!-- card // -->
</div> <!-- col // -->

@endforeach


</div> <!-- row.// -->


</div><!-- container // -->
</section>

@endsection


