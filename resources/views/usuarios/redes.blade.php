@extends('adminlte::page')

<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/Categoria.js') }}" type = "text/javascript" ></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type = "text/javascript"></script>
<script src ="{{ asset('/js/scripts_gerais/captura.js') }}" type = "text/javascript" ></script>

<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
    <link href="{{ asset('css/redes.css') }}" rel="stylesheet">

@section('htmlheader_title')
	Rede binário
@endsection

@section('main-content')


<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box box-success">
                   				                    
                    <div class="box-body text-center">
						<div class="col-md-12">
	                    	<a href="@if($user) {{ route('redes', $user->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color:<?php if(isset($user->qualificacao->cor)){ echo $user->qualificacao->cor; }?>;" class="img-circle" width="80px" height="80px" src="{{  asset($user->avatar) }}"></a>

	                    	<span class="nome"> {{ $user->user }}</span>
							
							<div class="divisao">&nbsp;</div>
							<div class="img">

								<a href="@if($filho_zero) {{ route('redes', $filho_zero->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($filho_zero->qualificacao->cor)){ echo  $filho_zero->qualificacao->cor; } ?>" class="img-circle" width="80px" height="80px" src="@if($filho_zero) {{  asset($filho_zero->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
								<span class="nome">@if($filho_zero) {{ $filho_zero->user }}@else - @endif</span>
							</div>

							<a href="@if($filho_um) {{ route('redes', $filho_um->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($filho_um->qualificacao->cor)){ echo $filho_um->qualificacao->cor; }?>;" class="img-circle" width="80px" height="80px" src="@if($filho_um) {{  asset($filho_um->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
							<span class="nome">@if($filho_um) {{ $filho_um->user }}@else - @endif</span>

							<div class="col-md-6">
								<div class="divisao">&nbsp;</div>
								<div class="img">
									<a href="@if($neto_zero_zero) {{ route('redes', $neto_zero_zero->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_zero_zero->qualificacao->cor)){ echo $neto_zero_zero->qualificacao->cor; } ?>;" class="img-circle" width="80px" height="80px" src="@if($neto_zero_zero) {{ asset($neto_zero_zero->avatar) }} @else {{ asset('img/user.png') }}  @endif "></a>
									<span class="nome">@if($neto_zero_zero) {{ $neto_zero_zero->user }}@else - @endif</span>
								</div>

								<a href="@if($neto_zero_um) {{ route('redes', $neto_zero_um->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_zero_um->qualificacao->cor)){ echo $neto_zero_um->qualificacao->cor;} ?>;" class="img-circle" width="80px" height="80px" src="@if($neto_zero_um) {{  asset($neto_zero_um->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
								<span class="nome">@if($neto_zero_um) {{ $neto_zero_um->user }}  @else - @endif</span>

								<div class="col-md-12">
									<div class="col-md-6">
										<div class="divisao">&nbsp;</div>
										<div class="img">
											<a href="@if($neto_zero_zero_zero) {{ route('redes', $neto_zero_zero_zero->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_zero_zero_zero->qualificacao->cor)){ echo $neto_zero_zero_zero->qualificacao->cor; } ?>;" class="img-circle" width="80px" height="80px" src="@if($neto_zero_zero_zero) {{  asset($neto_zero_zero_zero->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
											<span class="nome">@if($neto_zero_zero_zero) {{ $neto_zero_zero_zero->user }}  @else - @endif</span>
										</div>

										<a href="@if($neto_zero_zero_um) {{ route('redes', $neto_zero_zero_um->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_zero_zero_um->qualificacao->cor)){ echo $neto_zero_zero_um->qualificacao->cor; } ?>;" class="img-circle" width="80px" height="80px" src="@if($neto_zero_zero_um) {{  asset($neto_zero_zero_um->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
										<span class="nome">@if($neto_zero_zero_um) {{ $neto_zero_zero_um->user }} @else - @endif</span>

									</div>

									<div class="col-md-6">
										<div class="divisao">&nbsp;</div>
										<div class="img">

											<a href="@if($neto_zero_um_zero) {{ route('redes', $neto_zero_um_zero->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_zero_um_zero->qualificacao->cor)){ echo $neto_zero_um_zero->qualificacao->cor; } ?>;" class="img-circle"  width="80px" height="80px" src="@if($neto_zero_um_zero) {{  asset($neto_zero_um_zero->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
											<span class="nome">@if($neto_zero_um_zero) {{ $neto_zero_um_zero->user }} @else - @endif</span>
										</div>

										<a href="@if($neto_zero_um_um) {{ route('redes', $neto_zero_um_um->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_zero_um_um->qualificacao->cor)){ echo $neto_zero_um_um->qualificacao->cor; } ?>;" class="img-circle" width="80px" height="80px" src="@if($neto_zero_um_um) {{  asset($neto_zero_um_um->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
										<span class="nome">@if($neto_zero_um_um) {{ $neto_zero_um_um->user }} @else - @endif</span>

									</div>
								</div>


							</div>


							<div class="col-md-6">
								<div class="divisao">&nbsp;</div>
									<div class="img">

								<a href="@if($neto_um_zero) {{ route('redes', $neto_um_zero->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_um_zero->qualificacao->cor)){ echo $neto_um_zero->qualificacao->cor; } ?>;" class="img-circle"  width="80px" height="80px" src="@if($neto_um_zero) {{  asset($neto_um_zero->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
								<span class="nome">@if($neto_um_zero)  {{ $neto_um_zero->user }}@else - @endif</span>
							</div>

							<a href="@if($neto_um_um) {{ route('redes', $neto_um_um->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_um_um->qualificacao->cor)){ echo $neto_um_um->qualificacao->cor; } ?>;" class="img-circle" width="80px" height="80px" src="@if($neto_um_um) {{  asset($neto_um_um->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
							<span class="nome">@if($neto_um_um) {{ $neto_um_um->user }}@else - @endif</span>

							<div class="col-md-12">
									<div class="col-md-6">
										<div class="divisao">&nbsp;</div>
										<div class="img">
											<a href="@if($neto_um_zero_zero) {{ route('redes', $neto_um_zero_zero->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_um_zero_zero->qualificacao->cor)) {$neto_um_zero_zero->qualificacao->cor; } ?>;" class="img-circle" width="80px" height="80px" src="@if($neto_um_zero_zero) {{  asset($neto_um_zero_zero->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
											<span class="nome">@if($neto_um_zero_zero) {{ $neto_um_zero_zero->user }} @else - @endif</span>
										</div>

										<a href="@if($neto_um_zero_um) {{ route('redes', $neto_um_zero_um->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_um_zero_um->qualificacao->cor)) {$neto_um_zero_um->qualificacao->cor;} ?>;" class="img-circle" width="80px" height="80px" src="@if($neto_um_zero_um) {{  asset($neto_um_zero_um->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
										<span class="nome">@if($neto_um_zero_um) {{ $neto_um_zero_um->user }} @else - @endif</span>

									</div>

									<div class="col-md-6">
										<div class="divisao">&nbsp;</div>
										<div class="img">

											<a href="@if($neto_um_um_zero) {{ route('redes', $neto_um_um_zero->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_um_um_zero->qualificacao->cor)) {$neto_um_um_zero->qualificacao->cor;} ?>;" class="img-circle"  width="80px" height="80px" src="@if($neto_um_um_zero) {{  asset($neto_um_um_zero->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
											<span class="nome">@if($neto_um_um_zero) {{ $neto_um_um_zero->user }} @else - @endif</span>
										</div>

										<a href="@if($neto_um_um_um) {{ route('redes', $neto_um_um_um->id) }} @endif" class="user"  style="background:#EBEBEB"><img style="border: outset;border-color: <?php if(isset($neto_um_um_um->qualificacao->cor)) {$neto_um_um_um->qualificacao->cor;} ?>;" class="img-circle" width="80px" height="80px" src="@if($neto_um_um_um) {{  asset($neto_um_um_um->avatar) }} @else {{ asset('img/user.png') }}  @endif"></a>
										<span class="nome">@if($neto_um_um_um)  {{ $neto_um_um_um->user }}@else - @endif</span>

									</div>
								</div>

							</div>				
						</div>
                    </div>
               </div>
            </div>

        <div class="container-fluid spark-screen">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-success">			                    
                    <div class="box-body text-left">
                    	@foreach($qualificacaos as $qualificacao)
                    	<div class="col-md-3">
                    	<span style="background: {{$qualificacao->cor }};">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> {{ $qualificacao->nome }}
                    	</div>
                    	@endforeach
                    </div>
                    </div>
                </div>
            </div>
        </div>
       

        <div class="col-md-12">
                    <div class="box box-default">
                    <div class="box-body">
                           <table class="table">
                                    <thead>
                                        <th>Qualificação binária</th>
                                        <th>Posição da rede</th>
                                    </thead>
                                    <tr>
                                        <td width="20px">Ativa</td>
                                        <td>
                                          <form id="form">
                                              {{csrf_field()}}
                                            <select class="form-control" name="lado_rede">
                                              @if($user->lado_rede == 0) 
                                                <option value="0" selected="">ESQUERDA</option>
                                                @else
                                                <option value="0">ESQUERDA</option>
                                              @endif

                                              @if($user->lado_rede == 1) 
                                                <option value="1" selected="">DIREITA</option>
                                                @else
                                                <option value="1">DIREITA</option>
                                              @endif
                                            </select>
                                            </form>
                                        </td>
                                            
                                        <td><button class="btn btn-primary alterar_lado">Alterar</button> </td>
                                    </tr>
                                </table>
                        </div>
                    </div>
    	</div>
						
</div>

        </div>
@endsection

<!--

	https://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028.jpg?s=80&d=mm&r=g

	-->