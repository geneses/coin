@extends('adminlte::page')
<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/cadastro_user.js') }}" type = "text/javascript" ></script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 

@section('htmlheader_title')
    Transferência de saldo
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                        <div class="box-body">
<div class="col-sm-8 col-md-offset-2" style="    background-color: white;"> 

 <form class="form-horizontal" role="form" id="form" >
       {{csrf_field()}}
 				<div class="form-group">
 					<h2 class="text-center">Transferência de saldo</h2>
                    <div class="col-sm-12">
                            <strong>Seu saldo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Banco" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" value="{{ $saldo }}">
                            </div>       
                        </div>
                        
                    <div class="col-sm-12 hidden" id="nome-user">
                            <strong>Nome do usuário:</strong>
                            <div class="input-group ">
                                <span data-toggle="tooltip" title="User" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" id="nome_user">
                            </div>       
                        </div>
                        

                          <div class="col-sm-12">
                            <strong>User:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Agencia" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="patrocinador"  id="patrocinador">
                            </div>       
                        </div> 

                         <div class="col-sm-12">
                            <strong>Valor a ser transferido:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Agencia" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input onpaste="return false;" onkeypress="moeda(event)"  type="text" maxlength="13" onkeydown="FormataMoeda(this, 10, event)"  type="text" maxlength="254" class="form-control" name="valor"  id="valor">
                            </div>       
                        </div>

                        <div class="col-sm-12">
                            <strong>Senha de transação:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Agencia" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="password" maxlength="254" class="form-control" name="password"  id="password">
                            </div>       
                        </div> 
                    
                         <input type="hidden" maxlength="254" class="form-control" name="id"  id="id" value="{{ Auth::user()->id}}">

                         <div class="text-center" style="margin-top: 40px">


                            @if($saldo > 0)
                            
                                <button id="salvar" style="font-size: 20px; margin-bottom: 20px; margin-top: 20px" type="button" class="btn btn-action btn-success transferencia" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">transferir
                            @else
                                <button style="font-size: 20px; margin-bottom: 20px; margin-top: 20px" type="button" class="btn btn-action btn-danger" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">
                            Sem saldo suficiente para fazer transferir 

                            @endif
                        </button>
                    </div>
                         
                </div>
               </form>       
            </div>

              </div>
                    </div>
                </div>
        </div>
</div>
@endsection
