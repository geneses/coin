@extends('adminlte::page')
<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/cadastro_user.js') }}" type = "text/javascript" ></script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 

@section('htmlheader_title')
    Transferência de saldo
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                        <div class="box-body">
<div class="col-sm-12 table-responsive" style="    background-color: white;"> 
    <table class="table table-striped">
        <thead>
            <th>Nível</th>
            <th>Usuários indicados</th>
            <th>Status</th>
            <th>Data cadastro</th>
            <th>Ativação</th>
                          
        </thead>
        @foreach($usuarios as $usuario)
        <tr>
            <td>1</td>
            <td><a id="{{$usuario->user}}" class="usuario" data-toggle="modal" href="#sdas">{{$usuario->user}}</a></td>            <td>{{$usuario->status}}</td>
            <td>{{$usuario->created_at->format('d/m/Y')}}</td>
            <td></td>
        </tr>
        @endforeach
        <tr>
            <td><strong>Total:</strong> {{$usuarios->count('id')}}</td>
        </tr>
    </table>   
    </div>
    </div>
</div>
</div>
</div>
</div>


<div class="hidden container-fluid spark-screen" id="indicados">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                        <div class="box-body">
<div class="col-sm-12 table-responsive" style="    background-color: white;"> 
    <table class="table table-striped">
        <thead>
            <th>Nível</th>
            <th>Usuários indicados</th>
            <th>Status</th>
            <th>Data cadastro</th>
            <th>Ação</th>           
        </thead>
        <tbody id="table_patrocinador">
        </tbody>
    </table>   
    </div>
    </div>
</div>
</div>
</div>
</div>
@endsection
