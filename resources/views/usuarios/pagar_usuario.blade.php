@extends('adminlte::page')

<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/user.js') }}" type = "text/javascript" ></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type = "text/javascript"></script>

<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>

@section('htmlheader_title')
	Buscar Usuário
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box box-success">

                    <div class="box-header with-border">
                        <h3 class="box-title">Usuário</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <?php if($usuario != null){ ?>
                       <table>
                           <tr>
                               <td>Usuário</td>
                               <td> {{$usuario->user}}</td>
                           </tr> 

                           <tr>
                               <td>Nome</td>
                               <td> {{$usuario->nome}}</td>
                           </tr>
                           <tr>
                               <td>E-mail</td>
                               <td> {{$usuario->email}}</td>
                           </tr>
                           <tr>
                               <td>Data de cadastro</td>
                               <td> {{$usuario->data_cadastro}}</td>
                           </tr>
                       </table>  


                    <div class="pull-left" style="margin-top: 20px">      
                            <a class="btnPagar btn btn-primary btn-sm" title="Pagar usuario" data-toggle="tooltip"><span class="glyphicon glyphicon-plus"></span>Efetuar pagamento</a>
                        </div>
                    </div>
                    


                        <?php }else{ ?>
                        <h3> Usuário não encontrado </h3>

                        <a href="{{ route('gerenciar-buscar-usuario.index') }}" class="btn btn-primary" >Voltar</a>
                        <?php } ?>
                    <!-- /.box-body -->   
                </div>

			</div>
		</div>
	</div>
   <?php if($usuario != null){ 
    ?>
@include('usuarios.modals.pagar')
<?php } ?>
@endsection
