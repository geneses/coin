@extends('adminlte::page')
<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/cadastro_user.js') }}" type = "text/javascript" ></script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 

@section('htmlheader_title')
    Alterar senha
@endsection
@section('menu','dados')
@section('main-content')
<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    
                        <div class="box-body">

                      

<div class="col-sm-8 col-md-offset-2" style="    background-color: white;"> 

 <form class="form-horizontal" role="form" id="form" >
       {{csrf_field()}}
 				<div class="form-group">
 					<h2 class="text-center">Alterar senha</h2>
                        <div class="col-sm-12">
                            <strong>Senha atual:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Banco" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="password" maxlength="254" class="form-control" name="passwordVerify"  id="passwordVerify">
                            </div>       
                        </div>

                        <div class="col-sm-12">
                            <strong>Nova senha:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Agencia" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="password" maxlength="254" class="form-control" name="password"  id="password">
                            </div>       
                        </div> 
                    
                         <input type="hidden" maxlength="254" class="form-control" name="id"  id="id" value="{{ Auth::user()->id}}">

                         <div class="text-center" style="margin-top: 40px">
                
                          <button style="font-size: 20px; margin-bottom: 20px; margin-top: 20px" type="button" class="btn btn-action btn-success alterarSenha" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Alterar
                        </button>
                    </div>
                         
                </div>
               </form>       
            </div>

              </div>
                    </div>
                </div>
        </div>
</div>
@endsection
