<div id="criar_editar-modal" class="modal fade bs-example" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div> <!-- Fim de ModaL Header-->

      <div class="modal-body">

        <div class="erros callout callout-danger hidden">
                <p></p>
        </div>

       <form class="form-horizontal" role="form" id="form" >
                    <div class="form-group">
                        <div class="col-sm-12">
                            <strong>Saldo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip"  class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php if(isset(Auth::user()->saldo[0]->saldo)){ ?>
                                <input readonly="" value="R$ {{ number_format(Auth::user()->saldo[0]->saldo, 2, ',', '.') }}" type="text" maxlength="254" class="form-control" name="saldo"  id="saldo">
                                <?php } ?>
                            </div>       
                        </div>
                    </div>  

                    <div class="form-group">
                        <div class="col-sm-12">
                            <strong>Nome:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" value="{{ $usuario->nome }}" type="text" maxlength="254" class="form-control" name="nome"  id="nome">
                            </div>       
                        </div>
                    </div>  
                    <input type="hidden" name="fk_adesao_usuario" value="{{ $usuario->id }}">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <strong>User:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" value="{{ $usuario->user }}" type="text" maxlength="254" class="form-control" name="nome"  id="nome">
                            </div>       
                        </div>
                    </div>  

                    <div class="form-group">
                        <div class="col-sm-12">
                            <strong>Pacote:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Pacote" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select class="form-control" name="pacote"  id="pacote">
                                  @foreach($pacotes as $pacote)
                                  <option value="{{$pacote->id}}">{{$pacote->nome}} - R$ {{$pacote->valor_pacote}}</option>
                                  @endforeach
                                </select>
                            </div>       
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <strong>Senha de transação:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="password" maxlength="254" class="form-control" name="senha_transacao"  id="senha_transacao">
                            </div>       
                        </div>
                    </div>  
                </form>

      </div> <!-- Fim de ModaL Body-->


      <div class="modal-footer">
        <button type="button" class="btn btn-action btn-success pagarusuario" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Pagar
          <i class="fa fa-floppy-o"> </i>
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <i class='fa fa-times'></i>
        </button>
      </div> <!-- Fim de ModaL Footer-->

    </div> <!-- Fim de ModaL Content-->

  </div> <!-- Fim de ModaL Dialog-->

</div> <!-- Fim de ModaL Usuario-->