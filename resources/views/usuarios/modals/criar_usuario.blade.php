<div id="criar_editar-modal" class="modal fade bs-example" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div> <!-- Fim de ModaL Header-->

      <div class="modal-body">

        <div class="erros callout callout-danger hidden">
                <p></p>
        </div>
       <form class="form-horizontal" role="form" id="form" >

                    <div class="form-group">
                        <div class="col-sm-6">
                            <strong>Nome:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="nome"  id="nome">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>User name:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="User name" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input readonly="" type="text" maxlength="254" class="form-control" name="user"  id="user">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>Senha:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Senha" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="password"  id="password">
                            </div>       
                        </div>
                    </div>

                    <div class="form-group">
                   
                        <div class="col-sm-3">
                            <strong>Data de nascimento:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Data de nascimento" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="date" maxlength="254" class="form-control" name="data_nascimento"  id="data_nascimento">
                            </div>       
                        </div>

                         <div class="col-sm-4">
                            <strong>Status:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Status" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select name="status"  id="status" class="form-control">
                                    <option>Ativo</option>
                                    <option>Bloqueado</option>
                                    <option>Inativo</option>
                                    <option>Pago</option>
                                    <option>Pendente</option>
                                </select>
                            </div>       
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <strong>CPF:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="cpf" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cpf"  id="cpf">
                            </div>       
                        </div>
 
                        <div class="col-sm-4">
                            <strong>CNPJ:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="CNPJ" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cnpj"  id="cnpj">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Telefone:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Telefone" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="telefone"  id="telefone">
                            </div>       
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <strong>E-mail:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="E-mail" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="email"  id="email">
                            </div>       
                        </div>
                    
                        <div class="col-sm-4">
                            <strong>Rua:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Rua" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="rua"  id="rua">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Complemento:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Complemento" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="complemento"  id="complemento">
                            </div>       
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <strong>Cidade:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Cidade" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cidade"  id="cidade">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Estado:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Estado" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="estado"  id="estado">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Cep:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Cep" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cep"  id="cep">
                            </div>       
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3">
                            <strong>Sexo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Sexo" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select class="form-control" name="sexo"  id="sexo">
                                  <option>MASCULINO</option>
                                  <option>FEMININO</option>
                                </select>
                            </div>       
                        </div>
                    
                    <div class="form-group">
                    
                       
                        <div class="col-sm-3">
                            <strong>Numero:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Numero" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="numero"  id="numero">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>Bairro:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Bairro" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="bairro"  id="bairro">
                            </div>       
                        </div>
                    </div>
                            <input type="hidden" id="id" name="id">
                </form>

      </div> <!-- Fim de ModaL Body-->


      <div class="modal-footer">
        <button type="button" class="btn btn-action btn-success add" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Cadastrar
          <i class="fa fa-floppy-o"> </i>
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <i class='fa fa-times'></i>
        </button>
      </div> <!-- Fim de ModaL Footer-->

    </div> <!-- Fim de ModaL Content-->

  </div> <!-- Fim de ModaL Dialog-->

</div> <!-- Fim de ModaL Usuario-->