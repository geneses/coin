@extends('adminlte::page')
<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/cadastro_user.js') }}" type = "text/javascript" ></script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 

@section('htmlheader_title')
    Cadastro de afiliado
@endsection
@section('menu','dados')
@section('main-content')
<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    
                        <div class="box-body">

                      

<div class="col-sm-8 col-md-offset-2" style="    background-color: white;"> 

 <form class="form-horizontal" role="form" id="form" >
       {{csrf_field()}}
 				<div class="form-group">
 				                       
                        <input type="hidden" maxlength="254" class="form-control" name="id"  id="id" value="{{ Auth::user()->id }}">
                        
                        <h2 class="text-center">Dados pessoais</h2>
                        <div class="form-group">
                        <div class="col-sm-8">
                            <strong>Nome:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="nome"  id="nome" value="{{ Auth::user()->nome }}">
                            </div>       
                        </div>
          
                        <div class="col-sm-4">
                            <strong>RG:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="RG" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="rg"  id="rg" value="{{ Auth::user()->rg }}">
                            </div>       
                        </div>

                        
                         <div class="col-sm-5">
                            <strong>CPF:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="cpf" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cpf"  id="cpf" value="{{ Auth::user()->cpf }}">
                            </div>       
                        </div>

                        <div class="col-sm-3">
                            <strong>Data de nascimento:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Data de nascimento" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="date" maxlength="254" class="form-control" name="data_nascimento"  id="data_nascimento" value="{{ Auth::user()->data_nascimento }}">
                            </div>       
                        </div>


                        <div class="col-sm-6">
                            <strong>Orgão expedidor:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Orgão expedidor" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="orgao_expedidor"  id="orgao_expedidor" value="{{ Auth::user()->orgao_expedidor }}">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>PIS/PASEP:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="PIS/PASEP" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="pis"  id="pis" value="{{ Auth::user()->pis }}">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>Telefone:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Telefone" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="telefone"  id="telefone" value="{{ Auth::user()->telefone }}">
                            </div>       
                        </div>

                        <div class="col-sm-6">
                            <strong>Telefone:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Telefone" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="telefone2"  id="telefone2" value="{{ Auth::user()->telefone2 }}">
                            </div>       
                        </div>
                    </div>

                     <h2 class="text-center">Dados de endereço</h2>

                     <div class="form-group">
                        <div class="col-sm-4">
                            <strong>Rua:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Rua" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="rua"  id="rua" value="{{ Auth::user()->rua }}">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Complemento:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Complemento" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="complemento"  id="complemento" value="{{ Auth::user()->complemento }}">
                            </div>       
                        </div>
                   

                    
                        <div class="col-sm-4">
                            <strong>Cidade:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Cidade" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cidade"  id="cidade" value="{{ Auth::user()->cidade }}">
                            </div>       
                        </div>
                    </div>

                    <div class="form-group">
                         <div class="col-sm-3">
                            <strong>Estado:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Estado" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="estado"  id="estado" value="{{ Auth::user()->complemento }}">
                            </div>       
                        </div>

                    	 <div class="col-sm-3">
                            <strong>Cep:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Cep" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="cep"  id="cep" value="{{ Auth::user()->cep }}">
                            </div>       
                        </div>
                      
                        <div class="col-sm-2">
                            <strong>Numero:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Numero" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="numero"  id="numero" value="{{ Auth::user()->numero }}">
                            </div>       
                        </div>

                        <div class="col-sm-4">
                            <strong>Bairro:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Bairro" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="bairro"  id="bairro" value="{{ Auth::user()->bairro }}">
                            </div>       
                        </div>
                    </div>

                    
                    <div class="text-center" style="margin-top: 40px">
                       
                          <button style="font-size: 20px; margin-bottom: 20px; margin-top: 20px" type="button" class="btn btn-action btn-success updatePostUsuario" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Alterar
				        </button>
                    </div>
                </form>
            </div>

              </div>
                    </div>
                </div>
        </div>
</div>
@endsection
