@extends('adminlte::page')

<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type = "text/javascript"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 
@section('htmlheader_title')
	Buscar Produto
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Produtos</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                    <form class="form-horizontal" role="form" id="form"  method="POST" action="{{ url('/gerenciar-produtos/get-produto') }}">
                        {{csrf_field()}}
                    <input type="text" class="form-control" name="produto">

                    <button type="submit" class="btn btn-action btn-success buscar" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Buscar
                  
                    </button>
                    </form>
                        
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>

@endsection
