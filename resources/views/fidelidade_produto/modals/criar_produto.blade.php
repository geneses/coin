<div id="criar_editar-modal" class="modal fade bs-example" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div> <!-- Fim de ModaL Header-->

      <div class="modal-body">

        <div class="erros callout callout-danger hidden">
                <p></p>
        </div>
       <form class="form-horizontal" role="form" id="form" enctype="multipart/form-data" >
                    <div class="form-group">
                        <div class="col-sm-6">
                            <strong>Nome:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input onpaste="return false;" type="text" maxlength="254" class="form-control" name="nome"  id="nome">
                            </div>       
                        </div>
   
                        <div class="col-sm-2">
                            <strong>Estoque:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Estoque" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input  type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="estoque"  id="estoque">
                            </div>       
                        </div>

                         <div class="col-sm-3">
                            <strong>Valor do ponto fidelidade:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor fidelidade" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="13" onkeydown="FormataMoeda(this, 10, event)" class="form-control" name="valor_ponto_fidelidade"  id="valor_ponto_fidelidade">
                            </div>       
                        </div>

                    
                        <div class="col-sm-4">
                            <strong>Valor da compra:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor da compra" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="13" onkeydown="FormataMoeda(this, 10, event)" class="form-control" name="valor_compra"  id="valor_compra">
                            </div>       
                        </div>



                        <div class="col-sm-3">
                            <strong>Imagem:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Imgem" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="file" maxlength="254" class="form-control" name="imagem"  id="imagem">
                            </div>       
                        </div>
                        <div class="col-sm-4">
                            <strong>Categoria:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Comissão do produto" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select id="fk_categoria" name="fk_categoria" class="form-control">
                                    @foreach($categorias as $categoria)
                                    <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                                    @endforeach
                                </select>
                            </div>       
                        </div> 

                        <div class="col-sm-4">
                            <strong>Ativo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Comissão do produto" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select id="ativo" name="ativo" class="form-control">
                                    <option value="true">Ativo</option>
                                    <option value="false">Inativo</option>
                                </select>
                            </div>       
                        </div>
                    </div>
                    
                    <div class="col-sm-12">
                        <strong>Descrição:</strong>
                        <div class="input-group">
                             <textarea id="descricao" name="descricao"></textarea>
                        </div>       
                    </div>

                    <script>
                        CKEDITOR.replace('descricao');
                    </script>

                    <div class="col-sm-9">
                        <strong>Peso:</strong>
                        <div class="input-group">
                            <span data-toggle="tooltip" title="Peso" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                            <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="peso"  id="peso">
                        </div>       
                    </div>
                    
                        <input type="hidden" id="id" name="id">
                </form>

      </div> <!-- Fim de ModaL Body-->


      <div class="modal-footer">
        <button type="button" class="btn btn-action btn-success add" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Cadastrar
          <i class="fa fa-floppy-o"> </i>
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <i class='fa fa-times'></i>
        </button>
      </div> <!-- Fim de ModaL Footer-->

    </div> <!-- Fim de ModaL Content-->

  </div> <!-- Fim de ModaL Dialog-->

</div> <!-- Fim de ModaL Usuario-->