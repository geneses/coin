@extends('adminlte::page')
<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
<script src ="{{ asset('/js/scripts_gerais/financeiro_usuario.js') }}" type = "text/javascript" ></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}" type = "text/javascript"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}"> 

@section('htmlheader_title')
    Solicitar senha
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                        <div class="box-body">
<div class="col-sm-8 col-md-offset-2" style="    background-color: white;"> 

 <form class="form-horizontal" role="form" id="form" enctype="multipart/form-data">
       {{csrf_field()}}
                <div class="form-group">
                    <h2 class="text-center">Solicitar nova senha do financeiro</h2>
                        <div class="col-sm-12">
                            <strong>Digite sua senha de login:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Avatar" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="password" maxlength="254" class="form-control" name="password"  id="password">
                            </div>       
                        </div>

                         <div class="text-center" style="margin-top: 40px">
                
                          <button style="font-size: 20px; margin-bottom: 20px; margin-top: 20px" type="button" class="btn btn-action btn-success senhaFinanceiro" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Solicitar
                        </button>
                    </div>
                         
                </div>
               </form>       
            </div>

              </div>
                    </div>
                </div>
        </div>
</div>
@endsection
