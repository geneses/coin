@extends('incclube/layouts/template')
@section('title', 'Checkout')
@section('content')
<script src="{{ asset('/js/scripts_gerais/checkout.js' . '?update=' . str_random(3) ) }}" type="text/javascript">
</script>
<link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
<script src="{{ asset('js/iziToast.min.js') }}"></script>

<div class="container">
    <div class="text-center">
        <form class="form-horizontal" id="form">
            <div class="row">
                <div class="col col-md-8">

                    {{ csrf_field() }}
                    <hr class="mb-4">
                    <h4 class="mb-3">Pagamento</h4>
                    @foreach($carrinhos as $carrinho)
                        <input type="hidden" name="fk_produtos[]" value="{{$carrinho->fk_produto}}"/>
                    @endforeach
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="cc-name">Nome completo</label>
                            <input type="text" name="card_holder_name" id="card_holder_name"
                                class="form-control input-lg" placeholder="Nome (igual no cartão)">
                            <div class="invalid-feedback">
                                Name on card is required
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="cc-number">Número do cartão</label>
                            <input type="text" name="card_number" id="card_number"
                                onkeyup="getCreditCardLabel(this.value)" class="form-control input-lg n_card"
                                placeholder="Número do Cartão">
                            <div class="invalid-feedback">
                                Credit card number is required
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">Mês</label>
                            <select class="form-control input-lg" name="card_expiration_month"
                                id="card_expiration_month">
                                <option selected>Mês</option>
                                <option>01</option>
                                <option>02</option>
                                <option>03</option>
                                <option>04</option>
                                <option>05</option>
                                <option>06</option>
                                <option>07</option>
                                <option>08</option>
                                <option>09</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                            </select>
                            <div class="invalid-feedback">
                                Expiration date required
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">Ano</label>
                            <select class="form-control input-lg" name="card_expiration_year" id="card_expiration_year">
                                <option>Ano</option>
                                @foreach($datas as $data)
                                    <option>{{  $data->format('Y') }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Expiration date required
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">CVV</label>
                            <input type="text" class="form-control" id="cvv" name="cvv" placeholder="" required="">
                            <div class="invalid-feedback">
                                Security code required
                            </div>
                        </div>
                    </div>
                    <hr class="md-4">

                    <div class="row">
                        
                        <div class="col-md-2 ">
                            <div class="container">
                                <div class="form-group">
                                    <label for="semFrete">Retirada</label>
                                    <input type="radio" class="form-control" id="semFrete" name="frete" value="0" />
                                </div>
                                <div class="form-group">
                                    <label for="comFrete">Com frete</label>
                                    <input type="radio" class="form-control" id="comFrete" name="frete" value="1" data-valor="{{$frete->valor ?? 0}}"  />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 center">
                            <label for="parcelas">Parcelas</label>
                            <select class="form-control" name="Parcelas" id="parcelas">
                                 <option disabled value=0 selected >Selecione...</option>
                            </select>  
                        </div>
                    </div>
                    <hr class="mb-4">
                    <button class="pagar btn btn-primary btn-lg btn-block" type="button"
                        style="margin-bottom: 100px">Finalizar compra</button>

                </div>

                <div class="col col-md-4">

                    <hr class="mb-4">

                    <h4 class="mb-3">Informações</h4>
                    <div class="col-md-12 mb-12">
                        <label for="cc-expiration">Seu saldo</label>
                        <input class="form-control input-lg" readonly="" type="text" name="saldo" id="saldo"
                            value="R$ {{number_format($saldo_user->saldo ?? 0,2)}}">
                    </div>

                    <div class="col-md-12 mb-12">
                        <label for="cc-expiration">Usar saldo</label>
                        <input class="form-control input-lg"
                            placeholder="Informe quanto você quer usar para pagar com saldo" type="text"
                            name="saldo_pagar" id="saldo_pagar">
                    </div>

                    <div class="col-md-12 mb-12">
                        <label for="cc-expiration">Total da compra</label>
                        <input class="form-control input-lg" readonly="" type="text" name="valor" id="valor"
                            value="R$ {{$valor}}">
                        <input class="form-control input-lg" readonly="" type="hidden" name="valor2" id="valor2"
                            value="{{$valor}}">
                    </div>
                </div>

        </form>
    </div>
</div>
</div>

@endsection