<div id="criar_editar-modal" class="modal fade bs-example" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div> <!-- Fim de ModaL Header-->

      <div class="modal-body">

        <div class="erros callout callout-danger hidden">
                <p></p>
        </div>


       <form class="form-horizontal" role="form" id="form" >
                    <div class="form-group">
                        <div class="col-md-12">
                            <strong>Nome:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nome" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" maxlength="254" class="form-control" name="nome"  id="nome">
                            </div>       
                        </div>
                    </div>  

                    <div class="form-group">
                        <div class="col-md-3">
                            <strong>Comissao de binário:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Bonus Binário" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input onkeypress="moeda(event)" type="text" maxlength="254" class="form-control" name="comissao_binario"  id="comissao_binario">
                            </div>       
                        </div>
                   
                        <div class="col-md-3">
                            <strong>Ponto Binário:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Bonus Binário" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input onkeypress="moeda(event)" type="text" maxlength="254" class="form-control" name="bonus_binario"  id="bonus_binario">
                            </div>       
                        </div>
                    
                        <div class="col-md-3">
                            <strong>Ponto unilevel:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Bonus Binário" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="ponto_unilevel"  id="ponto_unilevel">
                            </div>       
                        </div>


                        <div class="col-md-3">
                            <strong>Disponivel:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Bonus Binário" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select class="form-control" id="disponivel" name="disponivel">
                                    <option value="1">Sim</option>
                                    <option value="0">Não</option>
                                </select>
                            </div>       
                        </div>
                    </div>  

                     <div class="form-group">
                        <div class="col-md-12">
                            <strong>Transação tipo:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor compra produto" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select class="form-control" name="fk_transacao_tipo"  id="fk_transacao_tipo">
                                  @foreach($transacoes as $transacao)
                                  <option value="{{$transacao->id}}">{{$transacao->nome}}</option>
                                  @endforeach
                                </select>
                            </div>       
                        </div>
                    </div>  

                    <div class="form-group">
                        <div class="col-md-4">
                            <strong>Valor compra produto:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor compra produto" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input onkeypress="moeda(event)" type="text" maxlength="254" class="form-control" name="v_compra_produto"  id="v_compra_produto">
                            </div>       
                        </div>
                  
                        <div class="col-md-4">
                            <strong>Valor pacote:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor compra produto" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="valor_pacote"  id="valor_pacote">
                            </div>       
                        </div>
                 
                        <div class="col-md-4">
                            <strong>Comissão indicação:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Valor compra produto" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="comissao_unilevel"  id="comissao_unilevel">
                            </div>       
                        </div>

                        <div class="col-md-4">
                            <strong>Fixo residual:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Fixo residual" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="residual"  id="residual">
                            </div>       
                        </div>

                        <div class="col-md-4">
                            <strong>Período Inicio rapido:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Inicio rapido" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="inicio_rapido"  id="inicio_rapido">
                            </div>       
                        </div>     

                        <div class="col-md-4">
                            <strong>Valor Inicio rapido:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Inicio rapido" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="inicio_rapido_valor"  id="inicio_rapido_valor">
                            </div>       
                        </div>
                     

                     <div class="col-md-4">
                            <strong>Ativo mes:</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Ativo mes" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="ativo_mes"  id="ativo_mes">
                            </div>       
                        </div>
                    </div>    
                      

                


                       <div class="form-group">
                        <div class="col-sm-8">
                            <strong>Nível</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nível" class="input-group-addon"><i class="fa fa-cube"></i></span>
                                <select class="form-control" name="nivel" id="nivel">
                                    <option>Nivel 1</option>
                                    <option>Nivel 2</option>
                                    <option>Nivel 3</option>
                                    <option>Nivel 4</option>
                                    <option>Nivel 5</option>
                                    <option>Nivel 6</option>
                                    <option>Nivel 7</option>
                                    <option>Nivel 8</option>
                                    <option>Nivel 9</option>
                                    <option>Nivel 10</option>
                                    <option>Nivel 11</option>
                                    <option>Nivel 12</option>              
                                </select>
                            </div>       
                        </div>
                    
                        <div class="col-sm-3">
                            <strong>Valor</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="pontos" class="input-group-addon"><i class="fa fa-bars"></i></span>
                                <input type="text" onkeypress="moeda(event)" maxlength="254" class="form-control" name="valor" id="valor">
                            </div>       
                        </div>

                        <div class="col-sm-1">
                            
                                <a class="btnAdcNivel btn btn-sm btn-primary" style="margin-top: 22px; padding: 11px"><i class="glyphicon glyphicon-plus"></i></a>
                        </div>
                    </div>


                      <div class="col-md-12">
                    <br>
                        <table class="table table-bordered table-responsive" id="pacotes">
                            <tr>
                                <th>Nível</th>
                                <th>Valor</th>
                                <th>Ações</th>
                            </tr>
                            <tbody id="pacote_id">
                            </tbody>
                        </table>
                    </div>   



                    <div class="form-group">
                        <div class="col-sm-8">
                            <strong>Ponto Fidelidade</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="Nível" class="input-group-addon"><i class="fa fa-cube"></i></span>
                                <select class="form-control" name="nivel2" id="nivel2">
                                    <option>Nivel 1</option>
                                    <option>Nivel 2</option>
                                    <option>Nivel 3</option>
                                    <option>Nivel 4</option>
                                    <option>Nivel 5</option>
                                    <option>Nivel 6</option>
                                    <option>Nivel 7</option>
                                    <option>Nivel 8</option>
                                    <option>Nivel 9</option>
                                    <option>Nivel 10</option>
                                    <option>Nivel 11</option>
                                    <option>Nivel 12</option>              
                                </select>
                            </div>       
                        </div>
                    
                        <div class="col-sm-3">
                            <strong>Valor</strong>
                            <div class="input-group">
                                <span data-toggle="tooltip" title="pontos" class="input-group-addon"><i class="fa fa-bars"></i></span>
                                <input type="number" maxlength="254" class="form-control" name="valor2" id="valor2">
                            </div>       
                        </div>

                        <div class="col-sm-1">
                            <a class="btnAdcPontoUnilevel btn btn-sm btn-primary" style="margin-top: 22px; padding: 11px"><i class="glyphicon glyphicon-plus"></i></a>
                        </div>
                    </div>


                      <div class="col-md-12">
                    <br>
                        <table class="table table-bordered table-responsive" id="ponto_unilevel_table">
                            <tr>
                                <th>Nível</th>
                                <th>Valor</th>
                                <th>Ações</th>
                            </tr>
                            <tbody id="ponto_unilevel_id">
                            </tbody>
                        </table>
                    </div>   
                            <input type="hidden" id="id" name="id">
                </form>

      </div> <!-- Fim de ModaL Body-->


      <div class="modal-footer">
        <button type="button" class="btn btn-action btn-success add" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> &nbsp Aguarde...">Cadastrar
          <i class="fa fa-floppy-o"> </i>
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <i class='fa fa-times'></i>
        </button>
      </div> <!-- Fim de ModaL Footer-->

    </div> <!-- Fim de ModaL Content-->

  </div> <!-- Fim de ModaL Dialog-->

</div> <!-- Fim de ModaL Usuario-->