@extends('adminlte::page')

<script src ="{{ asset('/plugins/jQuery/jQuery-3.1.0.min.js') }}" type = "text/javascript" ></script>
@section('htmlheader_title')
	Gerenciar Categoria
@endsection

@section('main-content')


<div class="container-fluid spark-screen">
		<div class="row">
			<?php $mobile = FALSE;
    $teste = FALSE;

    $user_agents = array("iPhone","iPad","Android","webOS","BlackBerry","iPod","Symbian","IsGeneric");

    foreach($user_agents as $user_agent){

        if (strpos($_SERVER['HTTP_USER_AGENT'], $user_agent) !== FALSE) {
            $mobile = TRUE;

            break;
        }
    }
?>

<?php if ($mobile){ ?>

			<div class="col-md-12" style="margin-top: 80px;">
<?php }else{  ?>
	<div class="col-md-12">
	<?php } ?>
				<div class="box box-success">            				                    
                    <div class="box-body text-center table-responsive">
	                    <table class="table table-striped">
							<thead>
		                    	<tr>
		                    		<th>Data</th>
		                    		<th>Descrição</th>
		                    		<th>Origem</th>
		                    		<th>Destino</th>
		                    		<th>Valor</th>
		                    	</tr>
		                    </thead>
		                    	@foreach($transacoes as $transacao)	
		                    	<tr>
		                    		<td>{{ $transacao->created_at->format('d/m/Y') }}</td>
		                    		<td>{{ $transacao->nome }}</td>
		                    		<td>{{ $transacao->origem }}</td>
		                    		<td>{{ $transacao->destino }}</td>
		                    		<?php if($transacao->acao == 1){ ?>
		                    		<td style="color: rgb(0, 128, 0);"> R$ {{ number_format($transacao->valor, 2, ',', '.')    }}</td>
		                    		<?php }else{?>
		                    		<td style="color: red;">-R$ {{ number_format($transacao->valor, 2, ',', '.')}}</td>
		                    		<?php } ?>
		                    	</tr>
		                    	@endforeach
		                    	<tr>
		                    		<th></th>
		                    		<th></th>
		                    		
		                    	</tr>
	                    </table>
					</div>
                    </div>
                    <div class="box box-success">            				                    
                    	<div class="box-body text-center">
                    		<div class="panel-body">
					<div class="col-md-2"></div>
					<div class="col-md-3">
						<span style="background:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<label>Creditado</label>
					</div>
					<div class="col-md-3">
						<span style="background:orange;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<label>Bloqueado</label>
					</div>
					<div class="col-md-3">
						<span style="background:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;<label>Debitado</label>
					</div>
				</div>
                    	</div>
                	</div>
               </div>
            </div>
</div>

@endsection