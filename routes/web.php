<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::get('/registrar/{user?}', ['as' => 'registrar', 'uses' => 'HomeController@registrar']);
Route::post('/logar', ['as' => 'logar', 'uses' => 'HomeController@logar']);
Route::get('/entrar', ['as' => 'entrar', 'uses' => 'HomeController@login']);

Route::get('/usuarios-pagar_usuario', function () {
    return view('usuarios.pagar_usuario');
});

Auth::routes();

Route::post('/carrinho', ['as' => 'carrinho', 'uses' => 'ProdutoController@carrinho']);
Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'HomeController@dashboard']);
Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('/filme', ['as' => 'filmes.index', 'uses' => 'FilmeController@index']);
Route::get('/categoria', ['as' => 'categoria.create', 'uses' => 'CategoriaController@create']);
Route::get('/captura/{id}', ['as' => 'captura', 'uses' => 'CapturaController@capturaImage']);
Route::post('/cadastrar-capturado', ['as' => 'cadastrar-capturado', 'uses' => 'CapturaController@cadastrarCapturado']);


Route::group(['prefix' => '/fidelidade', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'fidelidade.index', 'uses' => 'LojaFidelidadeController@index']);
        
        Route::get('detalhe-produto/{id}', ['as' => 'fidelidade.detalhe', 'uses' => 'LojaFidelidadeController@detalheProduto']); 

        Route::get('fidelidade-produtos-categoria/{id}', ['as' => 'fidelidade-produtos-categoria', 'uses' => 'LojaFidelidadeController@produtosCategoria']);
        
        Route::get('/registrar', ['as' => 'register.clube', 'uses' => 'LojaFidelidadeController@cadastroUser']);

        Route::get('/fidelidade-todos-produtos', ['as' => 'fidelidade-todos-produtos', 'uses' => 'LojaFidelidadeController@todosProdutos']);

        Route::post('/buscar-produto', ['as' => 'buscar.produto', 'uses' => 'LojaFidelidadeController@buscarProduto']);

        Route::get('/carrinho', ['as' => 'fidelidade.carrinho', 'uses' => 'LojaFidelidadeController@getCarrinho']);
        Route::get('/delete/item/carrinho/{id}', ['as' => 'fidelidade.delete.item.carrinho', 'uses' => 'LojaFidelidadeController@deleteItemCarrinho']);

        Route::post('/comprar', ['as' => 'comprar', 'uses' => 'LojaFidelidadeController@finalizarCompra']);
        Route::post('/gerarpedido', ['as' => 'gerarpedido', 'uses' => 'LojaFidelidadeController@GerarPedido']);
        

        Route::post('/carrinho', ['as' => 'LojaFidelidade-LojaFidelidades.carrinho', 'uses' => 'LojaFidelidadeController@carrinho']); 

        Route::post('/cadastrar', ['as' => 'LojaFidelidade-LojaFidelidades.carrinho', 'uses' => 'LojaFidelidadeController@storeUser']);
});

Route::group(['prefix' => '/clube', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'incclube.index', 'uses' => 'IncclubeController@index']);
        
        Route::get('detalhe-produto/{id}', ['as' => 'incclube.detalhe', 'uses' => 'IncclubeController@detalheProduto']); 

        Route::get('produtos-categoria/{id}', ['as' => 'produtos-categoria', 'uses' => 'IncclubeController@produtosCategoria']);
        
        Route::get('/registrar', ['as' => 'register.clube', 'uses' => 'IncclubeController@cadastroUser']);

        Route::get('/todos-produtos', ['as' => 'todos-produtos', 'uses' => 'IncclubeController@todosProdutos']);

        Route::get('/todos-produtos', ['as' => 'todos-produtos', 'uses' => 'IncclubeController@todosProdutos']);

        Route::post('/buscar-produto', ['as' => 'buscar.produto', 'uses' => 'IncclubeController@buscarProduto']);

        Route::get('/carrinho', ['as' => 'carrinho', 'uses' => 'IncclubeController@getCarrinho']);
        Route::get('/comprar', ['as' => 'comprar', 'uses' => 'ProdutoController@buscarProduto']);

        Route::post('/comprar', ['as' => 'comprar', 'uses' => 'IncclubeController@finalizarCompra']);
        Route::post('/gerarpedido', ['as' => 'gerarpedido', 'uses' => 'IncclubeController@GerarPedido']);
        
        Route::get('/delete/item/carrinho/{id}', ['as' => 'clube.delete.item.carrinho', 'uses' => 'IncclubeController@deleteItemCarrinho']);

        Route::post('/carrinho', ['as' => 'incclube-incclubes.carrinho', 'uses' => 'IncclubeController@carrinho']); 

        Route::post('/cadastrar', ['as' => 'incclube-incclubes.carrinho', 'uses' => 'IncclubeController@storeUser']);
});

Route::group(['prefix' => 'gerenciar-produtos', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('/unilevel/{id}',['as' => 'gerenciar-pacotes.unilevel', 'uses' => 'ProdutoController@unilevel']);
        Route::get('/fidelidade/{id}',['as' => 'gerenciar-pacotes.fidelidade', 'uses' => 'ProdutoController@fidelidade']);

        Route::get('', ['as' => 'gerenciar-produtos.index', 'uses' => 'ProdutoController@index']);
        Route::get('/list',['as' => 'gerenciar-produtos.list', 'uses' => 'ProdutoController@list']);
        Route::get('/plataforma',['as' => 'gerenciar-produtos.plataforma', 'uses' => 'ProdutoController@plataforma']);
        Route::post('/store', ['as' => 'gerenciar-produtos.store', 'uses' => 'ProdutoController@store']);
        Route::post('/update', ['as' => 'gerenciar-produtos.update', 'uses' => 'ProdutoController@update']);
        Route::post('/delete', ['as' => 'gerenciar-produtos.destroy', 'uses' => 'ProdutoController@destroy']);
        Route::get('/gerar-extrato', ['as' => 'gerenciar-produtos.gerar-extrato', 'uses' => 'ProdutoController@gerar_extrato']);
        Route::post('/get-produto', ['as' => 'get-produto', 'uses' => 'ProdutoController@getProduto']);

        Route::get('/buscar-produto', ['as' => 'gerenciar-buscar-produto.index', 'uses' => 'ProdutoController@buscarProduto']);

        Route::get('/vendidosprodutos', ['as' => 'vendidosprodutos', 'uses' => 'ProdutoController@getVendidos']);

        Route::get('/vendidos', ['as' => 'gerenciar-produtos.vendidos', 'uses' => 'ProdutoController@vendidos']);

        Route::get('/disponivelprodutos', ['as' => 'disponivelprodutos', 'uses' => 'ProdutoController@getDisponivel']);

        Route::get('/disponivel', ['as' => 'gerenciar-produtos.disponivel', 'uses' => 'ProdutoController@disponivel']);
});

Route::group(['prefix' => 'gerenciar-fidelidade-produtos', 'where' => ['id' => '[0-9]+'] ] ,function() {

        Route::get('', ['as' => 'gerenciar-fidelidade-produtos.index', 'uses' => 'FidelidadeProdutoController@index']);
        Route::get('/list',['as' => 'gerenciar-fidelidade-produtos.list', 'uses' => 'FidelidadeProdutoController@list']);
        Route::post('/store', ['as' => 'gerenciar-fidelidade-produtos.store', 'uses' => 'FidelidadeProdutoController@store']);
        Route::post('/update', ['as' => 'gerenciar-fidelidade-produtos.update', 'uses' => 'FidelidadeProdutoController@update']);
        Route::post('/delete', ['as' => 'gerenciar-fidelidade-produtos.destroy', 'uses' => 'FidelidadeProdutoController@destroy']);

        Route::post('/get-produto', ['as' => 'get-produto', 'uses' => 'FidelidadeProdutoController@getProduto']);

        Route::get('/buscar-produto', ['as' => 'gerenciar-buscar-produto.index', 'uses' => 'FidelidadeProdutoController@buscarProduto']);

        Route::get('/vendidosprodutos', ['as' => 'vendidosprodutos', 'uses' => 'FidelidadeProdutoController@getVendidos']);

        Route::get('/vendidos', ['as' => 'gerenciar-produtos.vendidos', 'uses' => 'FidelidadeProdutoController@vendidos']);

        Route::get('/disponivelprodutos', ['as' => 'disponivelprodutos', 'uses' => 'FidelidadeProdutoController@getDisponivel']);

        Route::get('/disponivel', ['as' => 'gerenciar-produtos.disponivel', 'uses' => 'FidelidadeProdutoController@disponivel']);
});

Route::post('/gerenciar-usuarios/nome-user', ['as' => 'gerenciar-usuarios.nome-user', 'uses' => 'UserController@nomeUser']); 

Route::post('/gerenciar-usuarios/verifica-user', ['as' => 'verificaUserName', 'uses' => 'UserController@verificaUserName']); 

Route::post('/gerenciar-usuarios/verifica-email', ['as' => 'verifica-email', 'uses' => 'UserController@verificaEmail']); 

Route::group(['prefix' => 'gerenciar-usuarios', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('/usuarios/{tipo}', ['as' => 'gerenciar-usuarios.index', 'uses' => 'UserController@index']);

        Route::get('/buscar-usuario', ['as' => 'gerenciar-buscar-usuario.index', 'uses' => 'UserController@buscarUsuario']);
        
        Route::get('/login-conta-user/{id}', ['as' => 'conta-user', 'uses' => 'HomeController@loginContaUser']);  

        Route::get('/saque', ['as' => 'gerenciar-usuarios.saque', 'uses' => 'FinanceiroController@saque']); 
        Route::post('/criar-saque', ['as' => 'gerenciar-usuarios.criar-saque', 'uses' => 'FinanceiroController@criarSaque']); 


        Route::post('/buscar/user/compra', ['as' => 'buscar-user-compra', 'uses' => 'FinanceiroController@buscarUserCompra']); 


        Route::get('/redes/{id?}', ['as' => 'redes', 'uses' => 'HomeController@redes']);
        Route::get('/extrato', ['as' => 'extrato', 'uses' => 'UserController@extrato']);
        
        Route::get('/rede-patrocinador-voltar/{patrocinador}', ['as' => 'rede-patrocinador-voltar', 'uses' => 'UserController@redePatrocinadorVoltar']);
        
        Route::get('/rede-patrocinador/{patrocinador}', ['as' => 'rede-patrocinador', 'uses' => 'UserController@redePatrocinador']);
        
        Route::get('/bancario', ['as' => 'bancario', 'uses' => 'UserController@bancario']);
        Route::post('/banco', ['as' => 'banco', 'uses' => 'UserController@updateBanco']);
        Route::get('/updateUsuario', ['as' => 'update.usuario', 'uses' => 'UserController@updateUsuario']);
        Route::post('/updatePostUsuario', ['as' => 'updatePostUsuario', 'uses' => 'UserController@updatePostUsuario']);

        Route::get('/rede-indicacao', ['as' => 'rede.indicacao', 'uses' => 'UserController@redeIndicacao']);
        Route::get('/transferencia', ['as' => 'transferencia', 'uses' => 'UserController@transferencia']);
        Route::post('/transferencia', ['as' => 'transferencia.update', 'uses' => 'UserController@criarTransferencia']);
        Route::post('/lado-rede', ['as' => 'lado-rede', 'uses' => 'UserController@ladoRede']);

        Route::get('/avatar', ['as' => 'avatar', 'uses' => 'UserController@avatar']);
        Route::post('/avatar', ['as' => 'avatar.update', 'uses' => 'UserController@updateAvatar']);

        Route::get('/imagem-padrao', ['as' => 'imagem-padrao', 'uses' => 'UserController@imagemPadrao']);
        Route::post('/imagem-padrao', ['as' => 'imagem-padrao.update', 'uses' => 'UserController@updateImagemPadrao']);

        Route::get('/senha', ['as' => 'senha', 'uses' => 'UserController@senha']);
        Route::post('/senha', ['as' => 'update.senha', 'uses' => 'UserController@updateSenha']);

        Route::get('/cadastro-usuario', ['as' => 'gerenciar-cadastro.cadastro', 'uses' => 'UserController@cadastroUsuario']);
       
        Route::get('/pagar-usuario', ['as' => 'pagar-usuario', 'uses' => 'UserController@index']);
        
        Route::post('/get-usuario', ['as' => 'get-usuario', 'uses' => 'UserController@getUsuario']);
        
        Route::get('/list/{tipo?}',['as' => 'gerenciar-usuarios.list', 'uses' => 'UserController@list']);
        Route::post('/store', ['as' => 'gerenciar-usuarios.store', 'uses' => 'UserController@store']);
        Route::post('/update', ['as' => 'gerenciar-usuarios.update', 'uses' => 'UserController@update']);
        Route::post('/delete', ['as' => 'gerenciar-usuarios.destroy', 'uses' => 'UserController@destroy']);
        
        Route::post('/pagar', ['as' => 'gerenciar-usuarios.pagar', 'uses' => 'UserController@pagar']);
});

Route::group(['prefix' => 'gerenciar-transacoes', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-transacoes.index', 'uses' => 'TransacaoController@index']);
        Route::get('/list',['as' => 'gerenciar-transacoes.list', 'uses' => 'TransacaoController@list']);
        Route::post('/store', ['as' => 'gerenciar-transacoes.store', 'uses' => 'TransacaoController@store']);
        Route::post('/update', ['as' => 'gerenciar-transacoes.update', 'uses' => 'TransacaoController@update']);
        Route::post('/delete', ['as' => 'gerenciar-transacoes.destroy', 'uses' => 'TransacaoController@destroy']);
});

Route::group(['prefix' => 'gerenciar-financeiro', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('/user-venda-pacote', ['as' => 'user-venda-pacote', 'uses' => 'LojaFidelidadeController@vendaPacote']);
        Route::post('/user-extrato', ['as' => 'user-extrato', 'uses' => 'LojaFidelidadeController@userExtrato']);
        Route::post('/user-extrato-pacote', ['as' => 'user-extrato-pacote', 'uses' => 'LojaFidelidadeController@userExtratoPacote']);
       
        Route::post('/extrato-pacote', ['as' => 'extrato-pacote', 'uses' => 'LojaFidelidadeController@vendaPacoteExtratoData']);

        Route::get('/', ['as' => 'gerenciar-financeiro.index', 'uses' => 'FinanceiroController@index']);
        Route::post('/modificar-saque', ['as' => 'gerenciar-financeiro.modificar.saque', 'uses' => 'FinanceiroController@modificarSaque']);
        
        Route::get('/saques-solicitados', ['as' => 'gerenciar-financeiro.saques.solicitados', 'uses' => 'FinanceiroController@saquesSolicitados']);
        
        Route::get('/saques-solicitados-exibir', ['as' => 'gerenciar-financeiro.saques.solicitados.exibir', 'uses' => 'FinanceiroController@saquesSolicitadosExibir']);

        Route::get('/listar-saques', ['as' => 'gerenciar-financeiro.listar.saques', 'uses' => 'FinanceiroController@listarSaques']);

        Route::get('/credito', ['as' => 'gerenciar-financeiro.credito', 'uses' => 'FinanceiroController@credito']);
        Route::post('/credito', ['as' => 'gerenciar-financeiro.credito', 'uses' => 'FinanceiroController@adicionarCredito']); 

        Route::get('/senha', ['as' => 'gerenciar-financeiro.senha', 'uses' => 'FinanceiroController@senha']);

        Route::post('/solicitar-senha', ['as' => 'gerenciar-financeiro.solicitar.senha', 'uses' => 'FinanceiroController@solicitarSenha']); 

        Route::get('/debito', ['as' => 'gerenciar-financeiro.debito', 'uses' => 'FinanceiroController@debito']);

        Route::post('/debito', ['as' => 'gerenciar-financeiro.debito', 'uses' => 'FinanceiroController@adicionarDebito']);

        Route::get('/list', ['as' => 'gerenciar-financeiro.list', 'uses' => 'FinanceiroController@list']);
        
        Route::post('/extrato', ['as' => 'gerenciar-financeiro.extrato', 'uses' => 'FinanceiroController@extrato']);
        
        Route::get('/extrato-geral', ['as' => 'gerenciar-financeiro.get.extrato.geral', 'uses' => 'FinanceiroController@getExtratoGeral']);

        Route::post('/extrato-geral', ['as' => 'gerenciar-financeiro.extrato.geral', 'uses' => 'FinanceiroController@extratoGeral']);
        
        Route::get('/pedidos/{nome_usuario?}', ['as' => 'gerenciar-financeiro.pedidos', 'uses' => 'FinanceiroController@gerenciar_pedidos']);

        Route::post('/pedidos', ['as' => 'pedidos.post', 'uses' => 'FinanceiroController@gerenciar_pedidos_post']);

        Route::get('/list_pedidos/{nome?}', ['as' => 'gerenciar-financeiro.list_pedidos', 'uses' => 'FinanceiroController@getPedidos']);
        Route::get('/getPedido/{codigo_pedido}', ['as' => 'gerenciar-financeiro.getPedido', 'uses' => 'FinanceiroController@getPedido']);
        Route::post('/finalizarPedido', ['as' => 'gerenciar-financeiro.finalizarPedido', 'uses' => 'FinanceiroController@finalizarPedido']);
        Route::post('/finalizarCompra', ['as' => 'gerenciar-financeiro.finalizarCompra', 'uses' => 'FinanceiroController@finalizarCompra']);
        
});

Route::group(['prefix' => 'relatorios', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('/transacoes', ['as' => 'relatorios.transacoes', 'uses' => 'RelatorioController@transacoes']);
        Route::get('/list',['as' => 'relatorios.list', 'uses' => 'TransacaoController@list']);
        Route::post('/store', ['as' => 'relatorios.store', 'uses' => 'TransacaoController@store']);
        Route::post('/update', ['as' => 'relatorios.update', 'uses' => 'TransacaoController@update']);
        Route::post('/delete', ['as' => 'relatorios.destroy', 'uses' => 'TransacaoController@destroy']);
});

Route::group(['prefix' => 'maximo', 'where' => ['id' => '[0-9]+'] ] ,function() {
       Route::get('', ['as' => 'maximo.index', 'uses' => 'MaximoController@index']);
        Route::get('/list',['as' => 'maximo.list', 'uses' => 'MaximoController@list']);
        Route::post('/store', ['as' => 'maximo.store', 'uses' => 'MaximoController@store']);
        Route::post('/update', ['as' => 'maximo.update', 'uses' => 'MaximoController@update']);
        Route::post('/delete', ['as' => 'maximo.destroy', 'uses' => 'MaximoController@destroy']);
});

Route::group(['prefix' => 'gerenciar-categorias', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-categorias.index', 'uses' => 'CategoriaController@index']);
        Route::get('/list',['as' => 'gerenciar-categorias.list', 'uses' => 'CategoriaController@list']);
        Route::post('/store', ['as' => 'gerenciar-categorias.store', 'uses' => 'CategoriaController@store']);
        Route::post('/update', ['as' => 'gerenciar-categorias.update', 'uses' => 'CategoriaController@update']);
        Route::post('/delete', ['as' => 'gerenciar-categorias.destroy', 'uses' => 'CategoriaController@destroy']);
});

Route::group(['prefix' => 'gerenciar-fidelidade-categorias', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-fidelidade-categorias.index', 'uses' => 'FidelidadeCategoriaController@index']);
        Route::get('/list',['as' => 'gerenciar-fidelidade-categorias.list', 'uses' => 'FidelidadeCategoriaController@list']);
        Route::post('/store', ['as' => 'gerenciar-fidelidade-categorias.store', 'uses' => 'FidelidadeCategoriaController@store']);
        Route::post('/update', ['as' => 'gerenciar-fidelidadecategorias.update', 'uses' => 'FidelidadeCategoriaController@update']);
        Route::post('/delete', ['as' => 'gerenciar-fidelidadecategorias.destroy', 'uses' => 'FidelidadeCategoriaController@destroy']);
});


Route::group(['prefix' => 'gerenciar-infinite', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-infinite.index', 'uses' => 'InfiniteController@index']);
        Route::get('/list',['as' => 'gerenciar-infinite.list', 'uses' => 'InfiniteController@list']);
        Route::post('/store', ['as' => 'gerenciar-infinite.store', 'uses' => 'InfiniteController@store']);
        Route::post('/update', ['as' => 'gerenciar-infinite.update', 'uses' => 'InfiniteController@update']);
        Route::post('/delete', ['as' => 'gerenciar-infinite.destroy', 'uses' => 'InfiniteController@destroy']);
});

Route::group(['prefix' => 'gerenciar-banners', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-banners.index', 'uses' => 'BannerController@index']);
        Route::get('/list',['as' => 'gerenciar-banners.list', 'uses' => 'BannerController@list']);
        Route::post('/store', ['as' => 'gerenciar-banners.store', 'uses' => 'BannerController@store']);
        Route::post('/update', ['as' => 'gerenciar-banners.update', 'uses' => 'BannerController@update']);
        Route::post('/delete', ['as' => 'gerenciar-banners.destroy', 'uses' => 'BannerController@destroy']);
});

Route::group(['prefix' => 'gerenciar-fidelidade-banners', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-fidelidade-banners.index', 'uses' => 'FidelidadeBannerController@index']);
        Route::get('/list',['as' => 'gerenciar-fidelidade-banners.list', 'uses' => 'FidelidadeBannerController@list']);
        Route::post('/store', ['as' => 'gerenciar-fidelidade-banners.store', 'uses' => 'FidelidadeBannerController@store']);
        Route::post('/update', ['as' => 'gerenciar-fidelidade-banners.update', 'uses' => 'FidelidadeBannerController@update']);
        Route::post('/delete', ['as' => 'gerenciar-fidelidade-banners.destroy', 'uses' => 'FidelidadeBannerController@destroy']);
});

Route::group(['prefix' => 'gerenciar-material', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-material.index', 'uses' => 'MaterialController@index']);
        Route::get('/list',['as' => 'gerenciar-material.list', 'uses' => 'MaterialController@list']);
        Route::post('/store', ['as' => 'gerenciar-material.store', 'uses' => 'MaterialController@store']);
        Route::post('/update', ['as' => 'gerenciar-material.update', 'uses' => 'MaterialController@update']);
        Route::post('/delete', ['as' => 'gerenciar-material.destroy', 'uses' => 'MaterialController@destroy']);
});

Route::group(['prefix' => 'gerenciar-mensagens', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-mensagens.index', 'uses' => 'MensagemController@index']);
        Route::get('/list',['as' => 'gerenciar-mensagens.list', 'uses' => 'MensagemController@list']);
        Route::post('/store', ['as' => 'gerenciar-mensagens.store', 'uses' => 'MensagemController@store']);
        Route::post('/update', ['as' => 'gerenciar-mensagens.update', 'uses' => 'MensagemController@update']);
        Route::post('/delete', ['as' => 'gerenciar-mensagens.destroy', 'uses' => 'MensagemController@destroy']);
});

Route::group(['prefix' => 'gerenciar-slides', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-slides.index', 'uses' => 'SlideController@index']);
        Route::get('/list',['as' => 'gerenciar-slides.list', 'uses' => 'SlideController@list']);
        Route::post('/store', ['as' => 'gerenciar-slides.store', 'uses' => 'SlideController@store']);
        Route::post('/update', ['as' => 'gerenciar-slides.update', 'uses' => 'SlideController@update']);
        Route::post('/delete', ['as' => 'gerenciar-slides.destroy', 'uses' => 'SlideController@destroy']);
});

Route::group(['prefix' => 'gerenciar-pacotes', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-pacotes.index', 'uses' => 'PacoteController@index']);
        Route::get('/list',['as' => 'gerenciar-pacotes.list', 'uses' => 'PacoteController@list']);
        Route::get('/nivel/{id}',['as' => 'gerenciar-pacotes.nivel', 'uses' => 'PacoteController@nivel']);
        Route::get('/ponto/{id}',['as' => 'gerenciar-pacotes.ponto', 'uses' => 'PacoteController@ponto']);
        Route::post('/store', ['as' => 'gerenciar-pacotes.store', 'uses' => 'PacoteController@store']);
        Route::post('/update', ['as' => 'gerenciar-pacotes.update', 'uses' => 'PacoteController@update']);
        Route::post('/delete', ['as' => 'gerenciar-pacotes.destroy', 'uses' => 'PacoteController@destroy']);
});

Route::group(['prefix' => 'gerenciar-capturas', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-capturas.index', 'uses' => 'CapturaController@index']);
        Route::get('/list',['as' => 'gerenciar-capturas.list', 'uses' => 'CapturaController@list']);
        Route::post('/store', ['as' => 'gerenciar-capturas.store', 'uses' => 'CapturaController@store']);
        Route::post('/update', ['as' => 'gerenciar-capturas.update', 'uses' => 'CapturaController@update']);
        Route::post('/delete', ['as' => 'gerenciar-capturas.destroy', 'uses' => 'CapturaController@destroy']);
});

Route::group(['prefix' => 'checkout', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'checkout.index', 'uses' => 'CheckoutController@index']);
        Route::post('/pagar', ['as' => 'pagar', 'uses' => 'CheckoutController@pagar']);
        Route::post('/update', ['as' => 'checkout.update', 'uses' => 'CheckoutController@update']);
        Route::post('/delete', ['as' => 'checkout.destroy', 'uses' => 'CheckoutController@destroy']);
        Route::get('/parcelas/{total}', ['as' => 'checkout.parcelas', 'uses' => 'CheckoutController@parcelas']);
});

Route::group(['prefix' => 'gerenciar-qualificacoes', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-qualificacoes.index', 'uses' => 'QualificacaoController@index']);
        Route::get('/list',['as' => 'gerenciar-qualificacoes.list', 'uses' => 'QualificacaoController@list']);
        Route::post('/store', ['as' => 'gerenciar-qualificacoes.store', 'uses' => 'QualificacaoController@store']);
        Route::post('/update', ['as' => 'gerenciar-qualificacoes.update', 'uses' => 'QualificacaoController@update']);
        Route::post('/delete', ['as' => 'gerenciar-qualificacoes.destroy', 'uses' => 'QualificacaoController@destroy']);
});
Route::group(['prefix' => 'gerenciar-impostos', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-impostos.index', 'uses' => 'ImpostoController@index']);
        Route::get('/list',['as' => 'gerenciar-impostos.list', 'uses' => 'ImpostoController@list']);
        Route::post('/store', ['as' => 'gerenciar-impostos.store', 'uses' => 'ImpostoController@store']);
        Route::post('/update', ['as' => 'gerenciar-impostos.update', 'uses' => 'ImpostoController@update']);
        Route::post('/delete', ['as' => 'gerenciar-impostos.destroy', 'uses' => 'ImpostoController@destroy']);
});
Route::group(['prefix' => 'gerenciar-fretes', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-fretes.index', 'uses' => 'FreteController@index']);
        Route::get('/list',['as' => 'gerenciar-fretes.list', 'uses' => 'FreteController@list']);
        Route::post('/store', ['as' => 'gerenciar-fretes.store', 'uses' => 'FreteController@store']);
        Route::post('/update', ['as' => 'gerenciar-fretes.update', 'uses' => 'FreteController@update']);
        Route::post('/delete', ['as' => 'gerenciar-fretes.destroy', 'uses' => 'FreteController@destroy']);
});

Route::group(['prefix' => 'gerenciar-parcelamentos', 'where' => ['id' => '[0-9]+'] ] ,function() {
        Route::get('', ['as' => 'gerenciar-parcelamentos.index', 'uses' => 'ParcelamentoController@index']);
        Route::get('/list',['as' => 'gerenciar-parcelamentos.list', 'uses' => 'ParcelamentoController@list']);
        Route::post('/store', ['as' => 'gerenciar-parcelamentos.store', 'uses' => 'ParcelamentoController@store']);
        Route::post('/update', ['as' => 'gerenciar-parcelamentos.update', 'uses' => 'ParcelamentoController@update']);
        Route::post('/delete', ['as' => 'gerenciar-parcelamentos.destroy', 'uses' => 'ParcelamentoController@destroy']);
});

Route::get('configuracoes', ['as' => 'configuracoes.index', 'uses' => 'ConfiguracaoController@index']);
Route::post('configuracoes-geral/store', ['uses' => 'ConfiguracaoController@store']);
Route::get('bonus={titulo}', ['as' => 'bonus.index', 'uses' => 'BonusController@index']);
//Route::get('pedidos', ['as' => 'pedidos.index', 'uses' => 'PedidosController@index']);
Route::group(['prefix' => 'pedidos', 'where' => ['id' => '[0-9]+'] ] ,function() {
    Route::get('', ['as' => 'pedidos.index', 'uses' => 'PedidosController@index']);
    Route::get('/list',['as' => 'pedidos.list', 'uses' => 'PedidosController@list']);
        
});







