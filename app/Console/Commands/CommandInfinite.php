<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;


class CommandInfinite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:infinite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::all();

           foreach ($user as $usuario) {
            $menor = 0;
            $fk_qualificacao = '';

            $linha_um = User::where('linha_patrocinada',1)
                ->where('infinite','>',0)
                ->where('patrocinador', $usuario->user)
                ->first();

            $linha_dois = User::where('linha_patrocinada',2)
                ->where('infinite','>',0)
                ->where('patrocinador', $usuario->user)
                ->first();

            if($linha_um != null && $linha_dois != null){
                if($linha_dois->infinite  > 0  && $linha_um->infinite > 0 && $usuario->infinite == 1){
                    $usuario->infinite = $usuario->infinite + 1;
                    $usuario->save();
                }

                if($linha_dois->infinite  > 1  && $linha_um->infinite > 1 && $usuario->infinite == 2){
                    $usuario->infinite = $usuario->infinite + 1;
                    $usuario->save();
                }

                if($linha_dois->infinite  > 2  && $linha_um->infinite > 2 && $usuario->infinite == 3){
                    $usuario->infinite = $usuario->infinite + 1;
                    $usuario->save();
                }

                if($linha_dois->infinite  > 3  && $linha_um->infinite > 3 && $usuario->infinite == 4){
                    $usuario->infinite = $usuario->infinite + 1;
                    $usuario->save();
                }
            }
        }
    }
}
