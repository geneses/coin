<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Qualificacao;
use App\User;

class CommandQualificacao extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sl:commandqualificacao';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $qualificacao = Qualificacao::orderBy('pontuacao','ASC')->get();

        $user = User::all();

        foreach ($user as $usuario) {
            $menor = 0;
            $fk_qualificacao = '';

            if($usuario->total_ponto_es < $usuario->total_ponto_di){
                $menor = $usuario->total_ponto_es;
            }else{
                $menor = $usuario->total_ponto_di;
            }

            foreach ($qualificacao as $value) {
                if($menor >= $value->pontuacao){
                    $fk_qualificacao = $value->id;
                }
            }

            if($fk_qualificacao != ''){
                $usuario->fk_qualificacao = $fk_qualificacao;
                $usuario->save();
            }else{
                $usuario->fk_qualificacao = $qualificacao[0]->id;
                $usuario->save();
            }
        }
    }
}
