<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Pacote;
use App\User;
use App\Qualificacao;
use App\Transacao;
use App\TransacaoTipo;
use App\SaldoUser;


class CommandBonusBinario extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sl:commandbonusbinario';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function buscaFilhos($patrocinador){
        $um = User::where('patrocinador', $patrocinador)
        ->where('derramamento',1)
        ->first();

        $zero = User::where('patrocinador', $patrocinador)
        ->where('derramamento',0)
        ->first();

        if($um != null && $zero != null ){
            return true;
        }else{
            return false;
        }
    }


    public function handle()
    {
        $users = User::JOIN('pacotes','pacotes.id','=','users.fk_pacote')
            ->join('qualificacaos','qualificacaos.id','=','users.fk_qualificacao')
            ->select('users.id','users.ponto_esquerda','users.ponto_direita','pacotes.comissao_binario','qualificacaos.maximo_binario','users.user')
            ->where('users.ponto_esquerda','>',0)
            ->where('users.ponto_direita','>',0)
            ->get();

            foreach ($users as $usuario) {

                $lado =  $this->buscaFilhos($usuario->user);

                if($lado){

                    if($usuario->ponto_esquerda < $usuario->ponto_direita){
                        $menor = $usuario->ponto_esquerda;
                        $usuario->ponto_esquerda = 0;
                        $usuario->ponto_direita  = $usuario->ponto_direita - $menor;
                    }

                    if($usuario->ponto_direita < $usuario->ponto_esquerda){
                        $menor = $usuario->ponto_direita;
                        $usuario->ponto_direita = 0;  
                        $usuario->ponto_esquerda  = $usuario->ponto_esquerda - $menor;
                        
                    }  

                    if($usuario->ponto_esquerda > 0 && $usuario->ponto_direita){
                       if($usuario->ponto_esquerda == $usuario->ponto_direita){
                            $menor = $usuario->ponto_direita;
                            $usuario->ponto_esquerda  = 0;
                            $usuario->ponto_direita = 0;    
                        }     
                    }
                   
                    $soma = ($menor * $usuario->comissao_binario) / 100;

                    if($soma > $usuario->maximo_binario){
                        $soma = $usuario->maximo_binario;
                    }

                    $saldo =  SaldoUser::where('fk_user',$usuario->id)->first();

                    if(!$saldo){
                        $saldo = new SaldoUser();
                        $saldo->fk_user = $usuario->id;
                    }

                    $saldo->saldo = $saldo->saldo + $soma;
                    $saldo->save();    
                    $usuario->save();

                    
                    $transacao = TransacaoTipo::where('nome','Bônus binário')->first();
                    
                    $transacao_user = new Transacao();
                    $transacao_user->valor = $soma;
                    $transacao_user->fk_transacao_tipo = $transacao->id;
                    $transacao_user->fk_membro = $usuario->id;
                    $transacao_user->fk_adesao_usuario = $usuario->id;
                    $transacao_user->save();
                }

            }
    }
}
