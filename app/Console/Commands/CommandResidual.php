<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\SaldoUser;
use App\Transacao;
use App\TransacaoTipo;

class CommandResidual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::JOIN('pacotes','pacotes.id','=','users.fk_pacote')
        ->select('users.id','pacotes.residual')
        ->where('users.ativo_mes','>', 0)
        ->where('fk_pacote','!=', null)
        ->get();

        foreach ($users as $usuario) {

            $saldo =  SaldoUser::where('fk_user',$usuario->id)->first();

                    if(!$saldo){
                        $saldo = new SaldoUser();
                        $saldo->fk_user = $usuario->id;
                    }

                    $saldo->saldo = $saldo->saldo + $usuario->residual;
                    $saldo->save();    
                    
                    $transacao = TransacaoTipo::where('nome','Bônus residual')->first();
                    
                    $transacao_user = new Transacao();
                    $transacao_user->valor = $usuario->residual;
                    $transacao_user->fk_transacao_tipo = $transacao->id;
                    $transacao_user->fk_membro = $usuario->id;
                    $transacao_user->fk_adesao_usuario = $usuario->id;
                    $transacao_user->save();

            
        }
    }
}
