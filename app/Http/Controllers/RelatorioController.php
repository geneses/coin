<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RelatorioController extends Controller
{
    public function transacoes(){
    	$transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
            ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
            ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
            ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.nome as origem', 'users.nome as destino')->get();

    	return view('relatorio.transacao',compact('transacoes'));
    }
}
