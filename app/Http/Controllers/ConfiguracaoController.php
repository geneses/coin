<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\configuracao;

class ConfiguracaoController extends Controller
{
    public function index(){
        $configuracao = configuracao::first();
            return view('configuracao.index', 
                        compact('configuracao'));
    }
    public function store(Request $request){  
            $configuracao = configuracao::first();
            if(!$configuracao){
                $configuracao = new configuracao();
            }
               //Geral
               $configuracao->nome_empresa = $request->nome_empresa;
               $configuracao->email_empresa = $request->email_empresa;

               if($request->logo_empresa){
                $logo_empresa = $request->file('logo_empresa')->store('public/imagens/configuracoes');
                $logo_empresa = str_replace('public','storage',$logo_empresa);
                $configuracao->logo_empresa = $logo_empresa;
               }
              

               //Site
               $configuracao->nome_site = $request->nome_site;
               $configuracao->email_site = $request->email_site;

               if($request->logo_site){
                $logo_site = $request->file('logo_site')->store('public/imagens/configuracoes');
                $logo_site = str_replace('public','storage',$logo_site);
                $configuracao->logo_site = $logo_site;
               }
               

               //escritorio
               $configuracao->nome_escritorio = $request->nome_escrito;
               $configuracao->email_escritorio = $request->email_escritorio;

               if($request->logo_escritorio){
                $logo_escritorio = $request->file('logo_escritorio')->store('public/imagens/configuracoes');
                $logo_escritorio = str_replace('public','storage',$logo_escritorio);
                $configuracao->logo_escritorio = $logo_escritorio;
               }

                //loja
                $configuracao->nome_loja = $request->nome_loja;
                $configuracao->email_loja = $request->email_loja;
              
                if($request->logo_loja){
                    $logo_loja = $request->file('logo_loja')->store('public/imagens/configuracoes');
                    $logo_loja = str_replace('public','storage',$logo_loja);
                    $configuracao->logo_loja = $logo_loja;
                }
               
                $configuracao->termos_uso = $request->termos_uso;
                $configuracao->save();
        
            return response()->json($configuracao);
        
    }
}
