<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\User;
use App\Qualificacao;
use App\SaldoUser;
use App\Transacao;
use App\TransacaoTipo;

class QualificacaoController extends Controller
{
    
    public function buscaFilhos($patrocinador){
        $count = User::where('fk_pacote','!=', null)
        ->where('patrocinador', $patrocinador)
        ->count();

      return $count;
    }

    public function index()
    {

        return view('qualificacao.index');
    }

    public function list()
    {
        $qualificacao = Qualificacao::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($qualificacao)
            ->editColumn('acoes', function ($qualificacao){
                return $this->setBtns($qualificacao);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(Qualificacao $Qualificacaos){
        $dados = "data-id_del='$Qualificacaos->id' 
        data-id='$Qualificacaos->id' 
        data-nome='$Qualificacaos->nome' 
        data-acao='$Qualificacaos->pontuacao' 
        data-premiacao='$Qualificacaos->premiacao' 
        data-pontuacao='$Qualificacaos->pontuacao' 
        data-maximo_binario='$Qualificacaos->maximo_binario' 
        data-bonus_infinite='$Qualificacaos->bonus_infinite' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'nome' => 'required',
            'pontuacao' => 'required',
            'premiacao' => 'required',
            'bonus_infinite' => 'required',
            'maximo_binario' => 'required',


            
        );
        $attributeNames = array(
            'nome' => 'Nome',
            'pontuacao' => 'Pontuação',
            'bonus_infinite' => 'Premiação',
            'premiacao' => 'Premiação',
            'maximo_binario' => 'maximo_binario'
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $qualificacao = new Qualificacao();
            $qualificacao->nome = $request->nome;
            $qualificacao->pontuacao = $request->pontuacao;
            $qualificacao->premiacao = $request->premiacao;
            $qualificacao->bonus_infinite = $request->bonus_infinite;
            $qualificacao->cor = $request->cor;
            $qualificacao->maximo_binario = $request->maximo_binario;
            $qualificacao->save();

            return response()->json($qualificacao);
        }
    }

    public function update(Request $request)
    {
        
        $qualificacao = Qualificacao::find($request->id);
        $qualificacao->nome = $request->nome;
        $qualificacao->pontuacao = $request->pontuacao;
        $qualificacao->premiacao = $request->premiacao;
        $qualificacao->bonus_infinite = $request->bonus_infinite;
        $qualificacao->cor = $request->cor;
        $qualificacao->maximo_binario = $request->maximo_binario;
        $qualificacao->save();
      
        return response()->json($qualificacao);
    }

    public function destroy(Request $request)
    {
        $qualificacao = qualificacao::destroy($request->id_del);

        return response()->json($qualificacao);
    }
}
