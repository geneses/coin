<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\Pacote;
use App\PacoteNivel;
use App\TransacaoTipo;
use App\PontoUnilevel;

class PacoteController extends Controller
{
     public function index()
    {
        $pacotes = Pacote::all();
        $transacoes = TransacaoTipo::orderBy("created_at","desc")->get();
        return view('pacote.index',compact('transacoes','pacotes'));
    }

 
    public function nivel($id)
    {
        $nivel = Pacote::join('pacote_nivels','pacote_nivels.fk_pacote','=','pacotes.id')
        ->where('pacote_nivels.fk_pacote',$id)
        ->get();
            
        return response()->json(['data'=> $nivel]);
    }

    public function ponto($id)
    {
        $nivel = Pacote::join('ponto_unilevels','ponto_unilevels.fk_pacote','=','pacotes.id')
        ->where('ponto_unilevels.fk_pacote',$id)
        ->get();
            
        return response()->json(['data'=> $nivel]);
    }

    public function list()
    {
        $produto = Pacote::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($produto)
            ->editColumn('acoes', function ($Pacote){
                return $this->setBtns($Pacote);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(Pacote $Pacotes){
        $dados = "data-id_del='$Pacotes->id' 
        data-id='$Pacotes->id' 
        data-nome='$Pacotes->nome' 
        data-v_compra_produto='$Pacotes->v_compra_produto' 
        data-comissao_unilevel='$Pacotes->comissao_unilevel' 
        data-bonus_binario='$Pacotes->bonus_binario' 
        data-comissao_binario='$Pacotes->comissao_binario' 
        data-valor_pacote='$Pacotes->valor_pacote' 
        data-residual='$Pacotes->residual'
        data-inicio_rapido='$Pacotes->inicio_rapido'
        data-ativo_mes='$Pacotes->ativo_mes'
        data-disponivel='$Pacotes->disponivel'
        data-inicio_rapido_valor='$Pacotes->inicio_rapido_valor'
        data-ponto_unilevel='$Pacotes->ponto_unilevel'
        data-fk_transacao_tipo='$Pacotes->fk_transacao_tipo'
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'nome' => 'required'      
        );
        $attributeNames = array(
            'nome' => 'Nome'
        );
//nome,bonus_binario, v_compra_produto, ponto_unilevel
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {
            $Pacote = new Pacote();
            $Pacote->nome = $request->nome;
            $Pacote->bonus_binario = $request->bonus_binario;
            $Pacote->v_compra_produto = $request->v_compra_produto;
            $Pacote->comissao_unilevel = $request->comissao_unilevel;
            $Pacote->comissao_binario = $request->comissao_binario;
            $Pacote->valor_pacote = $request->valor_pacote;
            $Pacote->ponto_unilevel = $request->ponto_unilevel_pacote;
            $Pacote->ativo_mes = $request->ativo_mes;
            $Pacote->inicio_rapido = $request->inicio_rapido;
            $Pacote->inicio_rapido_valor = $request->inicio_rapido_valor;
            $Pacote->residual = $request->residual;
            $Pacote->disponivel = $request->disponivel;
            $Pacote->fk_transacao_tipo = $request->fk_transacao_tipo;
            $Pacote->save();

            if(isset($request->ponto)){
                foreach ($request->ponto as $value) {
                    $PacoteNivel = new PacoteNivel();
                    $PacoteNivel->fk_pacote = $Pacote->id;
                    $PacoteNivel->valor = $value['valor'];
                    $PacoteNivel->nivel = $value['nivel'];
                    $PacoteNivel->save();
                }
            }

            if(isset($request->ponto_unilevel)){
	            foreach ($request->ponto_unilevel as $value) {
	            	$PacoteNivel = new PontoUnilevel();
		            $PacoteNivel->fk_pacote = $Pacote->id;
		            $PacoteNivel->valor = $value['valor'];
		            $PacoteNivel->nivel = $value['nivel'];
		            $PacoteNivel->save();
	            }
        	}

            return response()->json($Pacote);
        }
    }

    public function update(Request $request)
    {    
        $Pacote = Pacote::find($request->id);
        $Pacote->nome = $request->nome;
        $Pacote->bonus_binario = $request->bonus_binario;
        $Pacote->v_compra_produto = $request->v_compra_produto;
        $Pacote->comissao_unilevel = $request->comissao_unilevel;
        $Pacote->comissao_binario = $request->comissao_binario;
        $Pacote->valor_pacote = $request->valor_pacote;
        $Pacote->ativo_mes = $request->ativo_mes;
        $Pacote->inicio_rapido_valor = $request->inicio_rapido_valor;
        $Pacote->inicio_rapido = $request->inicio_rapido;
        $Pacote->disponivel = $request->disponivel;
        $Pacote->ponto_unilevel = $request->ponto_unilevel_pacote;
        $Pacote->fk_transacao_tipo = $request->fk_transacao_tipo;
        $Pacote->residual = $request->residual;

        $Pacote->save();

        if(isset($request->ponto)){
            $PacoteNivel = PacoteNivel::where('fk_pacote',$request->id)->delete();
            foreach ($request->ponto as $value) {
                $PacoteNivel = new PacoteNivel();
                $PacoteNivel->fk_pacote = $Pacote->id;
                $PacoteNivel->valor = $value['valor'];
                $PacoteNivel->nivel = $value['nivel'];
                $PacoteNivel->save();
            }
        }else{
            $PacoteNivel = PacoteNivel::where('fk_pacote',$request->id)->delete();
        }



      if(isset($request->ponto_unilevel)){
        //dd($request->ponto_unilevel);
         $ponto_unilevel = PontoUnilevel::where('fk_pacote',$request->id)->delete();
            foreach ($request->ponto_unilevel as $value) {    
                $ponto_unilevel = new PontoUnilevel();
                $ponto_unilevel->fk_pacote = $Pacote->id;
                $ponto_unilevel->valor = $value['valor'];
                $ponto_unilevel->nivel = $value['nivel'];
                $ponto_unilevel->save();
            }
        }else{
            $ponto_unilevel = PontoUnilevel::where('fk_pacote',$request->id)->delete();
        }
      
        return response()->json($Pacote);
    }

    public function destroy(Request $request)
    {
        $Pacote = Pacote::destroy($request->id_del);

        return response()->json($Pacote);
    }
}
