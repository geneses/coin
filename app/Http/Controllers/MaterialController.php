<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\Material;

class MaterialController extends Controller
{
    public function index()
    {
        return view('material_apoio.index');
    }

    public function list()
    {
        $material = Material::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($material)
            ->addColumn('material', function ($material){
                 $url = asset($material->material);
                 return '<a target="_blank"  href='.$url.'> Baixar </a>';
            })
            ->editColumn('acoes', function ($Material){
                return $this->setBtns($Material);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(Material $Materials){
        $dados = "data-id_del='$Materials->id' 
        data-id='$Materials->id' 
        data-nome='$Materials->nome' 
        data-material='$Materials->material' 
        ";

        $btnDeletar = '';

        if(Auth::user()->hasAnyRole(['empresa'])){
            $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver material' $dados> <i class='fa fa-eye'></i></a> ";

            $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar material' $dados><i class='fa fa-trash'></i></a>";
        }
        return $btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'nome' => 'required',
            'material' => 'required'          
        );
        $attributeNames = array(
            'nome' => 'nome',
            'material' => 'material'

        );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {
        	$material = $request->file('material')->store('public/imagens/Materials');
            $material = str_replace('public','storage',$material);

            $Material = new Material();
            $Material->nome = $request->nome;
            $Material->material = $material;
            $Material->save();

            return response()->json($Material);
        }
    }

    public function update(Request $request)
    {
        $material = $request->file('material')->store('public/materiais');
        $material = str_replace('public','storage',$material);

        $Material = Material::find($request->id);
        $Material->nome = $request->nome;
        $Material->material = $request->material;
        $Material->save();
      
        return response()->json($Material);
    }

    public function destroy(Request $request)
    {
        $Material = Material::destroy($request->id_del);

        return response()->json($Material);
    }
}
