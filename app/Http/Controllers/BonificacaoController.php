<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BonificacaoController extends Controller
{
    public function buscaFilhosLider($patrocinador){
        $count = User::where('fk_pacote','!=', null)
        ->where('patrocinador', $patrocinador)
        ->count();

      return $count;
    }

    public function liderCadastro(){
    	$array = array();

        $users = User::where('fk_pacote','!=', null)
            ->get();

            foreach ($users as $usuario) {

                $filhos =  $this->buscaFilhosLider($usuario->user);

                array_push($array,['pai:' => $usuario->user, 'Qtd filhos' => $filhos]);     
            }

            dd($array);
    }



}
