<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FidelidadeCategoria;
use App\ProdutoFidelidade;
use App\FidelidadeUserCompra;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;

class FidelidadeProdutoController extends Controller
{
	public function index()
    {
        $categorias = FidelidadeCategoria::orderBy('created_at')->get();
        return view('fidelidade_produto.index',compact('categorias'));
    }
    
    public function carrinho(Request $request)
    {
        $carrinho = New Carrinho();
        $carrinho->quantidade = $request->quantidade;
        $carrinho->fk_produto = $request->id;
        $carrinho->fk_user = Auth::user()->id;

        return response()->json($carrinho);
    }

    public function getProduto(Request $request)
    {
        $ProdutoFidelidade = ProdutoFidelidade::where('nome',$request->produto)->first();

        $view = view("produtos.pagar_produto",compact('ProdutoFidelidade'))->render();
        return $view;
    }
 
    public function list()
    {
        $produto = ProdutoFidelidade::orderBy('created_at', 'desc')
        ->where('fk_user',Auth::user()->id)
        ->get();
            
        return DataTables::of($produto)
            ->editColumn('acao', function ($produto){
                return $this->setBtns($produto);
            })
            ->addColumn('imagem', function ($produto){
                 $url = asset($produto->imagem);
                 return '<img src='.$url.' width="150px"  />';
            })
            ->escapeColumns([0])
            ->make(true);
    } 

    public function getVendidos()
    {
        $produto = FidelidadeUserCompra::join('produto_fidelidades','produto_fidelidades.id','=','fidelidade_user_compras.fk_produto')
        ->select('fidelidade_user_compras.quantidade','fidelidade_user_compras.created_at','fidelidade_user_compras.valor','fidelidade_user_compras.id','produto_fidelidades.imagem','produto_fidelidades.nome')
        ->orderBy('fidelidade_user_compras.created_at', 'desc')
        ->where('produto_fidelidades.fk_user',Auth::user()->id)
        ->get();
            
        return DataTables::of($produto)
            ->addColumn('imagem', function ($produto){
                 $url = asset($produto->imagem);
                 return '<img src='.$url.' width="150px"  />';
            })  
            ->editColumn('quantidade', function ($produto){
                return $this->qtdEstoque($produto);
            })->escapeColumns([0])
            ->make(true);
    }

    private function qtdEstoque(FidelidadeUserCompra $produto){
        $qtd = FidelidadeUserCompra::where('id', $produto->id)
                ->select('quantidade')
                ->sum('quantidade');
        return $qtd;
    }
    public function getDisponivel()
    {
        $produto = ProdutoFidelidade::orderBy('created_at', 'desc')
        ->where('estoque','>',0)
        ->get();
            
        return DataTables::of($produto)
            ->addColumn('imagem', function ($produto){
                 $url = asset($produto->imagem);
                 return '<img src='.$url.' width="150px"  />';
            })  
            ->escapeColumns([0])
            ->make(true);
    }

    

    public function disponivel()
    {
        return view('fidelidade_produto.disponivel');
    }

    public function vendidos()
    {
        return view('fidelidade_produto.vendidos');
    }
     
    private function setBtns(ProdutoFidelidade $produtos){
        $dados = "data-id_del='$produtos->id' 
        data-id='$produtos->id' 
        data-nome='$produtos->nome' 
        data-imagem='$produtos->imagem' 
        data-peso='$produtos->peso' 
        data-descricao='$produtos->descricao' 
        data-estoque='$produtos->estoque' 
        data-ativo='$produtos->ativo' 
        data-valor_compra='$produtos->valor_compra' 
        data-valor_ponto_fidelidade='$produtos->valor_ponto_fidelidade' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public static function moeda($get_valor) {

        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor);
        return $valor; 
    }

    public function store(Request $request)
    {  
         
        $rules = array(
            'nome' => 'required',
            'imagem' => 'required',
            'peso' => 'required',
            'descricao' => 'required',
            'estoque' => 'required',
            'valor_ponto_fidelidade' => 'required',
            'valor_compra' => 'required',
            
        );
        $attributeNames = array(
            'nome' => 'nome',
            'imagem' => 'imagem',
            'peso' => 'peso',
            'estoque' => 'qtdEstoque',
            'descricao' => 'descricao',
            'valor_ponto_fidelidade' => 'Valor fidelidade',
            'valor_compra' => 'Valor compra',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $imagem = $request->file('imagem')->store('public/imagens/produtos');
            $imagem = str_replace('public','storage',$imagem);
            $produto = new ProdutoFidelidade();
            $produto->nome = $request->nome;
            $produto->descricao = $request->descricao;
            $produto->peso = $request->peso;
            $produto->estoque = $request->estoque;
            $produto->imagem = $imagem;
            $produto->fk_user = Auth::user()->id;
            $produto->fk_categoria = $request->fk_categoria;;
            $produto->valor_compra = $this->moeda($request->valor_compra);
            $produto->valor_ponto_fidelidade = $this->moeda($request->valor_ponto_fidelidade);
            $produto->save();
        
            return response()->json($produto);
        }
    }

    public function update(Request $request)
    {
        
        $produto = ProdutoFidelidade::find($request->id);

        if($request->file('imagem')){
           $imagem = $request->file('imagem')->store('public/imagens/produtos');
           $imagem = str_replace('public','storage',$imagem); 
           $produto->imagem = $imagem;

        }
        
       	$produto->nome = $request->nome;
        $produto->descricao = $request->descricao;
        $produto->peso = $request->peso;
        $produto->estoque = $request->estoque;
        $produto->ativo = $request->ativo;
        $produto->valor_compra = $this->moeda($request->valor_compra);
    	$produto->valor_ponto_fidelidade = $this->moeda($request->valor_ponto_fidelidade);
        $produto->save();

        return response()->json($produto);
    }

    public function destroy(Request $request)
    {
        $produto = ProdutoFidelidade::destroy($request->id_del);

        return response()->json($produto);
    }
}
