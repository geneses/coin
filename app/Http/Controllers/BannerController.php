<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use Carbon;
use Vindi;
use App\Banner;
use App\User;
use App\SaldoUser;
use DatePeriod;
use DateInterval;
use DateTime;

class BannerController extends Controller
{

    public $valor = [
        '1-dia'   => 1.00,
        '1-mes'   => 29.90,
        '2-meses' => 39.90,
        '3-meses' => 49.90
    ];

    public function datas(){
        $data_atual = date("Y");
        $datas = new DatePeriod(new DateTime($data_atual), new DateInterval('P1Y'), 10);
        return $datas;
    }


    public function index()
    {
        /*

    // Declara em um array os valores de VINDI_API_KEY e VINDI_API_URI
    $arguments = array(
        'VINDI_API_KEY' => 'jVz1nd9v1ou4ZN9JmYSSkk5x35SZxCajq60miZ_TZ3Q',
        'VINDI_API_URI' => 'https://sandbox-app.vindi.com.br/api/v1/customers'
    );

    // $pagamento = array(
    //     'VINDI_API_KEY' => 'KQdncbJ3446dcikn83fp_Mi6zNK1KAKmrAOnmL97Pyo',
    //     'VINDI_API_URI' => 'https://app.vindi.com.br/api/v1/bills'
    // );

    // Instancia o serviço de Customers (Clientes) com o array contendo VINDI_API_KEY e VINDI_API_URI
    $customerService = new Vindi\Customer($arguments);
    //$bill = new Vindi\bill($pagamento);
    
    $id = 12;
    $customer = $customerService->all();//([

    foreach ($customer as  $value) {
        if($value->code == $id){
            $id_cutomer = $value->id;
        }
    }

    $datas = $this->datas();
    $valor = $this->valor;

    $saldo_user = SaldoUser::where('fk_user',Auth::user()->id)->first();

    return view('checkout.index', compact('valor','datas', 'saldo_user'));
    dd($id_cutomer);


    $pagamento = array(
        'VINDI_API_KEY' => 'jVz1nd9v1ou4ZN9JmYSSkk5x35SZxCajq60miZ_TZ3Q', //SANDBOX
        'VINDI_API_URI' => 'https://sandbox-app.vindi.com.br/api/v1/bills'
    );

    $bill = new Vindi\Bill($pagamento);


    $bill = $bill->create([
         'customer_id' => 9301482,
         'payment_method_code' => 'credit_card',
         'metadata' => 'metadata',
         'payment_profile' => [
             'holder_name' => 'MOISES R MASCARENHAS',
             'card_expiration' => '01/24',
             'card_number' => '5276600033067359',
             'card_cvv' => '434'
         ],
        'bill_items' => [[
             'product_id' => 17886,
             'amount' => 10
         ]],

     ]);






    

    dd($users);



       //echo $id_cutomer;

       // return view('banner.index');

       */


        return view('banner.index');
   
    }

    public function list()
    {
        $produto = Banner::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($produto)
            ->addColumn('imagem', function ($produto){
                 $url = $produto->imagem;
                 return '<img src='.$url.' width="150px"  />';
            })
            ->editColumn('acoes', function ($Banner){
                return $this->setBtns($Banner);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(Banner $Banners){
        $dados = "data-id_del='$Banners->id' 
        data-id='$Banners->id' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'imagem' => 'required'           
        );
        $attributeNames = array(
            'imagem' => 'imagem'
        );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {
        	$imagem = $request->file('imagem')->store('public/imagens/banners');
            $imagem = str_replace('public','storage',$imagem);

            $Banner = new Banner();
            $Banner->imagem = $imagem;
            $Banner->link = $request->link;
            $Banner->save();

            return response()->json($Banner);
        }
    }

    public function update(Request $request)
    {
        $Banner = Banner::find($request->id);
        if($request->file('imagem')){
        $imagem = $request->file('imagem')->store('public/imagens/banners');
            $imagem = str_replace('public','storage',$imagem);
            $Banner->imagem = $imagem;
        }
        $Banner->link = $request->link;
        $Banner->save();
      
        return response()->json($Banner);
    }

    public function destroy(Request $request)
    {
        $Banner = Banner::destroy($request->id_del);

        return response()->json($Banner);
    }
}