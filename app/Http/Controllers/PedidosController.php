<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\Carrinho;

class PedidosController extends Controller
{
    public function index()
    {
        return view('pedidos.index');
    }

    public function list()
    {
    	//Capturar Usuário Logado
        $usuario_logado = Auth::user();

        $carrinhos = Carrinho::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($carrinhos)
            ->editColumn('acoes', function ($Carrinho){
                return $this->setBtns($Carrinho);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(Carrinho $Carrinhos){
        $dados = "data-id='$Carrinhos->id' 
        data-fk_user='$Carrinhos->fk_user' 
        data-fk_produto='$Carrinhos->fk_produto' 
        data-quantidade='$Carrinhos->quantidade' 
        data-codigo_pedido='$Carrinhos->codigo_pedido'
        data-valor='$Carrinhos->valor'
        ";

        $btnDeletar = '';

        //if(Auth::user()->hasAnyRole(['empresa'])){
        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver Pedido' $dados> <i class='fa fa-eye'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Cancelar Pedido' $dados><i class='fa fa-trash'></i></a>";
        //}
        return $btnDeletar;
    }
}
