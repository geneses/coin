<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\SuporteMensagem;

class MensagemController extends Controller
{
     public function index()
    {
        return view('mensagem.index');
    }

 
    public function list()
    {
        $produto = SuporteMensagem::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($produto)
            ->editColumn('acoes', function ($SuporteMensagem){
                return $this->setBtns($SuporteMensagem);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(SuporteMensagem $SuporteMensagems){
        $dados = "data-id_del='$SuporteMensagems->id' 
        data-id='$SuporteMensagems->id' 
        data-titulo='$SuporteMensagems->titulo' 
        data-status='$SuporteMensagems->status' 
        data-mensagem='$SuporteMensagems->mensagem'
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'titulo' => 'required',
            'status' => 'required'        
        );
        $attributeNames = array(
            'titulo' => 'Título',
            'status' => 'Status'
        );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {
            $SuporteMensagem = new SuporteMensagem();
            $SuporteMensagem->titulo = $request->titulo;
            $SuporteMensagem->mensagem = $request->mensagem;
            $SuporteMensagem->status = $request->status;
            $SuporteMensagem->data_abertura = $request->data_abertura;
            $SuporteMensagem->data_alteracao = $request->data_alteracao;
            $SuporteMensagem->data_fechamento = $request->data_fechamento;
            $SuporteMensagem->fk_user = Auth::user()->id;
            $SuporteMensagem->save();

            return response()->json($SuporteMensagem);
        }
    }

    public function update(Request $request)
    {
        
        $SuporteMensagem = SuporteMensagem::find($request->id);
        $SuporteMensagem->titulo = $request->titulo;
        $SuporteMensagem->mensagem = $request->mensagem;
        $SuporteMensagem->status = $request->status;
        $SuporteMensagem->data_abertura = $request->data_abertura;
        $SuporteMensagem->data_alteracao = $request->data_alteracao;
        $SuporteMensagem->data_fechamento = $request->data_fechamento;
        $SuporteMensagem->save();
      
        return response()->json($SuporteMensagem);
    }

    public function destroy(Request $request)
    {
        $SuporteMensagem = SuporteMensagem::destroy($request->id_del);

        return response()->json($SuporteMensagem);
    }
}
