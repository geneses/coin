<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\User;
use App\Capturado;

class CapturaController extends Controller
{
    
    public function capturaImage($id)
    {
        $user = User::find($id);

        return view('captura.tela',compact('user'));
    }

    public function cadastrarCapturado(Request $request)
    {
        $capturado = new Capturado();

        $capturado->fk_user = $request->fk_user;
        $capturado->nome = $request->nome;
        $capturado->email = $request->email;
        $capturado->whatsapp = $request->whatsapp;
        $capturado->save();


        $user = User::find($request->fk_user);

        return view('captura.tela2',compact('user'));
    }

    public function index()
    {

        $capturados = Capturado::where('fk_user',Auth::user()->id)->get();
        return view('captura.index',compact('capturados'));
    }

 
    public function list()
    {
        $User = User::where('id', Auth::user()->id)
        ->get();
            
        return DataTables::of($User)
            ->editColumn('acoes', function ($User){
                return $this->setBtns($User);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(User $Users){
        $dados = "data-id_del='$Users->id' 
        data-id='$Users->id' 
        data-recru_texto1='$Users->recru_texto1' 
        data-recru_texto2='$Users->recru_texto2' 
        data-recru_fundo1='$Users->recru_fundo1' 
        data-recru_fundo2='$Users->recru_fundo2' 
        data-recru_video1='$Users->recru_video1' 
        data-recru_video2='$Users->recru_video2' 
        data-recru_link='$Users->recru_link' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        return $btnVer.$btnEditar;
    }

    public function store(Request $request)
    {  

        $rules = array(
               
        );
        $attributeNames = array(
        );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

           // dd($request->file('recru_fundo1'));

            if($request->file('recru_fundo1')){
                $recru_fundo1 = $request->file('recru_fundo1')->store('public/recrutamentos/imagens');
                $recru_fundo1 = str_replace('public','storage',$recru_fundo1);
            }

            if($request->file('recru_fundo2')){
                $recru_fundo2 = $request->file('recru_fundo2')->store('public/recrutamentos/imagens');
                $recru_fundo2 = str_replace('public','storage',$recru_fundo2);
            }

            $User = User::find( Auth::user()->id);
            $User->recru_texto1 = $request->recru_texto1;
            $User->recru_texto2 = $request->recru_texto2;
            $User->recru_video1 = $request->recru_video1;
            $User->recru_video2 = $request->recru_video2;
            $User->recru_fundo1 = $recru_fundo1;
            $User->recru_fundo2 = $recru_fundo2;
            $User->recru_link = $request->recru_link;
            $User->save();

            return response()->json($User);
        }
    }

    public function update(Request $request)
    {
        
        $User = User::find( Auth::user()->id);
        $User->recru_texto1 = $request->recru_texto1;
        $User->recru_texto2 = $request->recru_texto2;
        $User->recru_video1 = $request->recru_video1;
        $User->recru_video2 = $request->recru_video2;
        $User->recru_fundo1 = $request->recru_fundo1;
        $User->recru_fundo2 = $request->recru_fundo2;
        $User->recru_link = $request->recru_link;
        $User->save();
      
        return response()->json($User);
    }
}
