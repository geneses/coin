<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Response;
use DB;
use Auth;
use Mail;
use App\Mail\SendMailUser;
use App\User;
use App\Pacote;
use App\Transacao;
use App\SaldoBloqueado;
use App\SaqueUser;
use App\MaximoMinimo;
use App\Carrinho;
use App\SaldoUser;
use App\TransacaoTipo;
use App\FidelidadeCarrinho;
use App\Frete;
use DataTables;
use Hash;
use App\Http\Controllers\ProdutoController;


class FinanceiroController extends Controller
{
	public function index()
    {
        return view('financeiro_empresa.saques_solicitados');
    }  

    public static function moeda($get_valor) {

        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor);
        return $valor; 
    }

    public function buscarUserCompra(Request $request)
    {
        $user = User::where('user',$request->user)->first();

        $carrinho = Carrinho::JOIN('produtos','produtos.id','=','carrinhos.fk_produto')
                    ->SELECT('produtos.nome','produtos.valor_compra','carrinhos.quantidade',
                    'produtos.ponto_unilevel','produtos.comissao_produto','produtos.valor_v_usuario',
                    'produtos.pontos_binario','produtos.ponto_unilevel','produtos.pontos_carreira','carrinhos.fk_produto')
                    ->WHERE('carrinhos.fk_user', $user->id)
                    ->WHERE('carrinhos.codigo_pedido','!=',null)
                    ->GET();
        
        //return view('usuarios.pagar_user_compra',compact('user', 'carrinho'));
        return view('usuarios.pagar_uuario',compact('user', 'carrinho'));
    }

    public function credito()
	{
       
        return view('financeiro_empresa.credito');
	}

    public function list()
    {
        $saque_user = SaqueUser::join('users','users.id','=','saque_users.fk_user')
        ->select('saque_users.id','users.nome','saque_users.valor', 'saque_users.status','saque_users.created_at',
        'users.rua','users.complemento','users.numero','users.bairro','users.cidade','users.estado','users.cep','users.telefone','users.telefone2','users.banco','users.agencia','users.agencia_digito','users.conta','users.conta_digito','users.conta_tipo')
        ->orderBy('saque_users.created_at', 'desc')
        ->get();
        /*var_dump($saque_user);
        die();*/
        //dd($saque_user);
        return DataTables::of($saque_user)
            ->editColumn('acoes', function ($SaqueUser){
                return $this->setBtns($SaqueUser);
            })
            ->editColumn('created_at', function($SaqueUser){
            return date("d/m/Y H:i:s",strtotime($SaqueUser->created_at));
            })
            ->editColumn('valor', function($SaqueUser){
            return 'R$ '.number_format($SaqueUser->valor, 2, ',', '.');
            })
            ->escapeColumns([0])
            ->make(true);
    } 

    public function listarSaques()
    {
        $saque_user = SaqueUser::join('users','users.id','=','saque_users.fk_user')
        ->select('saque_users.id','users.nome','saque_users.valor', 'saque_users.status','saque_users.created_at',
        'users.rua','users.complemento','users.numero','users.bairro','users.cidade','users.estado','users.cep','users.telefone','users.telefone2','users.banco','users.agencia','users.agencia_digito','users.conta','users.conta_digito','users.conta_tipo')
        ->where('users.id',Auth::user()->id)
        ->orderBy('saque_users.created_at', 'desc')
        ->get();
            
        return DataTables::of($saque_user)
            ->editColumn('acoes', function ($SaqueUser){
                return $this->setBtns($SaqueUser);
            })
            ->editColumn('valor', function($SaqueUser){
            return 'R$ '.number_format($SaqueUser->valor, 2, ',', '.');
            })
            ->editColumn('created_at', function($SaqueUser){
            return date("d/m/Y H:i:s",strtotime($SaqueUser->created_at));
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(SaqueUser $saque){
        $dados = " 
        data-id='$saque->id' 
        data-nome='$saque->nome'
        data-rua='$saque->rua'
        data-cep='$saque->cep'
        data-complemento='$saque->complemento'
        data-numero='$saque->numero'
        data-bairro='$saque->bairro'
        data-cidade='$saque->cidade'
        data-estado='$saque->estado'
        data-telefone='$saque->telefone'
        data-telefone2='$saque->telefone2'
        data-banco='$saque->banco'
        data-agencia='$saque->agencia'
        data-agencia_digito='$saque->agencia_digito'
        data-conta='$saque->conta'
        data-conta_digito='$saque->conta_digito'
        data-conta_tipo='$saque->conta_tipo'
        data-valor='$saque->valor' 
        data-status='$saque->status' 
        data-created_at='$saque->created_at'
        ";

        $btnEditar = '';
        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver solicitação' $dados> <i class='fa fa-eye'></i></a> ";

        if($saque->status != 'CANCELADO' && $saque->status != 'PAGO'){
        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar solicitação' $dados> <i class='fa fa-edit'></i></a> ";
        }
        return $btnVer.$btnEditar;
    }

    public function adicionarCredito(Request $request)
    {

        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        if(!Hash::check($request->password, Auth::user()->senha_transacao)){
            return response()->json('Senha financeira errada',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

    	$user = User::where('user',$request->user)->first();

        $saldo_user = SaldoUser::where('fk_user',$user->id)->first();

        $valor = $this->moeda($request->valor);

        if($saldo_user){
            $saldo_user->saldo = $saldo_user->saldo + $valor;
            $saldo_user->save();
        }else{
            $saldo_user = New SaldoUser();
            $saldo_user->saldo = $saldo_user->saldo + $valor;
            $saldo_user->fk_user = $user->id;
            $saldo_user->save(); 
        }

        $transacao = TransacaoTipo::where('nome','Recebimento de transferência')->first();
        
        $transacao_user = new Transacao();
        $transacao_user->valor = $valor;
        $transacao_user->fk_transacao_tipo = $transacao->id;
        $transacao_user->fk_membro = $user->id;
        $transacao_user->fk_adesao_usuario = Auth::user()->id;
        $transacao_user->save();

        $transacao = TransacaoTipo::where('nome','Transferência para')->first();
        
        $transacao_user = new Transacao();
        $transacao_user->valor = $valor;
        $transacao_user->fk_transacao_tipo = $transacao->id;
        $transacao_user->fk_membro = Auth::user()->id;
        $transacao_user->fk_adesao_usuario = $user->id;
        $transacao_user->save();

        return response()->json($transacao_user);
    } 

    public function debito()
	{
        return view('financeiro_empresa.debito');
	}

    public function adicionarDebito(Request $request)
    {
        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        if(!Hash::check($request->password, Auth::user()->senha_transacao)){
            return response()->json('Senha financeira errada',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

    	$user = User::where('user',$request->user)->first();
        
        $saldo_user = SaldoUser::where('fk_user',$user->id)->first();
        
        $valor = $this->moeda($request->valor);

        if($saldo_user){
            $saldo_user->saldo = $saldo_user->saldo - $valor;
            $saldo_user->save();
        }else{
            $saldo_user = New SaldoUser();
            $saldo_user->saldo = $saldo_user->saldo - $valor;
            $saldo_user->fk_user = $user->id;
            $saldo_user->save(); 
        }

        $transacao = TransacaoTipo::where('nome','Débito')->first();
            
        $transacao_user = new Transacao();
        $transacao_user->valor = $valor;
        $transacao_user->fk_transacao_tipo = $transacao->id;
        $transacao_user->fk_membro = $user->id;
        $transacao_user->fk_adesao_usuario = Auth::user()->id;
        $transacao_user->save();

        $transacao = TransacaoTipo::where('nome','Débito para')->first();
        
        $transacao_user = new Transacao();
        $transacao_user->valor = $valor;
        $transacao_user->fk_transacao_tipo = $transacao->id;
        $transacao_user->fk_membro = Auth::user()->id;
        $transacao_user->fk_adesao_usuario = $user->id;
        $transacao_user->save();

        return response()->json($transacao_user);
    } 

    public function saque()
    {
        $saldo = User::JOIN('saldo_users','saldo_users.fk_user','users.id')
        ->where('users.id', Auth::user()->id)
        ->first();

        if($saldo != null){
            $saldo = $saldo->saldo;
        }else{
            $saldo = 0;
        }

        return view('usuarios.saque',compact('saldo'));
    }

    public function saquesSolicitados()
    {

        return view('financeiro_empresa.saques_solicitados');
    }
    
    public function saquesSolicitadosUsuario()
    {

        return view('financeiro_usuarios.saques_solicitados');
    }

    public function saquesSolicitadosExibir()
    {

        return view('financeiro_empresa.saques_solicitados_exibir');
    }


    public function criarSaque(Request $request)
    {

        $maximo = MaximoMinimo::where('tipo', 'saque')->first();

        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        $valor = $this->moeda($request->valor);

       if ($valor > $maximo->maximo or $valor < $maximo->minimo){
            return response()->json('Informe um valor acima de R$ '.$maximo->minimo .' e menor que R$'. $maximo->maximo .'  de zero!', $responsecode, $header, JSON_UNESCAPED_UNICODE);
        }


        if ($valor <= 0 or $valor == ''){
            return response()->json('Informe um valor acima de zero!', $responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        $saldo_user = SaldoUser::where('fk_user',Auth::user()->id)->first();

        if($saldo_user->saldo < $valor){
           return response()->json('Você não tem saldo suficiente',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        if(Hash::check($request->password, Auth::user()->senha_transacao)){
            
            $saldo_user->saldo = $saldo_user->saldo - $valor;
            $saldo_user->save();

            $transacao = TransacaoTipo::where('nome','Bloqueado')->first();
            
            $transacao_user = new Transacao();
            $transacao_user->valor = $valor;
            $transacao_user->fk_transacao_tipo = $transacao->id;
            $transacao_user->fk_membro = Auth::user()->id;
            $transacao_user->fk_adesao_usuario = Auth::user()->id;
            $transacao_user->save();

            $saque_user = new SaqueUser();
            $saque_user->valor = $valor;
            $saque_user->status = 'SOLICITADO';
            $saque_user->fk_user = Auth::user()->id;
            $saque_user->save();

            $saldo_bloqueado = new SaldoBloqueado();
            $saldo_bloqueado->saldo = $valor;
            $saldo_bloqueado->fk_saldo_user = $saque_user->id;
            $saldo_bloqueado->save();

            return response()->json(true,200);
        }else{
            return response()->json('Senha inválida',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }     
    }

    public function modificarSaque(Request $request)
    {
        $saque_users = SaqueUser::find($request->id);
        $saque_users->status = $request->status;
        $saque_users->save();
        $valor = $saque_users->valor;

        if($request->status == 'CANCELADO'){
            
            $saldo_user = SaldoUser::find($saque_users->fk_user);
            $saldo_user->saldo = $saldo_user->saldo + $valor;
            $saldo_user->save();

            $saldo_bloqueado = SaldoBloqueado::where('fk_saldo_user',$saque_users->id)
            ->delete();

            $transacao = TransacaoTipo::where('nome','Saldo desbloqueado')->first();
            
            $transacao_user = new Transacao();
            $transacao_user->valor = $valor;
            $transacao_user->fk_transacao_tipo = $transacao->id;
            $transacao_user->fk_membro = $saque_users->fk_user;
            $transacao_user->fk_adesao_usuario = $saque_users->fk_user;
            $transacao_user->save();
        }
        
        if($request->status == 'PAGO'){
            $saldo_bloqueado = SaldoBloqueado::where('fk_saldo_user',$saque_users->id)
            ->delete();

            $transacao = TransacaoTipo::where('nome','Saque')->first();
            
            $transacao_user = new Transacao();
            $transacao_user->valor = $valor;
            $transacao_user->fk_transacao_tipo = $transacao->id;
            $transacao_user->fk_membro = $saque_users->fk_user;
            $transacao_user->fk_adesao_usuario = $saque_users->fk_user;
            $transacao_user->save();
        }
        return response()->json($saque_users);
    }

    public function efetuarSaque(Request $request)
    {
        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        $user = User::find($request->user);

        $saldo_user = SaldoUser::where('fk_user',$user->id)->first();

        $valor = $this->moeda($request->valor);
        
        if($saldo_user->saldo > $valor){
            $saldo_user->saldo = $saldo_user->saldo - $valor;
            $saldo_user->save();
        }else{
            return response()->json('usuario sem saldo',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        $transacao = TransacaoTipo::where('nome','Saque')->first();
        
        $transacao_user = new Transacao();
        $transacao_user->valor = $valor;
        $transacao_user->fk_transacao_tipo = $transacao->id;
        $transacao_user->fk_membro = $user->id;
        $transacao_user->fk_adesao_usuario = $user->id;
        $transacao_user->save();

        return response()->json($transacao_user);
    }


    public function senha()
    {
        return view('financeiro_usuarios.senha');
    }

    public function solicitarSenha(Request $request)
    {
        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        if(Hash::check($request->password, Auth::user()->password)){

            Mail::to(Auth::user()->email)
            ->send(new SendMailUser());

            return response()->json(true,200);
        }else{
            return response()->json('Senha inválida',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

    } 

    public function extrato(Request $request)
    {
        $transacaos = [];
        $inicio = $request->inicio;
        //$inicio = date('Y/m/d', strtotime($request->inicio. ' - 1 days'));
       

        if($request->fim == null){
            $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->whereDate('transacaos.created_at', $inicio)
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem')
                ->WHERE('users.id',Auth::user()->id)->get();
        }else{
            $fim = date('Y/m/d', strtotime($request->fim. ' + 1 days'));
            
            $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->whereBetween('transacaos.created_at', [$inicio, $fim])
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem')
                ->WHERE('users.id',Auth::user()->id)->get();
        }

        return response()->json(['data'=> $transacoes]);
    } 

    public function getExtratoGeral()
    {
        return view('financeiro_empresa.extrato_geral');
    }

    public function extratoGeral(Request $request)
    {
        $transacaos = [];
        $inicio = $request->inicio;
    
        if($request->fim == null){
            $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->whereDate('transacaos.created_at', $inicio)
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem', 'users.user', 'u2.id as id_origem', 'users.id as id_destino')
                ->get();
        }else{
            $fim = date('Y/m/d', strtotime($request->fim. ' + 1 days'));
            
            $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->whereBetween('transacaos.created_at', [$inicio, $fim])
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem', 'users.user', 'u2.id as id_destino', 'users.id as id_origem')
                ->get();
        }

        return response()->json(['data'=> $transacoes]);
    }

    public function gerenciar_pedidos($nome = ''){
        return view('pedidos.index',compact('nome')) ;
    }

    public function gerenciar_pedidos_post(Request $request){
        $nome = $request->nome;
        return view('pedidos.index',compact('nome')) ;
    }
    
    public function getPedidos($nome = ''){
        if($nome == ''){


        $pedidos = Carrinho::JOIN('produtos','produtos.id','=','carrinhos.fk_produto')
        ->JOIN('users', 'users.id','carrinhos.fk_user')
    		->SELECT('carrinhos.codigo_pedido','carrinhos.fk_user','users.user as usuario','carrinhos.status')
            ->WHERE('carrinhos.codigo_pedido','!=',null)
            // ->groupBy('fidelidade_carrinhos.codigo_pedido')->
            ->distinct('carrinhos.codigo_pedido')
            ->GET();
        }else{
            $pedidos = Carrinho::JOIN('produtos','produtos.id','=','carrinhos.fk_produto')
            ->JOIN('users', 'users.id','carrinhos.fk_user')
            ->SELECT('carrinhos.codigo_pedido','carrinhos.fk_user','users.user as usuario','carrinhos.status')
            ->WHERE('carrinhos.codigo_pedido','!=',null)
            ->where('users.user', $nome)
            // ->groupBy('fidelidade_carrinhos.codigo_pedido')->
            ->distinct('carrinhos.codigo_pedido')
            ->GET();
        }
            //dd($pedidos);
            return DataTables::of($pedidos)
            ->editColumn('acoes', function ($pedidos){
               return $this->btnsPedidos($pedidos);
            })
            ->editColumn('valor', function($pedidos){
                $valor = 0;
                foreach(Carrinho::where('codigo_pedido', $pedidos->codigo_pedido)->get() as $valores){
                    $valor += $valores->valor * $valores->quantidade;
                }
                //return 'R$ '. Carrinho::where('codigo_pedido', $pedidos->codigo_pedido)->sum('valor').',00';
                return 'R$ '.number_format($valor,2);
            })
            ->editColumn('status', function($pedidos){
                return $pedidos->status;
            })
            ->escapeColumns([0])
            ->make(true);
            
    }
    /*
    public function getPedidos(){
        $pedidos = Carrinho::JOIN('produto_fidelidades','produto_fidelidades.id','=','carrinhos.fk_produto')
        ->JOIN('users', 'users.id','carrinhos.fk_user')
            ->SELECT('carrinhos.codigo_pedido','carrinhos.fk_user')
            ->WHERE('carrinhos.codigo_pedido','!=',null)
            // ->groupBy('fidelidade_carrinhos.codigo_pedido')->
            //->distinct('fidelidade_carrinhos.codigo_pedido')
            ->GET();
            return DataTables::of($pedidos)
            ->editColumn('acoes', function ($pedidos){
               return $this->btnsPedidos($pedidos);
            })
            ->editColumn('valor', function($pedidos){
            return 'R$ '. FidelidadeCarrinho::where('codigo_pedido', $pedidos->codigo_pedido)->sum('valor').',00';
            })->editColumn('usuario', function($pedidos){
                return $pedidos->usuario->nome;
                })
            ->escapeColumns([0])
            ->make(true);
            
    }*/
    private function btnsPedidos(Carrinho $pedidos){
        $btnPagar = "<a data-codigo='$pedidos->codigo_pedido' class='pagar-pedido btn btn-sm btn-success pagarpedido'>Pagar Pedido</a>";
        $btnCancelar = "<a data-codigo='$pedidos->codigo_pedido' class='cancelar-pedido btn btn-sm btn-danger cancelarpedido'>Cancelar Pedido</a>";
        return $btnPagar.$btnCancelar;
    }
    public function getPedido(Request $request){
        $pedido = Carrinho::JOIN('produtos','produtos.id','=','carrinhos.fk_produto')
        ->JOIN('users', 'users.id','carrinhos.fk_user')
    	->SELECT('produtos.nome','carrinhos.valor','carrinhos.quantidade','produtos.peso','carrinhos.codigo_pedido')
        ->WHERE('carrinhos.codigo_pedido',$request->codigo_pedido)
        ->GET();

            $total = 0;
            $pesos = 0;
            foreach($pedido as $valores){
                $total += $valores->quantidade * $valores->valor;
                $pesos += $valores->quantidade * $valores->peso;
                $codigo_pedido = $valores->codigo_pedido;
            }
            
            //frete
            $fretes = Frete::where('peso_inicial','<=',$pesos)
            ->where('peso_final','>=',$pesos)->select('valor')->first();

        return response()->json(['pedido' => $pedido, 'total' => $total, 'frete' => $fretes->valor, 'codigo_pedido' => $codigo_pedido]);
    }

    public function finalizarCompra(Request $request){

        //Saldo total
        foreach(Auth::user()->saldo as $saldo){
            $saldo_total = $saldo->saldo;
        }

        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        /*if(!Hash::check($request->senha, Auth::user()->senha_transacao)){
            return response()->json('Senha financeira errada',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }*/
        $total = 0;
        $pesos = 0;
        $carrinho = Carrinho::JOIN('produtos','produtos.id','=','carrinhos.fk_produto')
        ->JOIN('users', 'users.id','carrinhos.fk_user')
    	->SELECT('produtos.nome','carrinhos.valor','carrinhos.quantidade','produtos.peso','carrinhos.codigo_pedido','carrinhos.fk_produto','carrinhos.fk_user')
        ->WHERE('carrinhos.codigo_pedido',$request->codigo_pedido)
        ->GET();
        foreach($carrinho as $valores){
            $total += $valores->quantidade * $valores->valor;
            $pesos += $valores->quantidade * $valores->peso;
            $usuario = $valores->fk_user;
        }
        
        if($saldo_total < $total){
            return response()->json('Voce nao tem saldo suficiente',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }
              
        //frete
        if($request->tipo_retirada === "frete"){
            $fretes = Frete::where('peso_inicial','<=',$pesos)
            ->where('peso_final','>=',$pesos)->select('valor')->first();
            $total += $fretes->valor;
        }
        $metodo = new ProdutoController();
        if($metodo->buscarProduto($total,$usuario,$request->codigo_pedido))
            return response()->json('tudo certo');
        else{
            return response()->json('Erro',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }
        
        //Passar $total, $request->codigo_do_pedido, $usuario
        

    }

    public function finalizarPedido(Request $request){
        
        $produtos = FidelidadeCarrinho::where('fk_user',Auth::user()->id)->where('codigo_pedido',$request->codigo_pedido)->get();
        $total = FidelidadeCarrinho::where('fk_user',Auth::user()->id)->where('codigo_pedido',$request->codigo_pedido)->sum('valor');

        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        if(!Hash::check($request->senha, Auth::user()->senha_transacao)){
            return response()->json('Senha financeira errada',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        if(Auth::user()->ponto_fidelidade < $total){
            return response()->json('Voce nao tem saldo suficiente',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

            
        foreach ($produtos as $produto) {
            $fidelidade_compra = new FidelidadeUserCompra();
            $fidelidade_compra->fk_user = $produto->fk_user;
            $fidelidade_compra->fk_produto = $produto->fk_produto;
            $fidelidade_compra->valor = $produto->valor;
            $fidelidade_compra->quantidade = $produto->quantidade;
            $fidelidade_compra->save();
        }

        FidelidadeCarrinho::where('fk_user',Auth::user()->id)->delete();

        $user = User::find(Auth::user()->id);
        $user->ponto_fidelidade = $user->ponto_fidelidade - $total;
        $user->save();

        return response()->json($user);
    }
}




















