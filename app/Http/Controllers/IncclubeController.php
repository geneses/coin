<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Categoria;
use App\Banner;
use App\Carrinho;
use App\Role;
use App\User;
use Auth;
use Hash;

class IncclubeController extends Controller
{
    public function index(){

    	$banner = Banner::orderBy('id')->get();
    	$produtos = Produto::orderBy('created_at')
        ->where('ativo','true')
        ->get();

    	return view('incclube.index',compact('banner','produtos'));
    }

    public function deleteItemCarrinho($id){
        $carrinho = Carrinho::destroy($id);
        
        return response()->json($carrinho);
    }

    private function gerarcodigo(){
        #codigo iduser_date
        return 'U'.Auth::user()->id . 'DT' . date('ymdhis');
    }

    public function GerarPedido(Request $request){

        $produtos = Carrinho::where('fk_user',Auth::user()->id)->where('codigo_pedido',null)->get();
        $total = Carrinho::where('fk_user',Auth::user()->id)->where('codigo_pedido',null)->sum('valor');

        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        if(!Hash::check($request->senha, Auth::user()->senha_transacao)){
            return response()->json('Senha financeira errada',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        $codigo = $this->gerarcodigo();

        foreach ($produtos as $produto) {       
            $produto->codigo_pedido = $codigo;
            $produto->status = 'PENDENTE';
            $produto->save();
        }


        return response()->json();
    }

    public function todosProdutos(){
    	$produtos = Produto::orderBy('created_at')
        ->where('ativo','true')->get();

    	return view('incclube.produto.todos',compact('produtos'));
    }

    public function produtosCategoria($id){
    	$produtos = Produto::JOIN('categorias','categorias.id','=','produtos.fk_categoria')
            ->where('produtos.ativo','true')
    		->WHERE('categorias.id',$id)
            ->SELECT('produtos.nome','produtos.valor_v_site','produtos.valor_v_usuario','produtos.imagem')
    		->orderBy('produtos.created_at')->get();

    	return view('incclube.produto.todos',compact('produtos'));
    }

    public function detalheProduto($id){

    	$fotos = [];
    	$produtoSide = [];
    	$produto = Produto::JOIN('users','users.id','=','produtos.fk_user')
    		->LEFTJOIN('categorias','categorias.id','=','produtos.fk_categoria')
    		->SELECT('produtos.nome as nome_produto','produtos.valor_v_usuario', 'produtos.estoque'
    		,'users.nome as nome_usuario', 'users.telefone', 'users.email', 'users.estado', 'users.cidade','users.rua', 'produtos.imagem','categorias.nome as nome_categoria','produtos.id', 'produtos.valor_v_site', 'produtos.descricao')
    		->where('produtos.id',$id)
            ->where('produtos.ativo','true')
    		->orderBy('users.created_at')->first();

    	return view('incclube.produto.detalhe',compact('fotos','produto','produtoSide'));
    }

    public function carrinho(Request $request){
        
        $carrinho = new Carrinho();
        $carrinho->quantidade = $request->qtd;
        $carrinho->valor = $request->valor;
        $carrinho->fk_produto =  $request->fk_produto;
        $carrinho->fk_user = Auth::user()->id;
        $carrinho->save();
    }

    public function buscarProduto(Request $request){
        
        $produtos = Produto::Where('nome', 'like', '%' . $request->produto . '%')->get();
        
        return view('incclube.produto.todos',compact('produtos'));
    }

    public function storeUser(Request $request){
    	
    	$data_cadastro = date("Y-m-d"); 
            $user = new User();
            $user->nome = $request->nome;
            $user->user = $request->user;
            $user->ativo_mes = $request->ativo_mes;
            $user->avatar = $request->avatar;
            $user->data_nascimento = $request->data_nascimento;
            $user->cpf = $request->cpf;
            $user->rg = $request->rg;
            $user->email = $request->email;
            $user->telefone = $request->telefone;
            $user->telefone2 = $request->telefone2;
            $user->pis = $request->pis;
            $user->orgao_expedidor = $request->orgao_expedidor;
            $user->rua = $request->rua;
            $user->complemento = $request->complemento;
            $user->rua = $request->rua;
            $user->numero = $request->numero;
            $user->bairro = $request->bairro;
            $user->cidade = $request->cidade;
            $user->estado = $request->estado;
            $user->cep = $request->cep;
            $user->sexo = $request->sexo;
            $user->data_cadastro = $data_cadastro;
            $user->password = bcrypt($request->password);
            $user->save();

            $role_user = Role::where('name', 'usuario_site')->first();
            $user->roles()->sync($role_user); 
            
            return response()->json($user);
    }

    public function getCarrinho(){
    	$carrinho = Carrinho::JOIN('produtos','produtos.id','=','carrinhos.fk_produto')
    		->SELECT('produtos.nome','produtos.valor_v_usuario','carrinhos.quantidade','produtos.imagem','carrinhos.id','carrinhos.valor')
    		->WHERE('carrinhos.fk_user', Auth::user()->id)
             ->WHERENULL('carrinhos.codigo_pedido')
            ->GET();
        $valor = 0;
        foreach($carrinho as $carrinhos){
            $valor += floatval(number_format($carrinhos->valor * $carrinhos->quantidade,2));
        }
    	return view('incclube.carrinho.carrinho',compact('carrinho','valor'));
    }

    public function comprar(){
        return view('incclube.carrinho.carrinho',compact('carrinho'));
    }

    public function cadastroUser(){
    	return view('incclube.cadastro.cadastro');
    }
}
