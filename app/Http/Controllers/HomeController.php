<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Response;
use Cookie;
use App\User;
use App\Transacao;
use App\Slide;
use App\Qualificacao;
use Illuminate\Cookie\CookieJar;

class HomeController extends Controller
{

    public $filhos = []; 
    public $acimas = []; 
    
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {

        $saldo = User::JOIN('saldo_users','saldo_users.fk_user','=','users.id')
            ->join('qualificacaos','qualificacaos.id','=','users.fk_qualificacao')
            ->select('*')
            ->where('users.id',Auth::user()->id)
            ->first();

            $total_ganho = Transacao::leftJOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
            ->where('transacaos.fk_membro',Auth::user()->id)
            ->where('transacao_tipos.acao',1)
            ->select('transacaos.valor')
            ->sum('transacaos.valor');
        
        $slides = Slide::all();

        return view('auth.dashboard',compact('saldo','slides', 'total_ganho'));
    }

    public function registrar($user = '')
    {
        return view('auth/registrar',compact('user'));
    }

    public function login()
    {
        return view('auth.login');
    }

    public function loginContaUser($id)
    {
        Auth::loginUsingId($id);

        return redirect()->route('dashboard');
    }

    public function logar(Request $request)
    {
        $user = User::where('user',$request->email)->first();

        if($user != null){
            $request['email'] = $user->email;    
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password],
            $request->has('remember'))){

            if(Auth::user()->hasRole('usuario_site')){
                return redirect()->route('incclube.index');
            }else{
                return redirect()->route('dashboard');
            }            
        }

        return redirect()
        ->back()
        ->withInput()
        ->withErrors(['Usuário ou senha inválida']);
    }

    public function dashboard()
    {
         $saldo = User::LEFTJOIN('saldo_users','saldo_users.fk_user','=','users.id')
            ->LEFTJOIN('qualificacaos','qualificacaos.id','=','users.fk_qualificacao')
            ->select('users.nome as nome_user','users.ponto_esquerda','users.ponto_direita','users.total_ponto_es','users.total_ponto_di','saldo_users.saldo','qualificacaos.nome as nome_qualificacao', 'users.lado_rede', 'qualificacaos.cor')
            ->where('users.id',Auth::user()->id)
            ->first();

         $total_ganho = Transacao::leftJOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
            ->where('transacaos.fk_membro',Auth::user()->id)
            ->where('transacao_tipos.acao',1)
            ->select('transacaos.valor')
            ->sum('transacaos.valor');
         $slides = Slide::all();
        
        return view('auth.dashboard',compact('saldo','total_ganho','slides'));
    }


    public function buscaAcima($Acima){
        $Acima = User::where('acima',$Acima)
                ->get();            
        return $Acima;
    }

    public function buscaFilhoLadoZero($abaixo){
        $abaixo = User::where('acima',$abaixo)
                ->where('derramamento',0)
                ->first();            
        return $abaixo;
    }

    public function buscaFilhoLadoUm($abaixo){
        $abaixo = User::where('acima',$abaixo)
                ->where('derramamento',1)
                ->first();            
        return $abaixo;
    }



    public function buscaFilho($abaixo){
        $abaixo = User::where('acima',$abaixo)
                ->first();            
        return $abaixo;
    }

    public function addFilho($abaixo){

        $filho = $this->buscaFilho($abaixo);
       
        if ($filho != null)
        {  
            $this->addFilho($filho->user);
            array_push($this->filhos, ['user' => $filho->user,'lado' => $filho->lado_rede,'avatar' => $filho->avatar ]); 
        }else{
            return;
        }
    }

    public function redes($id = null){
        $neto_um_um_um='';$neto_um_um_zero='';$neto_um_zero_um='';$neto_um_zero_zero='';$neto_zero_um_um='';$neto_zero_um_zero='';$neto_zero_zero_um='';$neto_zero_zero_zero='';$neto_um_um='';$neto_um_zero='';$neto_zero_um='';$neto_zero_zero='';$filho_um='';$filho_zero='';

        if(!$id){
            $id = Auth::user()->id;
        }

        $qualificacaos = Qualificacao::all();

        $user = User::WHERE('id',$id)->first();

        //PRIMEIRO A ESQUERDA
        if($user){
            $filho_zero = $this->buscaFilhoLadoZero($user->user);
        }
        //PRIMEIRO A DIREITA
        if($user){
            $filho_um = $this->buscaFilhoLadoUm($user->user);
        }
        //PIMEIRO A DIREITA
        if($filho_zero){
            $neto_zero_zero = $this->buscaFilhoLadoZero($filho_zero->user);
        }
        //PIMEIRO A DIREITA
        if($filho_zero){
            $neto_zero_um = $this->buscaFilhoLadoUm($filho_zero->user);
        }
        //PIMEIRO A DIREITA
        if($filho_um){
            $neto_um_zero = $this->buscaFilhoLadoZero($filho_um->user);
        }
        //PIMEIRO A DIREITA
        if($filho_um){
            $neto_um_um = $this->buscaFilhoLadoUm($filho_um->user);
        }
        //PIMEIRO A DIREITA
        if($neto_zero_zero){
            $neto_zero_zero_zero = $this->buscaFilhoLadoZero($neto_zero_zero->user);
        }

        //PIMEIRO A DIREITA
        if($neto_zero_zero){
            $neto_zero_zero_um = $this->buscaFilhoLadoUm($neto_zero_zero->user);
        }

        //PIMEIRO A DIREITA
        if($neto_zero_um){
            $neto_zero_um_zero = $this->buscaFilhoLadoZero($neto_zero_um->user);
        }

        //PIMEIRO A DIREITA
        if($neto_zero_um){
            $neto_zero_um_um = $this->buscaFilhoLadoUm($neto_zero_um->user);
        }

        //PIMEIRO A DIREITA
        if($neto_um_zero){
            $neto_um_zero_zero = $this->buscaFilhoLadoZero($neto_um_zero->user);
        }

        //PIMEIRO A DIREITA
        if($neto_um_zero){
            $neto_um_zero_um = $this->buscaFilhoLadoUm($neto_um_zero->user);
        }

        //PIMEIRO A DIREITA
        if($neto_um_um){
            $neto_um_um_zero = $this->buscaFilhoLadoZero($neto_um_um->user);
        }

        //PIMEIRO A DIREITA
        if($neto_um_um){
            $neto_um_um_um = $this->buscaFilhoLadoUm($neto_um_um->user);
        }

        return view('usuarios.redes',compact('user','neto_um_um_um','neto_um_um_zero','neto_um_zero_um','neto_um_zero_zero','neto_zero_um_um','neto_zero_um_zero','neto_zero_zero_um','neto_zero_zero_zero','neto_um_um','neto_um_zero','neto_zero_um','neto_zero_zero','filho_um','filho_zero','qualificacaos'));
    }
}