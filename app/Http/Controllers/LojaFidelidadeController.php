<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProdutoFidelidade;
use App\BannerFidelidade;
use App\FidelidadeCarrinho;
use App\FidelidadeUserCompra;
use App\User;
use Auth;
use Hash;
use Response;

class LojaFidelidadeController extends Controller
{
     public function index(){

    	$banner = BannerFidelidade::orderBy('id')->get();
    	$produtos = ProdutoFidelidade::orderBy('created_at')
        ->where('ativo','true')
        ->get();

    	return view('fidelidade.index',compact('banner','produtos'));
    }

    public function todosProdutos(){
    	$produtos = ProdutoFidelidade::orderBy('created_at')
        ->where('ativo','true')->get();

    	return view('fidelidade.produto.todos',compact('produtos'));
    }

    public function produtosCategoria($id){
    	$produtos = ProdutoFidelidade::JOIN('fidelidade_categorias','fidelidade_categorias.id','=','produto_fidelidades.fk_categoria')
            ->where('produto_fidelidades.ativo','true')
    		->WHERE('fidelidade_categorias.id',$id)
            ->SELECT('produto_fidelidades.nome','produto_fidelidades.id', 'produto_fidelidades.valor_ponto_fidelidade','produto_fidelidades.imagem')
    		->orderBy('produto_fidelidades.created_at')->get();

    	return view('fidelidade.produto.todos',compact('produtos'));
    }

    public function detalheProduto($id){

    	$fotos = [];
    	$produtoSide = [];
    	$produto = ProdutoFidelidade::JOIN('users','users.id','=','produto_fidelidades.fk_user')
    		->LEFTJOIN('fidelidade_categorias','fidelidade_categorias.id','=','produto_fidelidades.fk_categoria')
    		->SELECT('produto_fidelidades.nome as nome_produto','produto_fidelidades.valor_ponto_fidelidade', 'produto_fidelidades.estoque'
    		,'produto_fidelidades.imagem','fidelidade_categorias.nome as nome_categoria','produto_fidelidades.id','produto_fidelidades.descricao')
    		->where('produto_fidelidades.id',$id)
            ->where('produto_fidelidades.ativo','true')
            ->first();

    	return view('fidelidade.produto.detalhe',compact('fotos','produto','produtoSide'));
    }

    public function carrinho(Request $request){
        
        $carrinho = new FidelidadeCarrinho();
        $carrinho->valor = $request->valor;
        $carrinho->quantidade = $request->qtd;
        $carrinho->fk_produto =  $request->fk_produto;
        $carrinho->fk_user = Auth::user()->id;
        $carrinho->save();
    }

    public function buscarProduto(Request $request){
        
        $produtos = ProdutoFidelidade::Where('nome', 'like', '%' . $request->produto . '%')->get();
        
        return view('fidelidade.produto.todos',compact('produtos'));
    }

    public function getCarrinho(){
        $carrinho = FidelidadeCarrinho::JOIN('produto_fidelidades','produto_fidelidades.id','=','fidelidade_carrinhos.fk_produto')
            ->SELECT('produto_fidelidades.nome','fidelidade_carrinhos.valor','fidelidade_carrinhos.quantidade','produto_fidelidades.imagem', 'fidelidade_carrinhos.id')
            ->WHERE('fidelidade_carrinhos.fk_user', Auth::user()->id)
            ->WHERE('fidelidade_carrinhos.codigo_pedido',null)
            ->GET();
        return view('fidelidade.carrinho.carrinho',compact('carrinho'));
    }

    public function deleteItemCarrinho($id){
    	$carrinho = FidelidadeCarrinho::destroy($id);
        
    	return response()->json($carrinho);
    }

    public function comprar(){
        return view('fidelidade.carrinho.carrinho',compact('carrinho'));
    }

    public function userExtrato(Request $request){
        
        $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->where('users.user', $request->user)
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem', 'users.user')
                ->get();

        return response()->json(['data'=> $transacoes]);
    }  

    public function userExtratoPacote(Request $request){
        
        $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->where('users.user', $request->user)
                ->where('transacao_tipos.nome', 'Compras de pacote')
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem', 'users.user')
                ->get();

        return response()->json(['data'=> $transacoes]);
    } 

    public function vendaPacote(){
        return view('financeiro_empresa.extrato_pacote');
    }


    public function vendaPacoteExtratoData(Request $request){
        $transacaos = [];
        $inicio = $request->inicio;
        
        if($request->fim == null){
            $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->where('transacao_tipos.nome', 'Compras de pacote')
                ->whereDate('transacaos.created_at', $inicio)
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem', 'users.user')
                ->get();
        }else{
            $fim = date('Y/m/d', strtotime($request->fim. ' + 1 days'));
            
            $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->where('transacao_tipos.nome', 'Compras de pacote')
                ->whereBetween('transacaos.created_at', [$inicio, $fim])
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem', 'users.user')
                ->get();
        }

        return response()->json(['data'=> $transacoes]);
    }

    public function finalizarCompra(Request $request){


        $produtos = FidelidadeCarrinho::where('fk_user',Auth::user()->id)->where('codigo_pedido',null)->get();
        $total = FidelidadeCarrinho::where('fk_user',Auth::user()->id)->where('codigo_pedido',null)->get();

        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        $soma = 0;
        foreach ($total as $value) {
            $soma += $value->valor * $value->quantidade;
        }

        if(!Hash::check($request->senha, Auth::user()->senha_transacao)){
            return response()->json('Senha financeira errada',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        if(Auth::user()->ponto_fidelidade < $soma){
            return response()->json('Voce nao tem saldo suficiente',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }
            
        foreach ($produtos as $produto) {
            $fidelidade_compra = new FidelidadeUserCompra();
            $fidelidade_compra->fk_user = $produto->fk_user;
            $fidelidade_compra->fk_produto = $produto->fk_produto;
            $fidelidade_compra->valor = $produto->valor;
            $fidelidade_compra->quantidade = $produto->quantidade;
            $fidelidade_compra->save();
        }

        FidelidadeCarrinho::where('fk_user',Auth::user()->id)->delete();

        $user = User::find(Auth::user()->id);
        $user->ponto_fidelidade = $user->ponto_fidelidade - $soma;
        $user->save();

        return response()->json($user);
    }
    private function gerarcodigo(){
        #codigo iduser_date
        return 'U'.Auth::user()->id . 'DT' . date('ymd');
    }
    public function GerarPedido(Request $request){

        $produtos = FidelidadeCarrinho::where('fk_user',Auth::user()->id)->where('codigo_pedido',null)->get();
        $total = FidelidadeCarrinho::where('fk_user',Auth::user()->id)->where('codigo_pedido',null)->sum('valor');

        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        if(!Hash::check($request->senha, Auth::user()->senha_transacao)){
            return response()->json('Senha financeira errada',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }


            $codigo = $this->gerarcodigo();
        foreach ($produtos as $produto) {
            
            $produto->codigo_pedido = $codigo;
            $produto->save();
        }


        return response()->json();
    }
}







