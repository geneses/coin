<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\Slide;

class SlideController extends Controller
{
     public function index()
    {
        return view('slides.index');
    }

 
    public function list()
    {
        $slide = Slide::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($slide)
            ->addColumn('imagem', function ($slide){
                 $url = asset($slide->imagem);
                 return '<img src='.$url.' width="150px"  />';
            })
            ->editColumn('acoes', function ($Slide){
                return $this->setBtns($Slide);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(Slide $Slides){
        $dados = "data-id_del='$Slides->id' 
        data-id='$Slides->id' 
        data-imagem='$Slides->imagem'
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'imagem' => 'required'

            
        );
        $attributeNames = array(
            'imagem' => 'Imagem'
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $imagem = $request->file('imagem')->store('public/imagens/slides');
            $imagem = str_replace('public','storage',$imagem);

            $slide = new Slide();
            $slide->imagem = $imagem;
            $slide->save();

            return response()->json($slide);
        }
    }

    public function update(Request $request)
    {
        
        $slide = Slide::find($request->id);
        $slide->nome = $request->nome;
        $slide->acao = $request->acao;
        $slide->save();
      
        return response()->json($slide);
    }

    public function destroy(Request $request)
    {
        $slide = slide::destroy($request->id_del);

        return response()->json($slide);
    }
}
