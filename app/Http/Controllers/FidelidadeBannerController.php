<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\BannerFidelidade;

class FidelidadeBannerController extends Controller
{
    public function index()
    {
        return view('fidelidade_banner.index');
    }

    public function list()
    {
        $fidelidade = BannerFidelidade::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($fidelidade)
            ->addColumn('imagem', function ($fidelidade){
                 $url = asset($fidelidade->imagem);
                 return '<img src='.$url.' width="150px"  />';
            })
            ->editColumn('acoes', function ($BannerFidelidade){
                return $this->setBtns($BannerFidelidade);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(BannerFidelidade $BannerFidelidades){
        $dados = "data-id_del='$BannerFidelidades->id' 
        data-id='$BannerFidelidades->id' 
        data-imagem='$BannerFidelidades->imagem' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'imagem' => 'required'           
        );
        $attributeNames = array(
            'imagem' => 'imagem'
        );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {
        	$imagem = $request->file('imagem')->store('public/imagens/BannerFidelidades');
            $imagem = str_replace('public','storage',$imagem);

            $BannerFidelidade = new BannerFidelidade();
            $BannerFidelidade->imagem = $imagem;
            $BannerFidelidade->link = $request->link;
            $BannerFidelidade->save();

            return response()->json($BannerFidelidade);
        }
    }

    public function update(Request $request)
    {

        $imagem = $request->file('imagem')->store('public/imagens/BannerFidelidades');
            $imagem = str_replace('public','storage',$imagem);
        
        $BannerFidelidade = BannerFidelidade::find($request->id);
        $BannerFidelidade->imagem = $imagem;
        $BannerFidelidade->link = $request->link;
        $BannerFidelidade->save();
      
        return response()->json($BannerFidelidade);
    }

    public function destroy(Request $request)
    {
        $BannerFidelidade = BannerFidelidade::destroy($request->id_del);

        return response()->json($BannerFidelidade);
    }
}
