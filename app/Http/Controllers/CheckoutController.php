<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DB;
use Auth;
use Carbon;
use Vindi;
use App\User;
use App\Qualificacao;
use App\SaldoUser;
use DatePeriod;
use DateInterval;
use DateTime;
use App\Produto;
use App\UnilevelProduto;
use App\FidelidadeProduto;
use App\TransacaoTipo;
use App\Transacao;
use App\Carrinho;
use App\UserCompra;
use App\Categoria;
use App\Parcelamento;
use App\Frete;
use App\Http\Controllers\ProdutoController;

class CheckoutController extends Controller
{
    public $usuario_acima = [];

    public function datas(){
        $data_atual = date("Y");
        $datas = new DatePeriod(new DateTime($data_atual), new DateInterval('P1Y'), 10);
        return $datas;
    }

    private function gerarcodigo(){
        #codigo iduser_date
        return 'U'.Auth::user()->id . 'DT' . date('ymdhis');
    }


    public function index()
    {
        //Consulta para retornar valores da compra ao usuário
        $carrinhos = Carrinho::where('fk_user',Auth::user()->id)
        ->where(function($query){
            $query->where('status','!=','PAGO')
            ->orwhereNull('status');
        })
        ->select('valor','quantidade','fk_produto')
        ->get();
        
        //dd($carrinhos);
        $valor = 0;
        $peso = 0;
        foreach($carrinhos as $carrinho){
            $valor = floatval(number_format($valor += $carrinho->valor * $carrinho->quantidade,2));
            $peso = floatval(number_format($peso += $carrinho->produto->peso));
        }
        //dd($valor);
        //Saldo Usuário
        $saldo_user = SaldoUser::where('fk_user',Auth::user()->id)->select('saldo')->first();
        
       
        /*Parcelamentos
        $parcelamentos = Parcelamento::orderBy('parcelas')->get();*/

        $frete = Frete::where('peso_inicial','<=',$peso)
        ->where('peso_final','>=',$peso)
        ->select('id','valor')
        ->first();
        
    	$datas = $this->datas();
        
    	return view('checkout.index',compact('datas','saldo_user','valor','carrinhos','frete'));
    }

    public function parcelas(Request $request){
        //Parcelamentos
        $parcelamentos = Parcelamento::orderBy('parcelas')->get();
        //Consulta para retornar valores da compra ao usuário
        $carrinhos = Carrinho::where('fk_user',Auth::user()->id)
        ->where(function($query){
            $query->where('status','!=','PAGO')
            ->orwhereNull('status');
        })
        ->select('valor','quantidade','fk_produto')
        ->get();
        $parecelas = array();
        foreach($carrinhos as $carrinho){
            foreach($parcelamentos as $parcelamento){
                $valor_mais_percentual = ($carrinho->valor * ($parcelamento->porcentagem) / 100 ) + $carrinho->valor;
                $valor_com_frete = ($valor_mais_percentual + ($request->total / $carrinho->quantidade) ) * $carrinho->quantidade;
                $texto = $parcelamento->parcelas."x de R$ ".$valor_com_frete / $parcelamento->parcelas;
                array_push($parecelas,[
                    'id' => $parcelamento->id,
                    'valor' => $texto,
                    'porcentagem' => $parcelamento->porcentagem,
                    'parcelas' => $parcelamento->parcelas
                ]);        
            }
        }
        
        //{{$parcelamento->parcelas}}x de R$ {{($valor+($parcelamento->porcentagem/100)) / $parcelamento->parcelas}}
        
        
        return response()->json($parecelas);
    }
    

    public function buscaAcimaPagar($acima){
        $acima = User::where('user',$acima)
                ->first();            
        return $acima;
    }

    public function addAcima($acima){

        $x = $this->buscaAcimaPagar($acima);
       
        if ($x != null)
        {  
            $this->addAcima($x->acima);
            array_push($this->usuario_acima, ["user"=> $x->user, "lado"=> $x->derramamento]); 
              
        }else{
            return;
        }
    }

    public function postback(Request $request)
    {

        $json = $request->all();

        $json = json_encode($json);
        $json = json_decode($json);

        if(isset($json->event->type)){
            if($json->event->type ==  'bill_paid'){
                $id = $json->event->data->bill->customer->code;

                $carrinho = Carrinho::JOIN('produtos','produtos.id','=','carrinhos.fk_produto')
                    ->SELECT('produtos.nome','produtos.valor_compra','carrinhos.quantidade',
                    'produtos.ponto_unilevel','produtos.comissao_produto','produtos.valor_v_usuario',
                    'produtos.pontos_binario','produtos.ponto_unilevel','produtos.pontos_carreira','carrinhos.fk_produto')
                    ->WHERE('carrinhos.fk_user', $id)
                    ->GET();
        
            
            foreach ($carrinho as $produto) {

                $user_compra = new UserCompra();
                $user_compra->quantidade = $produto->quantidade;
                $user_compra->fk_produto = $produto->fk_produto;
                $user_compra->fk_user = $id;
                $user_compra->save();

                //$produto1 = Produto::where('id',1)->first();

                $usuario = User::where('id', $id)->first();
                $usuario->ponto_unilevel = $usuario->ponto_unilevel + $produto->ponto_unilevel;
                $usuario->save();

                $usuario_acima = $this->buscaAcimaPagar($usuario->acima);

                if(isset($usuario_acima)){
                    $this->addAcima($usuario_acima->acima);
                }else{
                    $usuario_acima = $usuario;
                }

                array_push($this->usuario_acima, ["user"=>  $usuario_acima->user, "lado"=> $usuario_acima->derramamento]);

                array_push($this->usuario_acima, ["user"=>  $usuario->user, "lado"=> $usuario->derramamento]);

                for ($i=0; $i < count($this->usuario_acima); $i++) { 
                   
                    if($this->usuario_acima[$i]['user'] != $usuario->user){            
                        $user_binario = User::where('user', $this->usuario_acima[$i]['user'])->first();

                            if($this->usuario_acima[$i+1]['lado'] == 2){
                                $user_binario->ponto_esquerda =  $user_binario->ponto_esquerda + $produto->pontos_binario; 
                                $user_binario->total_ponto_es = $user_binario->total_ponto_es + $produto->pontos_binario; 
                            }else{
                                $user_binario->ponto_direita =  $user_binario->ponto_direita + $produto->pontos_binario; 
                                $user_binario->total_ponto_di = $user_binario->total_ponto_di + $produto->pontos_binario; 
                            } 

                            $user_binario->save();
                        }      
                }

                $unilevel_produto_1 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 1')
                ->first();

                $unilevel_produto_2 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 2')
                ->first();

                $unilevel_produto_3 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 3')
                ->first();

                $unilevel_produto_4 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 4')
                ->first();
                
                $unilevel_produto_5 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 5')
                ->first();

                $unilevel_produto_6 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 6')
                ->first();
                
                $unilevel_produto_7 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 7')
                ->first();
                
                $unilevel_produto_8 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 8')
                ->first();

                $unilevel_produto_9 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 9')
                ->first();

                $unilevel_produto_10 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 10')
                ->first();

                $unilevel_produto_11 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 11')
                ->first();

                $unilevel_produto_12 = UnilevelProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 12')
                ->first();        

                $transacao_tipo = TransacaoTipo::where('nome','Compras de produto')->first();   

                $transacao = New Transacao();
                $transacao->valor = $produto->valor_v_usuario;
                $transacao->fk_transacao_tipo = $transacao_tipo->id;
                $transacao->fk_membro = $id;
                $transacao->save(); 

                $user = User::where('id',$id)->first();

                $transacao_tipo_patrocinador = TransacaoTipo::where('nome','Bônus de venda')->first();

                $patrocinador = User::where('user',$user->patrocinador)->first();
                //SEGUNDO NIVEL
                 if($patrocinador != null){
                    if($patrocinador != null && $unilevel_produto_1 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_1->valor;
                            $saldo_patrocinador_new->saldo = $saldo_patrocinador_new->saldo + $produto->comissao_produto;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_1->valor;
                            $saldo_patrocinador->save();
                        }
                       
                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_1->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //SEGUNDO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                    
                    if($patrocinador != null && $unilevel_produto_2 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();
                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_2->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_2->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_2->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //TERCEIRO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $unilevel_produto_3 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_3->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_3->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_3->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //QUARTO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $unilevel_produto_4 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_4->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_4->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_4->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //QUINTO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $unilevel_produto_5 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_5->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_5->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_5->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                 //SEXTO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $unilevel_produto_6 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_6->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_6->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_6->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }
          
                 //SETIMO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $unilevel_produto_7 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_7->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_7->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_7->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //OITAVO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $unilevel_produto_8 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_8->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_8->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_8->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //NONO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $unilevel_produto_9 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_9->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_9->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_9->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //DECIMO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $unilevel_produto_10 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_10->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_10->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_10->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //DECIMO PRIMEIRO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $unilevel_produto_11 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_11->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_11->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_11->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }   

                //DECIMO SEGUNDO NIVEL
                if($patrocinador != null){  
                     $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                    if($patrocinador != null && $unilevel_produto_12 != null){
                        $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                        if($saldo_patrocinador == null){
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = $unilevel_produto_12->valor;
                            $saldo_patrocinador_new->fk_user = $patrocinador->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $unilevel_produto_12->valor;
                            $saldo_patrocinador->save();
                        }

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $unilevel_produto_12->valor;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //FIDELIDADE PRODUTO

                $fidelidade_produto_1 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 1')
                ->first();

                $fidelidade_produto_2 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 2')
                ->first();

                $fidelidade_produto_3 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 3')
                ->first();

                $fidelidade_produto_4 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 4')
                ->first();
                
                $fidelidade_produto_5 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 5')
                ->first();

                $fidelidade_produto_6 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 6')
                ->first();
                
                $fidelidade_produto_7 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 7')
                ->first();
                
                $fidelidade_produto_8 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 8')
                ->first();

                $fidelidade_produto_9 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 9')
                ->first();

                $fidelidade_produto_10 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 10')
                ->first();

                $fidelidade_produto_11 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 11')
                ->first();

                $fidelidade_produto_12 = FidelidadeProduto::where('fk_produto', $produto->id)
                ->where('nivel','Nivel 12')
                ->first();

                $patrocinador = User::where('user',$user->patrocinador)->first();
                $transacao_tipo_patrocinador = TransacaoTipo::where('nome','Pontos de fidelidade')->first();
               
                //SEGUNDO NIVEL
                 if($patrocinador != null){
                    if($patrocinador != null && $fidelidade_produto_1 != null){
                      
                       $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_1->ponto;
                        $patrocinador->save(); 
                        
                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $produto->valor_v_usuario;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }
                
                //SEGUNDO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                    
                    if($patrocinador != null && $fidelidade_produto_2 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_2->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_2->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //TERCEIRO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $fidelidade_produto_3 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_3->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_3->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //QUARTO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $fidelidade_produto_4 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_4->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_4->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //QUINTO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $fidelidade_produto_5 != null){
                       $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_5->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_5->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                 //SEXTO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $fidelidade_produto_6 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_6->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_6->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }
          
                 //SETIMO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $fidelidade_produto_7 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_7->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_7->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //OITAVO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $fidelidade_produto_8 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_8->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_8->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //NONO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $fidelidade_produto_9 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_9->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_9->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //DECIMO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $fidelidade_produto_10 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_10->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_10->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }

                //DECIMO PRIMEIRO NIVEL
                if($patrocinador != null){ 
                    $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                   
                    if($patrocinador != null && $fidelidade_produto_11 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_11->ponto;
                        $patrocinador->save(); 

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_11->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }   

                //DECIMO SEGUNDO NIVEL
                if($patrocinador != null){  
                     $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                    if($patrocinador != null && $fidelidade_produto_12 != null){
                        $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $fidelidade_produto_12->ponto;
                        $patrocinador->save();

                        $patrocinador_transacao = New Transacao();
                        $patrocinador_transacao->valor = $fidelidade_produto_12->ponto;
                        $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                        $patrocinador_transacao->fk_membro = $patrocinador->id;
                        $patrocinador_transacao->save();
                    }
                }
            }

            $carrinho = Carrinho::WHERE('fk_user',$id)->delete();

            return response()->json('true'); 
        }

        }
        // $status = $json = $json->data->bill->charges[0]->last_transaction->status;
        // return $status.$id;
	}


   	public function pagar(Request $request)
    {
    $productService = new Vindi\Product;
    //dd($request->all());
    /*foreach($productService->all() as $produtos){
        if()
        $vindi_id_produto = '';
    }*/
    $responsecode = 404;   
    
    $header = array (
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'utf-8'
    );
    
    
    $arguments = array(
        'VINDI_API_KEY' => env('VINDI_API_KEY'),
        'VINDI_API_URI' => env('VINDI_API_URI')
    );
     
    // $id = 3;
    $id = Auth::user()->id;
    
    //Consulta para retornar valores da compra ao usuário
    $carrinhos = Carrinho::where('fk_user',Auth::user()->id)
    ->where(function($query){
        $query->where('status','!=','PAGO')
        ->orwhereNull('status');
    })
    ->select('valor','quantidade','id','fk_produto')
    ->get();
    $valor = 0;
    $peso = 0;
    foreach($carrinhos as $carrinho){
        
        $valor = floatval(number_format($valor += $carrinho->valor * $carrinho->quantidade,2));
        $peso = floatval(number_format($peso += $carrinho->produto->peso));
    }
    $frete = Frete::where('peso_inicial','<=',$peso)
    ->where('peso_final','>=',$peso)
    ->select('id','valor')
    ->first();
    $frete_form = $request->frete == "1" ? true : false;
    $valor = $frete_form ? $valor + $frete->valor : $valor;

    //dd($valor);
    
    //Saldo Usuário
    $saldo_user = SaldoUser::where('fk_user',Auth::user()->id)->select('saldo')->first();

    //Condições para Saldo
    if($request->saldo_pagar != null){
        //Saldo insuficiente
        if($saldo_user->saldo < $request->saldo_pagar){
            return response()->json('Você não tem saldo suficiente',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }
        elseif($request->saldo_pagar > $valor)
            return response()->json('Valor superior ao da compra',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        else if($request->saldo_pagar < 0) //Valor Menor que 0
            return response()->json('Valor inadequado',$responsecode, $header, JSON_UNESCAPED_UNICODE);
    }
    
    //Descontando Saldo
    if(!is_null($request->saldo_pagar))
        $valor -= $request->saldo_pagar;
    
    //Funções da VINDI
    $customerService = new Vindi\Customer($arguments);
   
    //Consulta de Usuário
    $customer = $customerService->all();
    foreach ($customer as  $value) {
        if($value->code == $id){
            $id_cutomer = $value->id;
        }
    }
    
    
    $datas = $this->datas();
    $productService = new Vindi\Product;
    $produto = array();
    foreach($productService->all() as $produtos){
        foreach($request->fk_produtos as $itens){
            if(intval($itens) == $produtos->code)
                array_push($produto,$produtos->id);
        }
    }


    //Valores por Produto
    $valores = array();
    $total = 0;
    //Parcelas
    $parcelamento = Parcelamento::where('id',$request->Parcelas)->first();
    
    foreach($carrinhos as $carrinho){
        
        $valor_mais_percentual = floatval(number_format(($carrinho->valor * ($parcelamento->porcentagem) / 100 ) + $carrinho->valor,2));
        $valor_com_frete = number_format($valor_mais_percentual + ($frete->valor / $carrinho->quantidade),2);

        array_push($valores,floatval($valor_com_frete));
        $total +=  $valor_com_frete * $carrinho->quantidade;
    }
    //Total + frete
    //$total += $frete->valor;
    //var_dump($valores[0]);
    //dd($total, $parcelamento->parcelas);
    $aux_parcela = intval($parcelamento->parcelas);
    foreach($produto as $key => $i_produto){
        //dd($valores[$key]);
        //Realizando Pagamentos
        $bill = new Vindi\Bill($arguments);
        $bill = $bill->create([
            'customer_id' => $id_cutomer,
            'payment_method_code' => 'credit_card',
            'metadata' => 'metadata',
            'installments' => $aux_parcela,
            'payment_profile' => [
                'holder_name' => $request->card_holder_name,
                'card_expiration' => $request->card_expiration_month.'/'.$request->card_expiration_year,
                'card_number' => $request->card_number,
                'card_cvv' => $request->card_cvv
            ],
            'bill_items' => [[
                'product_id' => intval($i_produto),
                'amount' => $valores[$key]
            ]],

        ]);

        //Gerar Código e binificações
        if($bill){
            $codigo = $this->gerarcodigo();
            foreach($carrinhos as $carrinho){
                
                Carrinho::where('id',$carrinho->id)->update([
                    'codigo_pedido' => $codigo,
                    'updated_at' => date("Y-m-d H:i:s",strtotime("now"))
                ]);
            }
            $metodo = new ProdutoController();
            if($metodo->buscarProduto($total,Auth::user()->id,$codigo))
                return response()->json('Pagamento Efetuado');
            else
                return response()->json('Erro',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }
    }        
      
        return response()->json($bill);
    }
    
}