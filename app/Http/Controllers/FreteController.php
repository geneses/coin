<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use DataTables;
use App\Frete;

class FreteController extends Controller
{
    public function index(){
        return view('frete.index');
    }
    public function list(){
        $frete = Frete::all();
            
        return DataTables::of($frete)
            ->editColumn('acao', function ($frete){
                return $this->setBtns($frete);
            })->editColumn('peso', function ($frete){
                return $frete->peso_inicial.' a '.$frete->peso_final;
            })->escapeColumns([0])
            ->make(true);
    }
    public static function moeda($get_valor) {
        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor);
        return $valor; 
    }
    private function setBtns(Frete $frete){
        $dados = "data-id_del='$frete->id' 
        data-id='$frete->id' 
        data-titulo='$frete->titulo' 
        data-peso_inicial='$frete->peso_inicial'
        data-peso_final='$frete->peso_final'
        data-valor='$frete->valor'
        data-observacoes='$frete->observacoes'
        ";
        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar Frete' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar Frete' $dados><i class='fa fa-trash'></i></a>";

        return $btnEditar.$btnDeletar;
    }
    public function store(Request $request){  
         
        $rules = array(
            'titulo' => 'required',
            'peso_inicial' => 'required',
            'peso_final' => 'required',
            'valor' => 'required'
            
        );
        $attributeNames = array(
            'titulo' => 'Titulo',
            'peso_inicial' => 'Peso Inicial',
            'pesso_final' => 'Peso Final',
            'valor' => 'Valor'
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $Frete = new Frete();
            $Frete->titulo = $request->titulo;
            $Frete->peso_inicial = $request->peso_inicial;
            $Frete->peso_final = $request->peso_final;
            $Frete->valor = $this->moeda($request->valor);
            $Frete->observacoes = $request->observacoes;
            $Frete->save();
            return response()->json($Frete);
        }
    }

    public function update(Request $request){  
         
        $rules = array(
            'titulo' => 'required',
            'peso_inicial' => 'required',
            'peso_final' => 'required',
            'valor' => 'required'
            
        );
        $attributeNames = array(
            'titulo' => 'Titulo',
            'peso_inicial' => 'Peso Inicial',
            'pesso_final' => 'Peso Final',
            'valor' => 'Valor'
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $Frete = Frete::find($request->id);
            $Frete->titulo = $request->titulo;
            $Frete->peso_inicial = $request->peso_inicial;
            $Frete->peso_final = $request->peso_final;
            $Frete->valor = $this->moeda($request->valor);
            $Frete->observacoes = $request->observacoes;
            $Frete->save();
            return response()->json($Frete);
        }
    }
    public function destroy(Request $request)
    {
        $Frete = Frete::destroy($request->id_del);

        return response()->json($Frete);
    }
}
