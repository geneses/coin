<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\Categoria;

class CategoriaController extends Controller
{
     public function index()
    {
        return view('categoria.index');
    }

 
    public function list()
    {
        $produto = Categoria::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($produto)
            ->editColumn('acoes', function ($Categoria){
                return $this->setBtns($Categoria);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(Categoria $Categorias){
        $dados = "data-id_del='$Categorias->id' 
        data-id='$Categorias->id' 
        data-nome='$Categorias->nome' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'nome' => 'required'            
        );
        $attributeNames = array(
            'nome' => 'Nome'
        );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $Categoria = new Categoria();
            $Categoria->nome = $request->nome;
            $Categoria->save();

            return response()->json($Categoria);
        }
    }

    public function update(Request $request)
    {
        
        $Categoria = Categoria::find($request->id);
        $Categoria->nome = $request->nome;
        $Categoria->save();
      
        return response()->json($Categoria);
    }

    public function destroy(Request $request)
    {
        $Categoria = Categoria::destroy($request->id_del);

        return response()->json($Categoria);
    }
}
