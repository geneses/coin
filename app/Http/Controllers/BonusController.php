<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BonusController extends Controller
{
   public function index(Request $request){
       $titulo = $request->titulo;
       $bonus = [
                 'Bônus início rápido' => 'ESSE BÔNUS É CRIADO QUANDO SE CADASTRA UM PACOTE',
                 'Bônus binário' => 'ESSE BONUS É CRIADO QUANDO SE CADASTRA UM PACOTE',
                 'Bônus matriz forçada' => 'ESSE BONUS FUNCIONA QUANDO UM USUARIO PAGO E SEU PATROCINADO NAO TIVER ATIVO, RECEBE O PATROCINADOR DO SEU PATROCINADOR',
                 'Bônus indicação' => 'ESSE BÔNUS É CRIADO QUANDO SE CADASTRA UM PACOTE',
                 'Bônus venda direta' => 'ESSE BÔNUS É CRIADO QUANDO SE CADASTRA UM PACOTE',
                 'Bônus venda indireta' => 'ESSE BÔNUS É CRIADO QUANDO SE CADASTRA UM PACOTE',
                 'Bônus fidelidade' => 'ESSE BONUS É CRIADO QUANDO SE CADASTRA UM PACOTE'];
            return view('bonus.index', compact('bonus', 'titulo'));
   }
}
