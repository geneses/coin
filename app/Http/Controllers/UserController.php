<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use Vindi;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Pacote;
use App\Transacao;
use App\SaldoUser;
use App\TransacaoTipo;
use App\PacoteNivel;
use App\PontoUnilevel;
use App\MaximoMinimo;
use App\ImagemPadrao;
use App\Role;
use App\Infinite;
use Hash;


class UserController extends Controller
{
    public $usuario_acima = [];
    public $filhos = []; 
    public $usuario_acima_pai = []; 
    public $user_lado = '';  
    public $patrocinador_encontado;
    public $patrocinador_encontado_2;
    public $cont = 0 ;


    public function index($tipo)
    {
        return view('usuarios.index',compact('tipo'));
    } 

    public function buscarUsuario()
    {
        return view('usuarios.buscar_user');
    }

    public function redeIndicacao()
    {
        $user = User::find(Auth::user()->id);

        $usuarios = User::WHERE('patrocinador',$user->user)->get();
        
        return view('usuarios.rede_indicacao',compact('usuarios'));
    }  

    public function redePatrocinador($patrocinador)
    {

        $usuarios = User::WHERE('patrocinador',$patrocinador)->get();
        
        return response()->json(['data'=>$usuarios]);
    }  

    public function redePatrocinadorVoltar($patrocinador)
    {

        $user = User::WHERE('user',$patrocinador)->first();
        $usuarios = User::WHERE('patrocinador',$user->patrocinador)->get();
        
        return response()->json(['data'=>$usuarios]);
    }  

    public function transferencia()
    {
        $saldo = User::JOIN('saldo_users','saldo_users.fk_user','users.id')
        ->where('users.id', Auth::user()->id)
        ->first();

        if($saldo != null){
            $saldo = $saldo->saldo;
        }else{
            $saldo = 0;
        }

        return view('usuarios.transferencia',compact('saldo'));
    }

    public function verificaEmail(Request $request)
    {
        $user = User::where('email',$request->email)->first();

        return response()->json($user);
    } 

    public function verificaUserName(Request $request)
    {
        $user = User::where('user',$request->user)->first();

        return response()->json($user);
    } 

    public function nomeUser(Request $request)
    {
        $user = User::where('user',$request->user)->first();

        return response()->json($user);
    }

    public function senha()
    {
        return view('usuarios.senha');
    }

    public function avatar()
    {
        return view('usuarios.avatar');
    }

    public function updateAvatar(Request $request)
    {
        if($request->file('avatar')){
            $avatar = $request->file('avatar')->store('public/imagens/avatares');
            $avatar = str_replace('public','storage',$avatar);
        }

        $user = User::find($request->id);
        $user->avatar =  $avatar;
        $user->save();

        return response()->json($user);
    }

    public function imagemPadrao()
    {
        return view('usuarios.imagem_padrao');
    }

    public function updateImagemPadrao(Request $request)
    {
        $imagem_padrao = ImagemPadrao::first();

        if($request->file('imagem')){
            $imagem = $request->file('imagem')->store('public/imagens/imagem_padrao');
            $imagem = str_replace('public','storage',$imagem);
        }

        if($imagem_padrao){
            $imagem_padrao->imagem = $imagem;
            $imagem_padrao->save();
        }else{
            $imagem_padrao = new ImagemPadrao();
            $imagem_padrao->imagem = $imagem;
            $imagem_padrao->save();
        }

        return response()->json($imagem_padrao);
    } 


    public function ladoRede(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->lado_rede =  $request->lado_rede;
        $user->save();
        
        return response()->json($user);
    } 

    public static function moeda($get_valor) {

        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor);
        return $valor; 
    }

    public function criarTransferencia(Request $request)
    {
        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );

        $user = User::find($request->id);
        $saldo_user = SaldoUser::where('fk_user',$user->id)->first();

        $favorecido = User::where('user',$request->patrocinador)->first();

        if(!$favorecido){
            return response()->json('Usuário inexistente', $responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

         $maximo = MaximoMinimo::where('tipo', 'transferencia')->first();

        $valor = $this->moeda($request->valor);

        if ($valor > $maximo->maximo or $valor < $maximo->minimo){
            return response()->json('Informe um valor acima de R$ '.$maximo->minimo .' e menor que R$'. $maximo->maximo .'  de zero!', $responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        if($valor <= 0 or $valor == ''){
            return response()->json('Informe um valor acima de zero!', $responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        if($saldo_user->saldo < $valor){
           return response()->json('Você não tem saldo suficiente',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        if(!Hash::check($request->password, Auth::user()->senha_transacao)){
            return response()->json('Senha incorreta', $responsecode, $header, JSON_UNESCAPED_UNICODE);
        }else{
            
            $saldo_user->saldo = $saldo_user->saldo - $valor;
            $saldo_user->save(); 

            $transacao_para = TransacaoTipo::where('nome','Transferência para')->first();
            $transacao_transferencia = TransacaoTipo::where('nome','Recebimento de transferência')->first();

            $saldo_favoricido = SaldoUser::where('fk_user',$favorecido->id)->first();

            $transacao_user = new Transacao();
            $transacao_user->valor = $valor;
            $transacao_user->fk_transacao_tipo = $transacao_para->id;
            $transacao_user->fk_membro = $user->id;
            $transacao_user->fk_adesao_usuario = $favorecido->id;
            $transacao_user->save();

            $transacao_favorecido = new Transacao();
            $transacao_favorecido->valor = $valor;
            $transacao_favorecido->fk_transacao_tipo = $transacao_transferencia->id;
            $transacao_favorecido->fk_membro = $favorecido->id;
            $transacao_favorecido->fk_adesao_usuario = $user->id;
            $transacao_favorecido->save();

            if($saldo_favoricido == null){
                $saldo_favoricido = New SaldoUser();
                $saldo_favoricido->saldo = $valor;
                $saldo_favoricido->fk_user = $favorecido->id;
                $saldo_favoricido->save(); 
            }else{
                $saldo_favoricido->saldo = $saldo_favoricido->saldo + $valor;
                $saldo_favoricido->save(); 
            }

            return response()->json($saldo_favoricido);
        }
    }


    public function updateSenha(Request $request)
    {
        $user = User::find($request->id);

        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json($user);
    } 

    public function cadastroUsuario()
    {
        return view('auth.cadastro_painel');
    }

    public function getUsuario(Request $request)
    {
        $usuario = User::where('user',$request->user)->first();

        $pacote = Pacote::find($usuario->fk_pacote);

        if($pacote){
            $pacotes = Pacote::where('disponivel', $pacote->id)->get();
        }else{
            $pacotes = Pacote::orderBy('created_at','asc')->where('disponivel', 1)->get();
        }


        $view = view("usuarios.pagar_usuario",compact('usuario','pacotes'))->render();
        return $view;
    }

    public function extrato()
    {
        $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
            ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
            ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
            ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem')
            ->WHERE('users.id',Auth::user()->id)->get();

        $saldo_user = SaldoUser::where('fk_user',Auth::user()->id)->first();

        return view("usuarios.extrato",compact('transacoes','saldo_user'));      
    }

    public function bancario()
    {
        return view("usuarios.bancario");      
    }
    
    public function updateUsuario()
    {
        return view("usuarios.update");      
    }
 
    public function list($tipo = 'todos')
    {
        if($tipo == 'todos'){
             $User = User::join('role_user','role_user.user_id','=','users.id')
                ->join('roles','roles.id','=','role_user.role_id')
                ->select('users.nome','users.data_cadastro','roles.name','users.status', 'users.id')
                ->orderBy('users.created_at', 'desc')
            ->get();    
        }else if($tipo == 'ativos'){
            $User = User::join('role_user','role_user.user_id','=','users.id')
                ->join('roles','roles.id','=','role_user.role_id')
                ->where('roles.name','usuario_ativo')
                ->select('users.nome','users.data_cadastro','roles.name','users.status', 'users.id')
                ->orderBy('users.created_at', 'desc')
            ->get();  
        }else if($tipo == 'pendentes'){
            $User = User::join('role_user','role_user.user_id','=','users.id')
                ->join('roles','roles.id','=','role_user.role_id')
                ->where('roles.name','usuario_pendente')
                ->select('users.nome','users.data_cadastro','roles.name','users.status', 'users.id')
                ->orderBy('users.created_at', 'desc')
            ->get();  
        }else if($tipo == 'pagos'){
            $User = User::join('role_user','role_user.user_id','=','users.id')
                ->join('roles','roles.id','=','role_user.role_id')
                ->where('users.ativo_mes','>',0)
                ->select('users.nome','users.data_cadastro','roles.name','users.status', 'users.id')
                ->orderBy('users.created_at', 'desc')
            ->get();  
        }else if($tipo == 'inativos'){
            $User = User::leftjoin('role_user','role_user.user_id','=','users.id')
                ->leftjoin('roles','roles.id','=','role_user.role_id')
                ->where('users.status','Inativo')
                ->select('users.nome','users.data_cadastro','roles.name','users.status', 'users.id')
                ->orderBy('users.created_at', 'desc')
            ->get();  
        }else if($tipo == 'bloqueados'){
            $User = User::leftjoin('role_user','role_user.user_id','=','users.id')
                ->leftjoin('roles','roles.id','=','role_user.role_id')
                ->where('users.status','Bloqueado')
                ->select('users.nome','users.data_cadastro','roles.name','users.status', 'users.id')
                ->orderBy('users.created_at', 'desc')
            ->get();  
        }
        
            
        return DataTables::of($User)
            ->editColumn('acao', function ($User){
                return $this->setBtns($User);
            })->escapeColumns([0])
            ->make(true);
    }


    private function setBtns(User $users){
        $dados = "data-id_del='$users->id' 
        data-id='$users->id' 
        data-nome='$users->nome' 
        data-user='$users->user' 
        data-pacote='$users->pacote' 
        data-ativo_mes='$users->ativo_mes' 
        data-qualificacao='$users->qualificacao' 
        data-avatar='$users->avatar' 
        data-data_nascimento='$users->data_nascimento' 
        data-cpf='$users->cpf' 
        data-cnpj='$users->cpf' 
        data-email='$users->email' 
        data-telefone='$users->telefone' 
        data-rua='$users->rua' 

        data-complemento='$users->complemento' 
        data-rua='$users->rua' 
        data-numero='$users->numero' 
        data-bairro='$users->bairro' 
        data-cidade='$users->cidade' 
        data-estado='$users->estado' 
        data-cep='$users->cep' 
        data-sexo='$users->sexo' 
        data-data_cadastro='$users->data_cadastro' 
        data-data_ativacao='$users->data_ativacao' 
        data-banco='$users->banco' 
        data-agencia='$users->agencia' 
        data-agenfica_digito='$users->agenfica_digito' 
        data-status='$users->status'
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";
        
        $btnUser = "<a  class='btn btn-primary btn-sm'  href='../login-conta-user/$users->id' title='Entrar'> <i class='fa fa-share-square'></i></a> ";

        return $btnVer.$btnEditar.$btnUser;
    }


    public function buscaAcima($patrocinador,$lado){
        $filhos = User::where('acima',$patrocinador)
                ->where('derramamento', $lado)
                ->first();            
        return $filhos;
    }

    public function addFilhos($patrocinador,$lado){

        $x = $this->buscaAcima($patrocinador, $lado);
       
        if ($x != null)
        {  
            $this->addFilhos($x->user, $lado);
        }else{
            $this->user_lado = $patrocinador;
            return $patrocinador;
        }
    }
  
    public function store(Request $request)
    {  

        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );
        $request->user = str_replace(' ','', $request->user);
        $request->email = str_replace(' ','', $request->email);
        
        $n = is_numeric($request->user[0]);
        
        if(isset($request->user[1])){
            $l = is_numeric($request->user[1]);

            if($n == true && $l == true){
               return response()->json('O username não pode iniciar com numeros',$responsecode, $header, JSON_UNESCAPED_UNICODE);
            }   
        }else{
            return response()->json('O username não pode ter só um caracter',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }
          
        
        $patrocinador = User::where('user',$request->patrocinador)->first();

        if($patrocinador == null){
            return response()->json('Indicador não encontrado',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        $user_acima = $this->buscaAcima($request->patrocinador, $patrocinador->lado_rede);

        if(isset($user_acima->acima)){
            $this->addFilhos($user_acima->user,$user_acima->lado_rede);
        }else{
            $this->user_lado = $request->patrocinador;
        }

        $user_acima = $this->user_lado;

    	
        $rules = array(
            'nome' => 'required',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'data_nascimento' => 'required',
            'cpf' => 'required',
            'rg' => 'required',
            'email' => 'required',
            'telefone' => 'required',
            'rua' => 'required',
            'complemento' => 'required',
            'rua' => 'required',
            'numero' => 'required',
            'complemento' => 'required',
            'cidade' => 'required',
            'estado' => 'required',
            'cep' => 'required',
            'patrocinador' => 'required',
            'bairro' => 'required',
            'termo' => 'required',
            
        );
        $attributeNames = array(
            'nome' => 'Nome',
            'email'    => 'E-mail',
            'password' => 'Senha',
            'passwordConfirme' => 'Confirmação da senha',
            'data_nascimento' => 'Data de nascimento',
            'cpf' => 'CPF',
            'rg' => 'RG',
            'email' => 'E-mail',
            'telefone' => 'Telefone',
            'rua' => 'Rua',
            'complemento' => 'Complemento',
            'bairro' => 'Bairro',
            'rua' => 'Rua',
            'numero' => 'Número',
            'cidade' => 'Cidade',
            'estado' => 'Estado',
            'cep' => 'Cep',
            'patrocinador' => 'Patrocinador',
            'termo' => 'termo',

        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){  
                return Response::json(array('code'=> 404,'errors' => $validator->getMessageBag()->toArray()), 404);
        }else {

            $imagem_padrao = ImagemPadrao::first();
 
			$data_cadastro = date("Y-m-d"); 
            $user = new User();
            if($imagem_padrao){
                $user->avatar = $imagem_padrao->imagem;
            }
            $user->nome = $request->nome;
            $user->user = $request->user;
            if($request->ativo_mes){
                $user->ativo_mes = $request->ativo_mes;
            }
            $user->data_nascimento = $request->data_nascimento;
            $user->cpf = $request->cpf;
            $user->cpf = $request->cnpj;
            $user->rg = $request->rg;
            $user->email = $request->email;
            $user->telefone = $request->telefone;
            $user->telefone2 = $request->telefone2;
            $user->pis = $request->pis;
            $user->orgao_expedidor = $request->orgao_expedidor;
            $user->rua = $request->rua;
            $user->complemento = $request->complemento;
            $user->rua = $request->rua;
            $user->numero = $request->numero;
            $user->bairro = $request->bairro;
            $user->cidade = $request->cidade;
            $user->estado = $request->estado;
            $user->cep = $request->cep;
            $user->sexo = $request->sexo; 
            $user->data_cadastro = $data_cadastro;
            $user->data_ativacao = $request->data_ativacao;
            $user->banco = $request->banco;
            $user->agencia = $request->agencia;
            $user->agencia_digito = $request->agencia_digito;
            $user->lado_rede = $patrocinador->lado_rede;
            $user->conta_tipo = $request->conta_tipo;
            $user->patrocinador = $request->patrocinador;
            $user->acima = $user_acima;
            $user->derramamento = $patrocinador->lado_rede;
            $user->password = bcrypt($request->password);
            $user->data_cadastro = $data_cadastro;
            $user->save();

            $role_user = Role::where('name', 'usuario_pendente')->first();
            $user->roles()->sync($role_user); 

            $arguments = array(
                'VINDI_API_KEY' => 'jVz1nd9v1ou4ZN9JmYSSkk5x35SZxCajq60miZ_TZ3Q', //SANDBOX
                //'VINDI_API_KEY' => 'KQdncbJ3446dcikn83fp_Mi6zNK1KAKmrAOnmL97Pyo',
                'VINDI_API_URI' => 'https://sandbox-app.vindi.com.br/api/v1/customers'
            );

            // Instancia o serviço de Customers (Clientes) com o array contendo VINDI_API_KEY e VINDI_API_URI
            $customerService = new Vindi\Customer($arguments);

            $customer = $customerService->create([
                'name'  => $user->nome,
                'email' => $user->email,
                'code'=> $user->id,
            ]);
        
            return response()->json($user);
        }
    }

    public function buscaAcimaPagar($acima){
        $acima = User::where('user',$acima)
                ->first();            
        return $acima;
    }

     public function addAcima($acima){

        $x = $this->buscaAcimaPagar($acima);
       
        if ($x != null)
        {  
            $this->addAcima($x->acima);
            array_push($this->usuario_acima, ["user"=> $x->user, "lado"=> $x->derramamento]); 
              
        }else{
            return;
        }
    }  

    public function buscaAcimaPai($acima){
        $acima = User::where('user',$acima)
                ->first();            
        return $acima;
    }

    public function countPatrocinador($nome){ 
        $usuario = User::where('user',$nome)
            ->first();    
        if(isset($usuario->linha_patrocinada )){
            if($usuario->linha_patrocinada < 3){
            return $this->countPatrocinador($usuario->patrocinador);
        }else{
            
            $this->patrocinador_encontado_2 = $nome;
            return $nome;
        }
        }   
        
    }

    public function buscaPatrocinador($patrocinador){
        $patrocinador = User::where('user',$patrocinador)
                ->first();            
        return $patrocinador;
    }

    public function addPatrocinador($acima){

        $x = $this->buscaPatrocinador($acima);

        if($x != null){
            if ($x->ativo_mes < 1)
            {  
                $this->addPatrocinador($x->patrocinador);
            }else{
                $this->patrocinador_encontado = $x;
                return $x->user;
            }
        }
    }

    public function addAcimaPai($acima){

        $x = $this->buscaAcimaPai($acima);
       
        if ($x != null)
        {  
            $this->addAcimaPai($x->patrocinador);
            array_push($this->usuario_acima_pai, $x->user); 
              
        }else{
            return;
        }
    }

    public function pagar(Request $request)
    {

        $responsecode = 404;   
        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
        );


        if(!Hash::check($request->senha_transacao, Auth::user()->senha_transacao)){
           return response()->json('Senha financeira incorreta',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }

        $pacote = Pacote::LEFTJOIN('transacao_tipos','transacao_tipos.id','=','pacotes.fk_transacao_tipo')
        ->select('transacao_tipos.acao','pacotes.valor_pacote','transacao_tipos.id as id_transacao','pacotes.id as id_pacote', 'pacotes.bonus_binario', 'pacotes.comissao_unilevel' , 'pacotes.ponto_unilevel','pacotes.inicio_rapido_valor','pacotes.ativo_mes')
        ->where('pacotes.id',$request->pacote)->first();

        $saldo_user = SaldoUser::where('fk_user',Auth::user()->id)->first();

        if($saldo_user->saldo <  $pacote->valor_pacote){
            return response()->json('Você não tem saldo suficente para pagar o pacote',$responsecode, $header, JSON_UNESCAPED_UNICODE);
        }


        $usuario_um = User::where('id',$request->fk_adesao_usuario)->first();

        $qtd = User::where('fk_pacote','>', 0)
            ->where('patrocinador',$usuario_um->patrocinador)
            ->count();   

        $patrocinador_um = User::where('user',$usuario_um->patrocinador)->first();


        $linha1 = User::where('patrocinador',$patrocinador_um->user)
            ->where('linha_patrocinada', 1)
            ->first();

        $linha2 = User::where('patrocinador',$patrocinador_um->user)
            ->where('linha_patrocinada', 2)
            ->first();

            
        if($linha1 != null && $linha2 != null && $patrocinador_um->infinite == 0){
            $patrocinador_um->infinite = $patrocinador_um->infinite +1;
            $patrocinador_um->save();
        }


        $saldo_user->saldo = $saldo_user->saldo - $pacote->valor_pacote;
        $saldo_user->save();

        $usuario = User::where('id',$request->fk_adesao_usuario)->first();
        $usuario->ativo_mes = $usuario->ativo_mes + $pacote->ativo_mes;

        $usuario->fk_pacote = $pacote->id_pacote;
        $usuario->linha_patrocinada = $qtd + 1;
        $usuario->ponto_unilevel = $usuario->ponto_unilevel + $pacote->ponto_unilevel;
        $usuario->save();

        if($usuario->linha_patrocinada > 2){

            $patrocinador_um = User::where('user',$usuario->patrocinador)->first();

            if($patrocinador_um->ativo_mes > 0){

                $bonus_infinite = Infinite::where('numero', $patrocinador_um->infinite)->first();

                if($bonus_infinite != null ){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador_um->id)->first();
                        if($saldo_patrocinador == null){
                            
                            $saldo_patrocinador_new = New SaldoUser();
                            $saldo_patrocinador_new->saldo = ($pacote->valor_pacote)  * ($bonus_infinite->valor/100);
                            $saldo_patrocinador_new->fk_user = $patrocinador_um->id;
                            $saldo_patrocinador_new->save(); 
                        }else{
                            $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($pacote->valor_pacote)  * ($bonus_infinite->valor/100);
                            $saldo_patrocinador->save();
                        }

                    $transacao_tipo_patrocinador = TransacaoTipo::where('nome','Bônus infinite')->first();
                    
                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = ($pacote->valor_pacote)  * ($bonus_infinite->valor/100);
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador_um->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario->id;
                    $patrocinador_transacao->save();
                }
            }
        }else{

            $this->countPatrocinador($patrocinador_um->patrocinador);

            if($this->patrocinador_encontado_2){
                $user_encontrado = User::where('user',$this->patrocinador_encontado_2)->first();

                $user_patrocinador = User::where('user',$user_encontrado->patrocinador)->first();

                //dd($user_patrocinador);
                if($user_patrocinador->ativo_mes > 0){

                    $saldo_patrocinador = SaldoUser::where('fk_user',$user_patrocinador->id)->first();
                    if($saldo_patrocinador == null){
                        
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = 10;
                        $saldo_patrocinador_new->fk_user = $user_patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + 10;
                        $saldo_patrocinador->save();
                    }


                    $transacao_tipo_patrocinador = TransacaoTipo::where('nome','Bônus infinite')->first();
                    
                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = 10;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $user_patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario->id;
                    $patrocinador_transacao->save();
                }
            }
        }



        $role_user = Role::where('name', 'usuario_ativo')->first();
        $usuario->roles()->sync($role_user); 

        $usuario_acima = $this->buscaAcimaPagar($usuario->acima);

        if(isset($usuario_acima)){
            $this->addAcima($usuario_acima->acima);

            array_push($this->usuario_acima, ["user"=>  $usuario_acima->user, "lado"=> $usuario_acima->derramamento]);

            array_push($this->usuario_acima, ["user"=>  $usuario->user, "lado"=> $usuario->derramamento]);
        }

        for ($i=0; $i < count($this->usuario_acima); $i++) { 
           
            if($this->usuario_acima[$i]['user'] != $usuario->user){            
                $user_binario = User::where('user', $this->usuario_acima[$i]['user'])->first();

                    if($this->usuario_acima[$i+1]['lado'] == 2){
                        $user_binario->ponto_esquerda =  $user_binario->ponto_esquerda + $pacote->bonus_binario; 
                        $user_binario->total_ponto_es = $user_binario->total_ponto_es + $pacote->bonus_binario; 
                    }else{
                        $user_binario->ponto_direita =  $user_binario->ponto_direita + $pacote->bonus_binario; 
                        $user_binario->total_ponto_di = $user_binario->total_ponto_di + $pacote->bonus_binario; 
                    } 

                    $user_binario->save();
                }      
        }

        $ponto_nivel_1 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 1')
        ->first();

        $ponto_nivel_2 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 2')
        ->first();

        $ponto_nivel_3 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 3')
        ->first();

        $ponto_nivel_4 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 4')
        ->first();
        
        $ponto_nivel_5 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 5')
        ->first();

        $ponto_nivel_6 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 6')
        ->first();
        
        $ponto_nivel_7 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 7')
        ->first();
        
        $ponto_nivel_8 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 8')
        ->first();

        $ponto_nivel_9 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 9')
        ->first();

        $ponto_nivel_10 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 10')
        ->first();

        $ponto_nivel_11 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 11')
        ->first();

        $ponto_nivel_12 = PontoUnilevel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 12')
        ->first();



        $pacote_nivel_1 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 1')
        ->first();

        $pacote_nivel_2 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 2')
        ->first();

        $pacote_nivel_3 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 3')
        ->first();

        $pacote_nivel_4 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 4')
        ->first();
        
        $pacote_nivel_5 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 5')
        ->first();

        $pacote_nivel_6 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 6')
        ->first();
        
        $pacote_nivel_7 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 7')
        ->first();
        
        $pacote_nivel_8 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 8')
        ->first();

        $pacote_nivel_9 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 9')
        ->first();

        $pacote_nivel_10 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 10')
        ->first();

        $pacote_nivel_11 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 11')
        ->first();

        $pacote_nivel_12 = PacoteNivel::where('fk_pacote', $pacote->id_pacote)
        ->where('nivel','Nivel 12')
        ->first();        
    
        $transacao_tipo = TransacaoTipo::where('nome','Compras de pacote')->first();       

        $transacao = New Transacao();
        $transacao->valor = $pacote->valor_pacote;
        $transacao->fk_transacao_tipo = $transacao_tipo->id;
        $transacao->fk_membro = Auth::user()->id;
        $transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
        $transacao->save(); 

        $user = User::where('id',$request->fk_adesao_usuario)->first();

        $transacao_tipo_patrocinador = TransacaoTipo::where('nome','Bônus de indicação')->first();

       
        $inicio_rapido = TransacaoTipo::where('nome','Bônus de inicio rapido')->first();

        $patrocinador = User::where('user',$user->patrocinador)->first();
        $primeiro_patrocinador = User::where('user',$user->patrocinador)->first();

        if($patrocinador != null){
            if($patrocinador->ativo_mes < 1){
        
                $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);
                $patrocinador = $this->patrocinador_encontado;
            }else{
                $patrocinador = User::where('user',$patrocinador->user)->first();
            }
        }

        if($patrocinador != null){
           
            if($patrocinador != null && $pacote != null && $patrocinador->inicio_rapido > 0) {
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();
                if($saldo_patrocinador == null){
                    
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote->inicio_rapido_valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote->inicio_rapido_valor;
                    $saldo_patrocinador->save();
                }
               
                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote->inicio_rapido_valor;
                $patrocinador_transacao->fk_transacao_tipo = $inicio_rapido->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
                
            }
        }


        //PRIMEIRO NIVEL
         if($patrocinador != null){

            if($patrocinador != null && $pacote_nivel_1 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();
                
                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_1->valor;
                    $saldo_patrocinador_new->saldo =  $saldo_patrocinador_new->saldo + $pacote->comissao_unilevel;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_1->valor;
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote->comissao_unilevel;
                    $saldo_patrocinador->save();
                }
               
                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_1->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();

                if($pacote->comissao_unilevel > 0){
                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $pacote->comissao_unilevel;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                    $patrocinador_transacao->save();

                }

            }else if($patrocinador != null && $pacote_nivel_1 == null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo =  $saldo_patrocinador_new->saldo + $pacote->comissao_unilevel;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote->comissao_unilevel;
                    $saldo_patrocinador->save();
                }
               
                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_1->valor;;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();

                if($pacote->comissao_unilevel > 0){
                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $pacote->comissao_unilevel;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                    $patrocinador_transacao->save();

                }
            }
        }

 
        if($patrocinador != null){
            if($patrocinador->ativo_mes < 1){
        
                $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                $patrocinador = $this->patrocinador_encontado;
            }else{
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
            }
        }

        //SEGUNDO NIVEL
        if($patrocinador != null){ 
            
            if($patrocinador != null && $pacote_nivel_2 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();
                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_2->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_2->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_2->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
            
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //TERCEIRO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $pacote_nivel_3 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_3->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_3->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_3->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

       

        if($patrocinador != null){ $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //QUARTO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $pacote_nivel_4 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_4->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_4->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_4->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }


        if($patrocinador != null){        

            $patrocinador = User::where('user',$patrocinador->patrocinador)->first();

            if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //QUINTO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $pacote_nivel_5 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_5->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_5->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_5->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

         //SEXTO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $pacote_nivel_6 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_6->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_6->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_6->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }
  
         //SETIMO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $pacote_nivel_7 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_7->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_7->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_7->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //OITAVO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $pacote_nivel_8 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_8->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_8->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_8->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //NONO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $pacote_nivel_9 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_9->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_9->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_9->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }


        if($patrocinador != null){        $patrocinador = User::where('user',$patrocinador->patrocinador)->first();

             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //DECIMO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $pacote_nivel_10 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_10->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_10->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_10->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

      
        if($patrocinador != null){  $patrocinador = User::where('user',$patrocinador->patrocinador)->first();

             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //DECIMO PRIMEIRO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $pacote_nivel_11 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_11->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_11->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_11->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }   



        if($patrocinador != null){        $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //DECIMO SEGUNDO NIVEL
        if($patrocinador != null){  
             
       
            if($patrocinador != null && $pacote_nivel_12 != null && $patrocinador->ativo_mes > 0){
                $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                if($saldo_patrocinador == null){
                    $saldo_patrocinador_new = New SaldoUser();
                    $saldo_patrocinador_new->saldo = $pacote_nivel_12->valor;
                    $saldo_patrocinador_new->fk_user = $patrocinador->id;
                    $saldo_patrocinador_new->save(); 
                }else{
                    $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + $pacote_nivel_12->valor;
                    $saldo_patrocinador->save();
                }

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_12->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }



        //FIDELIDADE

        $patrocinador = User::where('user',$user->patrocinador)->first();
        $transacao_tipo_patrocinador = TransacaoTipo::where('nome','Pontos de fidelidade')->first();

        if($patrocinador != null){
            if($patrocinador->ativo_mes < 1){
        
                $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                $patrocinador = $this->patrocinador_encontado;
            }else{
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
            }
        }
       
        //SEGUNDO NIVEL
         if($patrocinador != null){
            if($patrocinador != null && $ponto_nivel_1 != null && $patrocinador->ativo_mes > 0){
              
               $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_1->valor;
                $patrocinador->save(); 
                
                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $pacote_nivel_1->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }
        
        //SEGUNDO NIVEL
        if($patrocinador != null){ 
            
            
            if($patrocinador != null && $ponto_nivel_2 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_2->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_2->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

       

        if($patrocinador != null){ $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //TERCEIRO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $ponto_nivel_3 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_3->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_3->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //QUARTO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $ponto_nivel_4 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_4->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_4->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

       

        if($patrocinador != null){ $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //QUINTO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $ponto_nivel_5 != null){
               $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_5->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_5->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

       

        if($patrocinador != null){ $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

         //SEXTO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $ponto_nivel_6 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_6->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_6->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

       

        if($patrocinador != null){ $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }
  
         //SETIMO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $ponto_nivel_7 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_7->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_7->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //OITAVO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $ponto_nivel_8 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_8->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_8->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //NONO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $ponto_nivel_9 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_9->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_9->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

        

        if($patrocinador != null){$patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //DECIMO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $ponto_nivel_10 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_10->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_10->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

       

        if($patrocinador != null){ $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //DECIMO PRIMEIRO NIVEL
        if($patrocinador != null){ 
            
           
            if($patrocinador != null && $ponto_nivel_11 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_11->valor;
                $patrocinador->save(); 

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_11->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }   


        if($patrocinador != null){
        $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
             if($patrocinador != null){      

                if($patrocinador->ativo_mes < 1){
            
                    $patrocinador = $this->addPatrocinador($patrocinador->patrocinador);

                    $patrocinador = $this->patrocinador_encontado;
                }else{
                    $patrocinador = User::where('user',$patrocinador->user)->first();
                }
            }
        }

        //DECIMO SEGUNDO NIVEL
        if($patrocinador != null){  
             
       
            if($patrocinador != null && $ponto_nivel_12 != null && $patrocinador->ativo_mes > 0){
                $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + $ponto_nivel_12->valor;
                $patrocinador->save();

                $patrocinador_transacao = New Transacao();
                $patrocinador_transacao->valor = $ponto_nivel_12->valor;
                $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                $patrocinador_transacao->fk_membro = $patrocinador->id;
                $patrocinador_transacao->fk_adesao_usuario = $request->fk_adesao_usuario;
                $patrocinador_transacao->save();
            }
        }

    }

    public function update(Request $request)
    {
        
        $user = User::find($request->id);
        $user->nome = $request->nome;
        $user->data_nascimento = $request->data_nascimento;
        $user->cpf = $request->cpf;
        $user->cpf = $request->cnpj;
        $user->telefone = $request->telefone;
        $user->rua = $request->rua;
        $user->complemento = $request->complemento;
        $user->rua = $request->rua;
        $user->numero = $request->numero;
        $user->bairro = $request->bairro;
        $user->cidade = $request->cidade;
        $user->estado = $request->estado;
        $user->cep = $request->cep;
        $user->status = $request->status;
        //$user->password = bcrypt($request->password);
        $user->sexo = $request->sexo;
        $user->save();
      
        return response()->json($user);
    }

    public function updatePostUsuario(Request $request)
    {
        
        $user = User::find($request->id);
        $user->nome = $request->nome;
        $user->data_nascimento = $request->data_nascimento;
        $user->rg = $request->rg;
        $user->cpf = $request->cpf;
        $user->pis = $request->pis;
        $user->orgao_expedidor = $request->orgao_expedidor;
        $user->telefone = $request->telefone;
        $user->telefone2 = $request->telefone2;
        $user->rua = $request->rua;
        $user->complemento = $request->complemento;
        $user->rua = $request->rua;
        $user->numero = $request->numero;
        $user->bairro = $request->bairro;
        $user->cidade = $request->cidade;
        $user->estado = $request->estado;
        $user->cep = $request->cep;
        $user->save();
      
        return response()->json($user);
    }
    
    public function updateBanco(Request $request)
    {
        
        $user = User::find($request->id);
        
        $user->banco = $request->banco;
        $user->agencia = $request->agencia;
        $user->conta_digito = $request->conta_digito;
        $user->agencia_digito = $request->agencia_digito;
        $user->conta = $request->conta;
        $user->conta_tipo = $request->conta_tipo;
        $user->save();
      
        return response()->json($user);
    }

    public function destroy(Request $request)
    {
        $user = User::destroy($request->id_del);

        return response()->json($user);
    }
}
