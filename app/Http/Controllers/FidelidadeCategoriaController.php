<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FidelidadeCategoria;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;

class FidelidadeCategoriaController extends Controller
{
    public function index()
    {
        return view('fidelidade_categoria.index');
    }

 
    public function list()
    {
        $produto = FidelidadeCategoria::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($produto)
            ->editColumn('acoes', function ($FidelidadeCategoria){
                return $this->setBtns($FidelidadeCategoria);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(FidelidadeCategoria $FidelidadeCategorias){
        $dados = "data-id_del='$FidelidadeCategorias->id' 
        data-id='$FidelidadeCategorias->id' 
        data-nome='$FidelidadeCategorias->nome' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'nome' => 'required'            
        );
        $attributeNames = array(
            'nome' => 'Nome'
        );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $FidelidadeCategoria = new FidelidadeCategoria();
            $FidelidadeCategoria->nome = $request->nome;
            $FidelidadeCategoria->save();

            return response()->json($FidelidadeCategoria);
        }
    }

    public function update(Request $request)
    {
        
        $FidelidadeCategoria = FidelidadeCategoria::find($request->id);
        $FidelidadeCategoria->nome = $request->nome;
        $FidelidadeCategoria->save();
      
        return response()->json($FidelidadeCategoria);
    }

    public function destroy(Request $request)
    {
        $FidelidadeCategoria = FidelidadeCategoria::destroy($request->id_del);

        return response()->json($FidelidadeCategoria);
    }
}
