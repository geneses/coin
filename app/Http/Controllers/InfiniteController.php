<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\Infinite;

class InfiniteController extends Controller
{

    public function index()
    {

        return view('infinite.index');
    }

    public function list()
    {
        $Infinite = Infinite::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($Infinite)
            ->editColumn('acoes', function ($Infinite){
                return $this->setBtns($Infinite);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(Infinite $Infinites){
        $dados = "data-id_del='$Infinites->id' 
        data-id='$Infinites->id' 
        data-nome='$Infinites->nome' 
        data-numero='$Infinites->numero' 
        data-valor='$Infinites->valor' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'nome' => 'required',
            'numero' => 'required',
            'valor' => 'required',
        );
        $attributeNames = array(
            'nome' => 'Nome',
            'numero' => 'Pontuação',
            'valor' => 'Premiação',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $Infinite = new Infinite();
            $Infinite->nome = $request->nome;
            $Infinite->numero = $request->numero;
            $Infinite->valor = $request->valor;
            $Infinite->save();

            return response()->json($Infinite);
        }
    }

    public function update(Request $request)
    {
        
        $Infinite = Infinite::find($request->id);
        $Infinite->nome = $request->nome;
        $Infinite->numero = $request->numero;
        $Infinite->valor = $request->valor;
        $Infinite->save();
      
        return response()->json($Infinite);
    }

    public function destroy(Request $request)
    {
        $Infinite = Infinite::destroy($request->id_del);

        return response()->json($Infinite);
    }
}
