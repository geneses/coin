<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\MaximoMinimo;

class MaximoController extends Controller
{
     public function index()
    {
        return view('usuarios.maximo_minimo');
    }

 
    public function list()
    {
        $produto = MaximoMinimo::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($produto)
            ->editColumn('acoes', function ($produto){
                return $this->setBtns($produto);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(MaximoMinimo $MaximoMinimos){
        $dados = "data-id_del='$MaximoMinimos->id' 
        data-id='$MaximoMinimos->id' 
        data-minimo='$MaximoMinimos->minimo' 
        data-maximo='$MaximoMinimos->maximo' 
        data-tipo='$MaximoMinimos->tipo' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        //$btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'minimo' => 'required',          
            'maximo' => 'required'            
        );
        $attributeNames = array(
            'minimo' => 'minimo',
            'maximo' => 'maximo'
        );

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $MaximoMinimo = new MaximoMinimo();
            $MaximoMinimo->minimo = $request->minimo;
	        $MaximoMinimo->maximo = $request->maximo;
	        $MaximoMinimo->tipo = $request->tipo;
            $MaximoMinimo->save();

            return response()->json($MaximoMinimo);
        }
    }

    public function update(Request $request)
    {
        
        $MaximoMinimo = MaximoMinimo::find($request->id);
        $MaximoMinimo->minimo = $request->minimo;
        $MaximoMinimo->maximo = $request->maximo;
        $MaximoMinimo->tipo = $request->tipo;
        $MaximoMinimo->save();
      
        return response()->json($MaximoMinimo);
    }

    public function destroy(Request $request)
    {
        $MaximoMinimo = MaximoMinimo::destroy($request->id_del);

        return response()->json($MaximoMinimo);
    }
}
