<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Auth;
use App\TransacaoTipo;

class TransacaoController extends Controller
{
     public function index()
    {
        return view('transacao.index');
    }

 
    public function list()
    {
        $produto = TransacaoTipo::orderBy('created_at', 'desc')
        ->get();
            
        return DataTables::of($produto)
            ->editColumn('acoes', function ($transacaoTipo){
                return $this->setBtns($transacaoTipo);
            })->escapeColumns([0])
            ->make(true);
    }

    private function setBtns(TransacaoTipo $transacaoTipos){
        $dados = "data-id_del='$transacaoTipos->id' 
        data-id='$transacaoTipos->id' 
        data-nome='$transacaoTipos->nome' 
        data-acao='$transacaoTipos->acao' 
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public function store(Request $request)
    {  
        $rules = array(
            'nome' => 'required',
            'acao' => 'required'

            
        );
        $attributeNames = array(
            'nome' => 'Nome',
            'acao' => 'Ação'
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $transacao = new TransacaoTipo();
            $transacao->nome = $request->nome;
            $transacao->acao = $request->acao;
            $transacao->save();

            return response()->json($transacao);
        }
    }

    public function update(Request $request)
    {
        
        $transacao = TransacaoTipo::find($request->id);
        $transacao->nome = $request->nome;
        $transacao->acao = $request->acao;
        $transacao->save();
      
        return response()->json($transacao);
    }

    public function destroy(Request $request)
    {
        $transacao = Transacao::destroy($request->id_del);

        return response()->json($transacao);
    }
}
