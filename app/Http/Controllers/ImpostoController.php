<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use DataTables;
use App\Imposto;

class ImpostoController extends Controller
{
    public function index(){
        return view('imposto.index');
    }
  
    public function list(){
        $imposto = Imposto::all();
            
        return DataTables::of($imposto)
            ->editColumn('acao', function ($imposto){
                return $this->setBtns($imposto);
            })->escapeColumns([0])
            ->make(true);
    }
    private function setBtns(Imposto $imposto){
        $dados = "data-id_del='$imposto->id' 
        data-id='$imposto->id' 
        data-titulo='$imposto->titulo' 
        data-porcentagem='$imposto->porcentagem'
        ";
        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar Imposto' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar Imposto' $dados><i class='fa fa-trash'></i></a>";

        return $btnEditar.$btnDeletar;
    }
    public function store(Request $request){  
         
        $rules = array(
            'titulo' => 'required',
            'porcentagem' => 'required'
            
        );
        $attributeNames = array(
            'titulo' => 'titulo',
            'porcentagem' => 'porcentagem'
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $imposto = Imposto::create($request->all());
        
            return response()->json($imposto);
        }
    }
    public function update(Request $request){  
         
        $rules = array(
            'titulo' => 'required',
            'porcentagem' => 'required'
            
        );
        $attributeNames = array(
            'titulo' => 'titulo',
            'porcentagem' => 'porcentagem'
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $imposto = Imposto::find($request->id);
            $imposto->titulo = $request->titulo;
            $imposto->porcentagem = $request->porcentagem;
            $imposto->save();
        
            return response()->json($imposto);
        }
    }
    public function destroy(Request $request)
    {
        $imposto = Imposto::destroy($request->id_del);

        return response()->json($imposto);
    }

}

