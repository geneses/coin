<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use DataTables;
use App\Parcelamento;

class ParcelamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('parcelamento.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $parcelamento = Parcelamento::all();
            
        return DataTables::of($parcelamento)
            ->editColumn('acao', function ($parcelamento){
                return $this->setBtns($parcelamento);
            })->editColumn('porcentagem', function ($parcelamento){
                return $parcelamento->porcentagem.' %';
            })->escapeColumns([0])
            ->make(true);
    }
    private function setBtns(Parcelamento $parcelamento){
        $dados = "data-id_del='$parcelamento->id' 
        data-id='$parcelamento->id' 
        data-porcentagem='$parcelamento->porcentagem' 
        data-parcelas='$parcelamento->parcelas'";
        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar parcela' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar parcela' $dados><i class='fa fa-trash'></i></a>";

        return $btnEditar.$btnDeletar;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $rules = array(
            'parcelas' => 'required | Numeric | min:0',
            'porcentagem' => 'required | Numeric | min:0'

            
        );
        $attributeNames = array(
            'parcelas' => 'Parcelas',
            'porcentagem' => 'Porcentagem',

        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {
            
            $Parcelamento = new Parcelamento();
            $Parcelamento->porcentagem = $request->porcentagem;
            $Parcelamento->parcelas = $request->parcelas;            
            $Parcelamento->save();
            return response()->json($Parcelamento);
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = array(
            'parcelas' => 'required',
            'porcentagem' => 'required'

            
        );
        $attributeNames = array(
            'parcelas' => 'Parcelas',
            'porcentagem' => 'Porcentagem',

        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

            $Parcelamento = Parcelamento::find($request->id);
            $Parcelamento->parcelas = $request->parcelas;
            $Parcelamento->porcentagem = $request->porcentagem;
            $Parcelamento->save();
            return response()->json($Parcelamento);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Parcelamento = Parcelamento::destroy($request->id_del);

        return response()->json($Parcelamento);
    }
}
