<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use DataTables;
use DB;
use Vindi;
use Auth;
use App\Produto;
use App\UnilevelProduto;
use App\FidelidadeProduto;
use App\User;
use App\SaldoUser;
use App\TransacaoTipo;
use App\Transacao;
use App\Carrinho;
use App\UserCompra;
use App\Categoria;

class ProdutoController extends Controller
{

    public $usuario_acima = [];

     public function index()
    {
        
        $categorias = Categoria::orderBy('created_at')->get();
        return view('produtos.index',compact('categorias'));
    }

    public function plataforma(){
        return view('produtos.plataforma');
    }

     public function buscaAcimaPagar($acima){
        $acima = User::where('user',$acima)
                ->first();            
        return $acima;
    }
    public function gerar_extrato(){
        return view('produtos.extrato');
    }
    public function extrato_produto(Request $request){
        $transacaos = [];
        $inicio = $request->inicio;
        /*
        if($request->fim == null){
            $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->where('transacao_tipos.nome', 'Compras de pacote')
                ->whereDate('transacaos.created_at', $inicio)
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem', 'users.user')
                ->get();
        }else{
            $fim = date('Y/m/d', strtotime($request->fim. ' + 1 days'));
            
            $transacoes = User::JOIN('transacaos','transacaos.fk_membro','=','users.id')
                ->JOIN('transacao_tipos','transacao_tipos.id','=','transacaos.fk_transacao_tipo')
                ->JOIN('users as u2','u2.id','=','transacaos.fk_adesao_usuario')
                ->where('transacao_tipos.nome', 'Compras de pacote')
                ->whereBetween('transacaos.created_at', [$inicio, $fim])
                ->SELECT('transacaos.valor','transacao_tipos.nome','transacao_tipos.acao','transacaos.created_at','u2.user as origem', 'users.user')
                ->get();
        }

        return response()->json(['data'=> $transacoes]);*/
    }
     public function addAcima($acima){

        $x = $this->buscaAcimaPagar($acima);
       
        if ($x != null)
        {  
            $this->addAcima($x->acima);
            array_push($this->usuario_acima, ["user"=> $x->user, "lado"=> $x->derramamento]); 
              
        }else{
            return;
        }
    }

    //Gerar Bonificações
    public function buscarProduto($total = null, $usuario_id = null, $codigo_do_pedido = null)
    {
        $usuario_id = $usuario_id != null ? $usuario_id : Auth::user()->id;
        if(is_null($codigo_do_pedido)){
            $carrinho = Carrinho::JOIN('produtos','produtos.id','=','carrinhos.fk_produto')
                ->SELECT('produtos.nome','produtos.valor_compra','carrinhos.quantidade',
                'produtos.ponto_unilevel','produtos.comissao_produto','produtos.valor_v_usuario',
                'produtos.pontos_binario','produtos.ponto_unilevel','produtos.pontos_carreira','carrinhos.fk_produto','produtos.id')
                ->WHERE('carrinhos.fk_user', $usuario_id)
                ->GET();
        }else{
            $carrinho = Carrinho::JOIN('produtos','produtos.id','=','carrinhos.fk_produto')
                ->SELECT('produtos.nome','produtos.valor_compra','carrinhos.quantidade',
                'produtos.ponto_unilevel','produtos.comissao_produto','produtos.valor_v_usuario',
                'produtos.pontos_binario','produtos.ponto_unilevel','produtos.pontos_carreira','carrinhos.fk_produto','produtos.id')
                ->WHERE('carrinhos.fk_user', $usuario_id)
                ->WHERE('carrinhos.codigo_pedido',$codigo_do_pedido)
                ->GET();
            //Dando baixa no Estoque
            foreach($carrinho as $carrinhos){
                $produto_estoque = Produto::find($carrinhos->fk_produto);
                $produto_estoque->estoque = $produto_estoque->estoque - $carrinhos->quantidade;
                $produto_estoque->save();
            }
        }

        $soma_ponto_binario = 0;
        $ponto_binario =  Carrinho::where('codigo_pedido',$codigo_do_pedido)->get();
        foreach($ponto_binario as $ponto_binario2){
            $soma_ponto_binario += $ponto_binario2->produto->pontos_binario * $ponto_binario2->quantidade;
        }

        $soma_ponto_unilevel = 0;
        $ponto_unilevel =  Carrinho::where('codigo_pedido',$codigo_do_pedido)->get();
        foreach($ponto_unilevel as $ponto_unilevel2){
            $soma_ponto_unilevel += $ponto_unilevel2->produto->ponto_unilevel * $ponto_unilevel2->quantidade;
        }
        //dd($soma_ponto_binario);
            //Finalizar pedido
            if(!is_null($total)){
                $transacao_tipo = TransacaoTipo::where('nome','Compras de produto')->first();   
                $transacao = New Transacao();
                $transacao->valor = $total;
                $transacao->fk_transacao_tipo = $transacao_tipo->id;
                $transacao->fk_membro = Auth::user()->id;
                $transacao->fk_adesao_usuario = $usuario_id;
                $transacao->save(); 
            }
        $usuario_acima_barrar = false;
        $usuario_acima_barrar_ponto = false;
        foreach ($carrinho as $produto) {
            //validar mais uma vez para quando o saldo for menor que o produto
            //$saldo_usuario = SaldoUser::where('fk_user',$usuario_id)->first();
            // if($saldo_usuario != null){
            //     $saldo_usuario->saldo = $saldo_usuario->saldo - $produto->valor_v_usuario;
            //     $saldo_usuario->save();    
            // }else{
            //     return response()->json('usuario sem saldo');
            // }  

            //var_dump($produto);
            //die();
            $user_compra = new UserCompra();
            $user_compra->quantidade = $produto->quantidade;
            $user_compra->fk_produto = $produto->fk_produto;
            $user_compra->fk_user = $usuario_id;
            $user_compra->save();

            //$produto1 = Produto::where('id',1)->first();
            if(!$usuario_acima_barrar_ponto){
                $usuario = User::where('id', $usuario_id)->first();
                $usuario->ponto_unilevel = $usuario->ponto_unilevel + $soma_ponto_unilevel;
                $usuario->save();
                $usuario_acima_barrar_ponto = true;
            }

            $usuario_acima = $this->buscaAcimaPagar($usuario->acima);

            if(isset($usuario_acima)){
                $this->addAcima($usuario_acima->acima);
            }else{
                $usuario_acima = $usuario;
            }

            array_push($this->usuario_acima, ["user"=>  $usuario_acima->user, "lado"=> $usuario_acima->derramamento]);

            array_push($this->usuario_acima, ["user"=>  $usuario->user, "lado"=> $usuario->derramamento]);
            if(!$usuario_acima_barrar){//Pecorrer apenas uma vez
                for ($i=0; $i < count($this->usuario_acima); $i++) { 
                
                    if($this->usuario_acima[$i]['user'] != $usuario->user){            
                        $user_binario = User::where('user', $this->usuario_acima[$i]['user'])->first();

                            if($this->usuario_acima[$i+1]['lado'] == 2){
                                $user_binario->ponto_esquerda =  $user_binario->ponto_esquerda + $soma_ponto_binario; 
                                $user_binario->total_ponto_es = $user_binario->total_ponto_es + $soma_ponto_binario; 
                            }else{
                                $user_binario->ponto_direita =  $user_binario->ponto_direita + $soma_ponto_binario; 
                                $user_binario->total_ponto_di = $user_binario->total_ponto_di + $soma_ponto_binario; 
                            } 

                            $user_binario->save();
                        }      
                    
                }
                $usuario_acima_barrar = true;
            }
            

            $unilevel_produto_1 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 1')
            ->first();
            //dd($unilevel_produto_1);

            $unilevel_produto_2 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 2')
            ->first();

            $unilevel_produto_3 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 3')
            ->first();

            $unilevel_produto_4 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 4')
            ->first();
            
            $unilevel_produto_5 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 5')
            ->first();

            $unilevel_produto_6 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 6')
            ->first();
            
            $unilevel_produto_7 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 7')
            ->first();
            
            $unilevel_produto_8 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 8')
            ->first();

            $unilevel_produto_9 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 9')
            ->first();

            $unilevel_produto_10 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 10')
            ->first();

            $unilevel_produto_11 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 11')
            ->first();

            $unilevel_produto_12 = UnilevelProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 12')
            ->first();        

            if(is_null($total)){
                $transacao_tipo = TransacaoTipo::where('nome','Compras de produto')->first();   
                $transacao = New Transacao();
                $transacao->valor = $produto->valor_v_usuario;
                $transacao->fk_transacao_tipo = $transacao_tipo->id;
                $transacao->fk_membro = Auth::user()->id;
                $transacao->fk_adesao_usuario = $usuario_id;
                $transacao->save(); 
            }
            
            $user = User::where('id',$usuario_id)->first();

            $transacao_tipo_patrocinador = TransacaoTipo::where('nome','Bônus de venda')->first();

            $patrocinador = User::where('user',$user->patrocinador)->first();
            
            //PRIMEIRO NIVEL
             if($patrocinador != null){    
                if($patrocinador != null && $unilevel_produto_1 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();
                    //dd($unilevel_produto_1);
                      if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_1->valor * $produto->quantidade;
                        //$saldo_patrocinador_new->saldo = $saldo_patrocinador_new->saldo + $produto->comissao_produto;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_1->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                        //dd($saldo_patrocinador);
                        
                    }
                   
                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_1 ->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //SEGUNDO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                
                if($patrocinador != null && $unilevel_produto_2 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();
                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_2->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_2->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_2->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //TERCEIRO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $unilevel_produto_3 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_3->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_3->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_3->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //QUARTO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $unilevel_produto_4 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_4->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_4->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_4->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //QUINTO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $unilevel_produto_5 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_5->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_5->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_5->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->save();
                }
            }

             //SEXTO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $unilevel_produto_6 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_6->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_6->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_6->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }
      
             //SETIMO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $unilevel_produto_7 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_7->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_7->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_7->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //OITAVO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $unilevel_produto_8 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_8->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_8->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_8->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //NONO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $unilevel_produto_9 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_9->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_9->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_9->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //DECIMO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $unilevel_produto_10 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_10->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_10->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_10->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //DECIMO PRIMEIRO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $unilevel_produto_11 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_11->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_11->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_11->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }   

            //DECIMO SEGUNDO NIVEL
            if($patrocinador != null){  
                 $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
           
                if($patrocinador != null && $unilevel_produto_12 != null){
                    $saldo_patrocinador = SaldoUser::where('fk_user',$patrocinador->id)->first();

                    if($saldo_patrocinador == null){
                        $saldo_patrocinador_new = New SaldoUser();
                        $saldo_patrocinador_new->saldo = $unilevel_produto_12->valor * $produto->quantidade;
                        $saldo_patrocinador_new->fk_user = $patrocinador->id;
                        $saldo_patrocinador_new->save(); 
                    }else{
                        $saldo_patrocinador->saldo = $saldo_patrocinador->saldo + ($unilevel_produto_12->valor * $produto->quantidade);
                        $saldo_patrocinador->save();
                    }

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $unilevel_produto_12->valor * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //FIDELIDADE PRODUTO

            $fidelidade_produto_1 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 1')
            ->first();

            $fidelidade_produto_2 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 2')
            ->first();

            $fidelidade_produto_3 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 3')
            ->first();

            $fidelidade_produto_4 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 4')
            ->first();
            
            $fidelidade_produto_5 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 5')
            ->first();

            $fidelidade_produto_6 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 6')
            ->first();
            
            $fidelidade_produto_7 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 7')
            ->first();
            
            $fidelidade_produto_8 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 8')
            ->first();

            $fidelidade_produto_9 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 9')
            ->first();

            $fidelidade_produto_10 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 10')
            ->first();

            $fidelidade_produto_11 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 11')
            ->first();

            $fidelidade_produto_12 = FidelidadeProduto::where('fk_produto', $produto->id)
            ->where('nivel','Nivel 12')
            ->first();

            $patrocinador = User::where('user',$user->patrocinador)->first();
            $transacao_tipo_patrocinador = TransacaoTipo::where('nome','Pontos de fidelidade')->first();
           //dd($fidelidade_produto_1);
           //dd($produto);
            //SEGUNDO NIVEL
             if($patrocinador != null){
                if($patrocinador != null && $fidelidade_produto_1 != null){
                  
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_1->ponto * $produto->quantidade);
                    $patrocinador->save(); 
                    
                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_1->ponto  * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                    
                }
            }
            
            //SEGUNDO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
                
                if($patrocinador != null && $fidelidade_produto_2 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_2->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_2->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //TERCEIRO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $fidelidade_produto_3 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_3->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_3->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //QUARTO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $fidelidade_produto_4 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_4->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_4->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //QUINTO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $fidelidade_produto_5 != null){
                   $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_5->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_5->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

             //SEXTO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $fidelidade_produto_6 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_6->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_6->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }
      
             //SETIMO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $fidelidade_produto_7 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_7->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_7->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //OITAVO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $fidelidade_produto_8 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_8->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_8->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //NONO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $fidelidade_produto_9 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_9->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_9->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //DECIMO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $fidelidade_produto_10 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_10->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_10->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }

            //DECIMO PRIMEIRO NIVEL
            if($patrocinador != null){ 
                $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
               
                if($patrocinador != null && $fidelidade_produto_11 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_11->ponto * $produto->quantidade);
                    $patrocinador->save(); 

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_11->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }   

            //DECIMO SEGUNDO NIVEL
            if($patrocinador != null){  
                 $patrocinador = User::where('user',$patrocinador->patrocinador)->first();
           
                if($patrocinador != null && $fidelidade_produto_12 != null){
                    $patrocinador->ponto_fidelidade =  $patrocinador->ponto_fidelidade + ($fidelidade_produto_12->ponto * $produto->quantidade);
                    $patrocinador->save();

                    $patrocinador_transacao = New Transacao();
                    $patrocinador_transacao->valor = $fidelidade_produto_12->ponto * $produto->quantidade;
                    $patrocinador_transacao->fk_transacao_tipo = $transacao_tipo_patrocinador->id;
                    $patrocinador_transacao->fk_membro = $patrocinador->id;
                    $patrocinador_transacao->fk_adesao_usuario = $usuario_id;
                    $patrocinador_transacao->save();
                }
            }
        }

        if(is_null($codigo_do_pedido))
            $carrinho = Carrinho::WHERE('fk_user',$usuario_id)->delete();
        else{
            Carrinho::WHERE('codigo_pedido',$codigo_do_pedido)->update([
                'updated_at' => date("Y-m-d H:i:s",strtotime("now")),
                'status' => 'PAGO'
            ]);

        }

        return response()->json('true');
    }

    public function carrinho(Request $request)
    {
        $carrinho = New Carrinho();
        $carrinho->quantidade = $request->quantidade;
        $carrinho->fk_produto = $request->id;
        $carrinho->fk_user = Auth::user()->id;

        return response()->json($carrinho);
    }

    public function getProduto(Request $request)
    {
        $produto = produto::where('nome',$request->produto)->first();

        $view = view("produtos.pagar_produto",compact('produto'))->render();
        return $view;
    }
 
    public function list()
    {
        $produto = Produto::orderBy('created_at', 'desc')
        ->where('fk_user',Auth::user()->id)
        ->get();
            
        return DataTables::of($produto)
            ->editColumn('acao', function ($produto){
                return $this->setBtns($produto);
            })
            ->addColumn('imagem', function ($produto){
                 $url = asset($produto->imagem);
                 return '<img src='.$url.' width="150px"  />';
            })
            ->escapeColumns([0])
            ->make(true);
    } 

    public function getVendidos()
    {
        $produto = UserCompra::join('produtos','produtos.id','=','user_compras.fk_produto')

        ->orderBy('user_compras.created_at', 'desc')
        ->where('produtos.fk_user',Auth::user()->id)
        ->get();
            
        return DataTables::of($produto)
            ->addColumn('imagem', function ($produto){
                 $url = asset($produto->imagem);
                 return '<img src='.$url.' width="150px"  />';
            })  
            ->editColumn('quantidade', function ($produto){
                return $this->qtdEstoque($produto);
            })->escapeColumns([0])
            ->make(true);
    }

    public function getDisponivel()
    {
        $produto = Produto::orderBy('created_at', 'desc')
        ->where('fk_user',Auth::user()->id)
        ->get();
            
        return DataTables::of($produto)
            ->addColumn('imagem', function ($produto){
                 $url = asset($produto->imagem);
                 return '<img src='.$url.' width="150px"  />';
            })  
            ->escapeColumns([0])
            ->make(true);
    }

    private function qtdEstoque(Produto $produto){
        $qtd = UserCompra::where('fk_produto', $produto->id)
                ->select('quantidade')
                ->sum('quantidade');
        return $qtd;
    }

    public function disponivel()
    {
        return view('produtos.disponivel');
    }

    public function vendidos()
    {
        return view('produtos.vendidos');
    }

    private function setBtns(produto $produtos){
        $dados = "data-id_del='$produtos->id' 
        data-id='$produtos->id' 
        data-valor_compra='$produtos->valor_compra' 
        data-valor_v_site='$produtos->valor_v_site' 
        data-valor_v_usuario='$produtos->valor_v_usuario' 
        data-valor_v_pa='$produtos->valor_v_pa' 
        data-valor_v_cd='$produtos->valor_v_cd' 
        data-pontos_binario='$produtos->pontos_binario' 
        data-pontos_carreira='$produtos->pontos_carreira' 
        data-nome='$produtos->nome' 
        data-estoque='$produtos->estoque' 
        data-ponto_unilevel='$produtos->ponto_unilevel' 
        data-comissao_produto='$produtos->comissao_produto' 
        data-fk_categoria='$produtos->fk_categoria'
        data-ativo='$produtos->ativo'
        data-descricao='$produtos->descricao'
        data-peso='$produtos->peso'
        ";

        $btnVer = "<a class='btn btn-info btn-sm btnVer' data-toggle='tooltip' title='Ver setor' $dados> <i class='fa fa-eye'></i></a> ";

        $btnEditar = "<a class='btn btn-primary btn-sm btnEditar' data-toggle='tooltip' title='Editar setor' $dados> <i class='fa fa-edit'></i></a> ";

        $btnDeletar = "<a class='btn btn-danger btn-sm btnDeletar' data-toggle='tooltip' title='Deletar setor' $dados><i class='fa fa-trash'></i></a>";


        return $btnVer.$btnEditar.$btnDeletar;
    }

    public static function moeda($get_valor) {

        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor);
        return $valor; 
    }

    public function store(Request $request)
    {  
          
        $rules = array(
            'estoque' => 'required',
            'valor_compra' => 'required',
            'valor_v_site' => 'required',
            'valor_v_usuario' => 'required',
            'valor_v_pa' => 'required',
            'valor_v_cd' => 'required',
            'peso' => 'required',
            'pontos_binario' => 'required',
            'pontos_carreira' => 'required'
            
        );
        $attributeNames = array(
            'estoque' => 'Estoque',
            'valor_compra' => 'Valor da compra',
            'valor_v_site' => 'Valor do site',
            'valor_v_usuario' => 'Valor do usuario',
            'valor_v_pa' => 'Valor PA',
            'valor_v_cd' => 'Valor CD',
            'pontos_binario' => 'Pontos binario',
            'peso' => 'Peso',
            'pontos_carreira' => 'Pontos Carreira'
        );
        
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($attributeNames);
        if ($validator->fails()){
                return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {
            $imagem = $request->file('imagem')->store('public/imagens/produtos');
            $imagem = str_replace('public','storage',$imagem);

			$data_cadastro = date("Y-m-d"); 
            $produto = new Produto();
            $produto->nome = $request->nome;
            $produto->imagem = $imagem;
            $produto->valor_compra = $this->moeda($request->valor_compra);
            $produto->valor_v_site = $this->moeda($request->valor_v_site);
            $produto->valor_v_usuario = $this->moeda($request->valor_v_usuario);
            $produto->valor_v_pa = $this->moeda($request->valor_v_pa);
            $produto->valor_v_cd = $this->moeda($request->valor_v_cd);
            $produto->pontos_binario = $request->pontos_binario;
            $produto->ponto_unilevel = $request->ponto_unilevel;
            $produto->peso = $request->peso;
            $produto->ativo = 'Ativo';
            $produto->descricao = $request->descricao;
            $produto->comissao_produto = $request->comissao_produto;
            $produto->fk_categoria = $request->fk_categoria;
            $produto->estoque = $request->estoque;
            $produto->fk_user = Auth::user()->id;
            $produto->pontos_carreira = $request->pontos_carreira;
            $produto->data_cadastro = $data_cadastro;
            $produto->save();

            if(isset($request->unilevel)){
                $unilevel = json_decode($request->unilevel);
	            foreach ($unilevel as $value) {
	            	$UnilevelProduto = new UnilevelProduto();
		            $UnilevelProduto->fk_produto = $produto->id;
		            $UnilevelProduto->valor = $value->valor;
		            $UnilevelProduto->nivel = $value->nivel;
		            $UnilevelProduto->save();
	            }
        	}

            if(isset($request->fidelidade)){
                $fidelidade = json_decode($request->fidelidade);
	            foreach ($fidelidade as $value) {
	            	$FidelidadeProduto = new FidelidadeProduto();
		            $FidelidadeProduto->fk_produto = $produto->id;
		            $FidelidadeProduto->ponto = $value->ponto;
		            $FidelidadeProduto->nivel = $value->nivel;
		            $FidelidadeProduto->save();
	            }
        	}
            try {
                //chaves do .env para vindi
                $arguments = array(
                    'VINDI_API_KEY' => env('VINDI_API_KEY'),
                    'VINDI_API_URI' => env('VINDI_API_URI')
                );

            // Instancia o serviço de Customers (Clientes) com o array contendo VINDI_API_KEY e VINDI_API_URI
            $productService = new Vindi\Product($arguments);

            // Cria um novo produto:
            $product = $productService->create([
                'name' => $produto->nome,
                'code' => $produto->id,
                'pricing_schema' => [
                    'price' => $produto->valor_v_site,
                    'schema_type' => '1',
                ]
            ]);
            //dd($product);
            } catch (\Exception $e) {
               return response()->json($e);
            }
           
            return response()->json($produto);
        }
    }

    public function unilevel($id)
    {
        $nivel = UnilevelProduto::where('fk_produto',$id)
        ->get();
            
        return response()->json(['data'=> $nivel]);
    }

    public function fidelidade($id)
    {
        $nivel = FidelidadeProduto::where('fk_produto',$id)
        ->get();
            
        return response()->json(['data'=> $nivel]);
    }


    public function update(Request $request)
    {
        
        $produto = Produto::find($request->id);

        if($request->file('imagem')){
           $imagem = $request->file('imagem')->store('public/imagens/produtos');
           $imagem = str_replace('public','storage',$imagem); 
           $produto->imagem = $imagem;

        }

        $produto->nome = $request->nome;
        $produto->valor_compra = $this->moeda($request->valor_compra);
        $produto->valor_v_site = $this->moeda($request->valor_v_site);
        $produto->valor_v_usuario = $this->moeda($request->valor_v_usuario);
        $produto->valor_v_pa = $this->moeda($request->valor_v_pa);
        $produto->valor_v_cd = $this->moeda($request->valor_v_cd);
        $produto->pontos_binario = $request->pontos_binario;
        $produto->ponto_unilevel = $request->ponto_unilevel;
        $produto->peso = $request->peso;
        $produto->descricao = $request->descricao;
        if(isset($request->ativo)){
            $produto->ativo = $request->ativo;
        }
        $produto->comissao_produto = $request->comissao_produto;
        $produto->fk_categoria = $request->fk_categoria;
        $produto->estoque = $request->estoque;
        $produto->pontos_carreira = $request->pontos_carreira;
        $produto->save();

            if(isset($request->unilevel)){
                UnilevelProduto::where('fk_produto',$produto->id)->delete();
                $unilevel = json_decode($request->unilevel);
                foreach ($unilevel as $value) {
                    $UnilevelProduto = new UnilevelProduto();
                    $UnilevelProduto->fk_produto = $produto->id;
                    $UnilevelProduto->valor = $value->valor;
                    $UnilevelProduto->nivel = $value->nivel;
                    $UnilevelProduto->save();
                }
            }else{
                UnilevelProduto::where('fk_produto',$produto->id)->delete();
            }

            if(isset($request->fidelidade)){
                FidelidadeProduto::where('fk_produto',$produto->id)->delete();
                $fidelidade = json_decode($request->fidelidade);
                foreach ($fidelidade as $value) {
                    $FidelidadeProduto = new FidelidadeProduto();
                    $FidelidadeProduto->fk_produto = $produto->id;
                    $FidelidadeProduto->ponto = $value->ponto;
                    $FidelidadeProduto->nivel = $value->nivel;
                    $FidelidadeProduto->save();
                }
            }else{
                FidelidadeProduto::where('fk_produto',$produto->id)->delete();
            }

        return response()->json($produto);
    }

    public function destroy(Request $request)
    {
        $produto = produto::destroy($request->id_del);

        return response()->json($produto);
    }
}
