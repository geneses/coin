<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrinho extends Model
{
    public function usuario() {
        return $this->belongsTo(User::class,'fk_user');
    }

    public function produto(){
        return $this->belongsTo(Produto::class,'fk_produto');
    }
}
