<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
use App\User;

class SendMailUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->subject('Senha financeira');
        $this->from('suporte.coin@gmail.com');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $senha = str_random(10);
        $user = User::find(Auth::user()->id);
        $user->senha_transacao = bcrypt($senha);
        $user->save();

        return $this->view('financeiro_usuarios.geracao_senha',compact('senha'));
    }
}
