<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransacaoTipo extends Model
{
     protected $fillable = [
        'nome', 'acao',
    ];
}
